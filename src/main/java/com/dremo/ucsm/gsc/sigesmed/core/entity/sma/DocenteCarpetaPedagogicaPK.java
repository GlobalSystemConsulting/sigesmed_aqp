/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author gscadmin
 */
@Embeddable
public class DocenteCarpetaPedagogicaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "doc_id")
    private int docId;
    @Basic(optional = false)
    @Column(name = "car_dig_id")
    private long carDigId;
    @Basic(optional = false)
    @Column(name = "con_sec_car_ped_id")
    private int conSecCarPedId;

    public DocenteCarpetaPedagogicaPK() {
    }

    public DocenteCarpetaPedagogicaPK(int docId, long carDigId, int conSecCarPedId) {
        this.docId = docId;
        this.carDigId = carDigId;
        this.conSecCarPedId = conSecCarPedId;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public long getCarDigId() {
        return carDigId;
    }

    public void setCarDigId(long carDigId) {
        this.carDigId = carDigId;
    }

    public int getConSecCarPedId() {
        return conSecCarPedId;
    }

    public void setConSecCarPedId(int conSecCarPedId) {
        this.conSecCarPedId = conSecCarPedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) docId;
        hash += (int) carDigId;
        hash += (int) conSecCarPedId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocenteCarpetaPedagogicaPK)) {
            return false;
        }
        DocenteCarpetaPedagogicaPK other = (DocenteCarpetaPedagogicaPK) object;
        if (this.docId != other.docId) {
            return false;
        }
        if (this.carDigId != other.carDigId) {
            return false;
        }
        if (this.conSecCarPedId != other.conSecCarPedId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.DocenteCarpetaPedagogicaPK[ docId=" + docId + ", carDigId=" + carDigId + ", conSecCarPedId=" + conSecCarPedId + " ]";
    }
    
}
