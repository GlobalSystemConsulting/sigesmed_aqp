/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.SerieDocumentalDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author admin
 */
public class RegistrarSerieDocumentalTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
       SerieDocumental sd = null;
       
       try{
           JSONObject requestData = (JSONObject)wr.getData();
           int areaID = requestData.getInt("areaID");
           int uni_org_ID = requestData.getInt("uni_org_id");
           String cod = requestData.getString("cod_serie");
           String nombre = requestData.getString("nom_serie");
           int val = requestData.getInt("val_serie");
           
           sd = new SerieDocumental(0,areaID,uni_org_ID,cod,nombre,val);
           
       //     public SerieDocumental(int serie_doc_id,int are_id,int uni_org_id,String codigo,String nombre , String valor){
       }catch(Exception e){
           System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Serie Documental , datos incorrectos", e.getMessage() );
       }
        //Operaciones con la Base de Datos
       SerieDocumentalDAO ser_doc_dao = (SerieDocumentalDAO)FactoryDao.buildDao("sad.SerieDocumentalDAO");
       try{
           ser_doc_dao.insert(sd);
       }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Serie Documental, datos incorrectos", e.getMessage() );
       } 
       JSONObject oResponse = new JSONObject();
        oResponse.put("serie_id",sd.getSerDocId());
        oResponse.put("ser_doc_cod",sd.getCodigo());
        oResponse.put("nom",sd.getNombre());
        return WebResponse.crearWebResponseExito("El registro de La Serie Documental se realizo correctamente", oResponse);
       
       
       
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
