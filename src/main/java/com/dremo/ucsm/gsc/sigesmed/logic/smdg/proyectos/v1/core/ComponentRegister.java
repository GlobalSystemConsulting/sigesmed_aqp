/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ActualizarActividadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ActualizarProyectosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ActualizarRecursosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.EliminarActividadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.EliminarProyectosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.EliminarRecursosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ListarActividadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ListarProyectoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ListarProyectosxOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.ListarRecursosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.RegistrarActividadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.RegistrarProyectosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx.RegistrarRecursosTx;

/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_MONITOREO_DOCUMENTOS_GESTION);        
        
        //Registrando el Nombre del componente
        component.setName("proyectos");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarProyectosxOrganizacion", ListarProyectosxOrganizacionTx.class);
        component.addTransactionGET("listarActividades", ListarActividadesTx.class);
        component.addTransactionGET("listarRecursos", ListarRecursosTx.class);
        component.addTransactionGET("listarProyecto", ListarProyectoTx.class);
        
        component.addTransactionPUT("actualizarProyectos", ActualizarProyectosTx.class);
        component.addTransactionPUT("actualizarActividades", ActualizarActividadesTx.class);
        component.addTransactionPUT("actualizarRecursos", ActualizarRecursosTx.class);
        
        component.addTransactionPOST("registrarProyectos", RegistrarProyectosTx.class);
        component.addTransactionPOST("registrarActividades", RegistrarActividadesTx.class);
        component.addTransactionPOST("registrarRecursos", RegistrarRecursosTx.class);
        
        component.addTransactionPUT("eliminarProyectos", EliminarProyectosTx.class);
        component.addTransactionPUT("eliminarActividades", EliminarActividadesTx.class);
        component.addTransactionPUT("eliminarRecursos", EliminarRecursosTx.class);
        
        return component;
    }
}
