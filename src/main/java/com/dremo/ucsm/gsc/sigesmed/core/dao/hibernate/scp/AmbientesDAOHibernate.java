/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.Ambientes;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.AmbientesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;

/**
 *
 * @author Administrador
 */
public class AmbientesDAOHibernate extends GenericDaoHibernate<Ambientes> implements AmbientesDAO {

    @Override
    public List<Ambientes> listarAmbientes(int org_id) {
        
   
        List<Ambientes> ambientes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT amb FROM Ambientes amb WHERE amb.est_reg!='E' and amb.org_id=:p1";
            Query query = session.createQuery(hql);
            query.setParameter("p1",org_id);
            ambientes = query.list();
            
        }
        catch(Exception e){
         System.out.println("No se pudo Mostrar la lista de Ambientse \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar la lista de Ambientes \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return ambientes;  
        
    }
    
    
}
