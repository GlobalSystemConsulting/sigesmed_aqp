/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx.ActualizarDirectorioExternoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx.EliminarDirectorioExternoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx.InsertarDirectorioExternoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx.ListarDirectorioExternoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx.ListarDirectorioExternoxNombreTx;

/**
 *
 * @author ucsm
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DIRECTORIO);        
        
        //Registrando el Nombre del componente
        component.setName("directorioExterno");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionPOST("insertarDirectorioExterno", InsertarDirectorioExternoTx.class);        
        component.addTransactionDELETE("eliminarDirectorioExterno", EliminarDirectorioExternoTx.class);
        component.addTransactionPUT("actualizarDirectorioExterno", ActualizarDirectorioExternoTx.class);
        component.addTransactionGET("listarDirectorioExterno", ListarDirectorioExternoTx.class);
        component.addTransactionGET("listarDirectorioExternoxNombre", ListarDirectorioExternoxNombreTx.class);
        
        return component;
    }
}
