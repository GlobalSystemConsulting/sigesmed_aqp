/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.CatalogoBienesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
/**
 *
 * @author Administrador
 */
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;

public class CatalogoBienesDAOHibernate extends GenericDaoHibernate<CatalogoBienes> implements CatalogoBienesDAO{

    @Override
    public List<CatalogoBienes> listarCatalogo() {
        
        List<CatalogoBienes> cat_bienes = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT cb FROM CatalogoBienes cb JOIN FETCH cb.unidad_medida JOIN FETCH cb.familia familia  JOIN FETCH familia.clase_generica clase JOIN FETCH clase.grupo  WHERE cb.est_reg!='E'";
          //    String hql = "SELECT DISTINCT ii FROM InventarioInicial ii JOIN FETCH ii.inv_ini_det detalle JOIN FETCH detalle.cod_bie WHERE ii.est_reg!='E'";

         //   String hql = "SELECT cb FROM CatalogoBienes cb  JOIN FETCH cb.familia  JOIN FETCH cb.unidad_medida WHERE cb.est_reg!='E'";

            Query query = session.createQuery(hql); 
            cat_bienes = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Mostrar el Catalogo de Bienes \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Mostrar el Catalogo de Bienes \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return cat_bienes;  

    }

    @Override
    public CatalogoBienes mostrarDetalleCatalogo(int cat_bie_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
