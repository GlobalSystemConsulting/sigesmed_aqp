/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;


import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Administrador
 */
@Entity(name = "com.dremo.ucsm.gsm.sigesmed.core.entity.se.TipoPariente")
@Table(name = "tipo_pariente", schema="public")

public class TipoPariente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tpa_id")
    private Integer tpaId;
    
    @Column(name = "tpa_des")
    private String tpaDes;

    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    public TipoPariente() {
    }

    public TipoPariente(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public TipoPariente(String tpaDes) {        
        this.tpaDes = tpaDes;
    }

    public Integer getTpaId() {
        return tpaId;
    }

    public void setTpaId(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public String getTpaDes() {
        return tpaDes;
    }

    public void setTpaDes(String tpaDes) {
        this.tpaDes = tpaDes;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tpaId != null ? tpaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPariente)) {
            return false;
        }
        TipoPariente other = (TipoPariente) object;
        if ((this.tpaId == null && other.tpaId != null) || (this.tpaId != null && !this.tpaId.equals(other.tpaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dremo.ucsm.gsm.sigesmed.core.entity.se.TipoPariente[ tpaId=" + tpaId + " ]";
    }
    
}
