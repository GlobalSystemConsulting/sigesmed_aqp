/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx;

/**
 *
 * @author abel
 */
public final class EstadoExpediente {
    public static final int NUEVO = 1;
    public static final int RECHAZADO = 2;
    public static final int RECIBIDO = 3;
    public static final int ESPERA = 4;
    public static final int FINALIZADO = 5;
    public static final int DERIVADO = 6;
    public static final int DEVUELTO = 7;
    
    public static String toString(int estado){
        switch(estado){
            case NUEVO: return "NUEVO";
            case RECHAZADO: return "RECHAZADO";
            case RECIBIDO: return "RECIBIDO";
            case FINALIZADO: return "FINALIZADO";
            case DERIVADO: return "DERIVADO";
            case DEVUELTO: return "DEVUELTO";                    
            default:
                    return "";
        }
    }
}

