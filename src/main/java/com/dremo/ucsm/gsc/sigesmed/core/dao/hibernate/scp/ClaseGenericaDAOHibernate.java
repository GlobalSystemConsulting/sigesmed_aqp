/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ClaseGenericaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.CatalogoBienes;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.GrupoGenerico;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ClaseGenerica;

/**
 *
 * @author Administrador
 */
public class ClaseGenericaDAOHibernate extends GenericDaoHibernate<ClaseGenerica> implements ClaseGenericaDAO{

    

    @Override
    public ClaseGenerica mostrarDetalleClase(int cla_gem_id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ClaseGenerica> listarClases(int gru_gen_id) {
        
         List<ClaseGenerica> clases = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT DISTINCT cg FROM ClaseGenerica cg JOIN FETCH cg.grupo WHERE cg.grupo.gru_gen_id=:p1 and cg.est_reg!='E'";
            Query query = session.createQuery(hql);
            query.setParameter("p1",gru_gen_id );
            clases = query.list();
            
        }catch(Exception e){
             System.out.println("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
             throw new UnsupportedOperationException("No se pudo Listar las Clases Genericas \\n "+ e.getMessage());
        }
        finally{
            session.close();
        }
        return clases;       
    }
    
}
