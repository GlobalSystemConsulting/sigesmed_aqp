/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx.*;
/**
 *
 * @author Administrador
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_MONITOREO_ACOMPANIAMIENTO);        
        
        //Registrando el Nombre del componente
        component.setName("programacion_monitoreo");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        
        component.addTransactionGET("listarMonitoreosPorUgel", ListarMonitoreosPorUgelTx.class);
        component.addTransactionGET("listarMonitoreoDetalle", ListarMonitoreoDetalleTx.class);
        component.addTransactionGET("listarInstitucionesPorUgel", ListarInstitucionesPorUgelTx.class);
        component.addTransactionGET("listarEspecialistasDeUgel", ListarEspecialistasUgelTx.class);
        component.addTransactionPOST("programarMonitoreoMasivo", ProgramarMonitoreoMasivoTx.class);
        component.addTransactionPUT("actualizarMonitoreo", ActualizarMonitoreoTx.class);
        component.addTransactionPUT("actualizarDetalleMonitoreo", ActualizarDetalleMonitoreoTx.class);
        component.addTransactionPOST("reporteSMA", ReporteSMATx.class);
        
        return component;
    }
}


