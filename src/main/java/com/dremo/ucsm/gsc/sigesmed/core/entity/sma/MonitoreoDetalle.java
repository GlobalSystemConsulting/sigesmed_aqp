/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "monitoreo_detalle", schema = "pedagogico")

public class MonitoreoDetalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mnd_id")
    private Integer mndId;
    
    @Column(name = "fec_vis")
    @Temporal(TemporalType.DATE)
    private Date fecVis;
    
    @Column(name = "id_esp")
    private Integer idEsp;
    
    @Column(name = "id_doc")
    private Integer idDoc;
    
    @Column(name = "eta_mon")
    private Character etaMon;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;

    @Column(name = "est_reg")
    private String estReg;

    @JoinColumn(name = "mni_id", referencedColumnName = "mni_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Monitoreo mniId;

    public MonitoreoDetalle() {
    }

    public MonitoreoDetalle(Integer mndId) {
        this.mndId = mndId;
    }
    
    public MonitoreoDetalle(Monitoreo monitoreo, Integer idDoc, Character etaMon, Integer usuMod) { 
        this.mniId = monitoreo;
        this.idDoc = idDoc;
        this.etaMon = etaMon;
        this.usuMod = usuMod;
        this.fecMod = new Date();
        this.estReg = "A";
    }

    public Integer getMndId() {
        return mndId;
    }

    public void setMndId(Integer mndId) {
        this.mndId = mndId;
    }

    public Date getFecVis() {
        return fecVis;
    }

    public void setFecVis(Date fecVis) {
        this.fecVis = fecVis;
    }

    public Integer getIdEsp() {
        return idEsp;
    }
    
    public void setIdEsp(Integer idEsp) {
        this.idEsp = idEsp;
    }
    
    public Integer getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public Character getEtaMon() {
        return etaMon;
    }

    public void setEtaMon(Character etaMon) {
        this.etaMon = etaMon;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public Monitoreo getMniId() {
        return mniId;
    }

    public void setMniId(Monitoreo mniId) {
        this.mniId = mniId;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mndId != null ? mndId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MonitoreoDetalle)) {
            return false;
        }
        MonitoreoDetalle other = (MonitoreoDetalle) object;
        if ((this.mndId == null && other.mndId != null) || (this.mndId != null && !this.mndId.equals(other.mndId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.MonitoreoDetalle[ mndId=" + mndId + " ]";
    }
    
}
