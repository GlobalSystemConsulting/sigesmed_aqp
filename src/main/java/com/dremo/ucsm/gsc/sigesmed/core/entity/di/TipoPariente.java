/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.di;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "tipo_pariente")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "TipoPariente.findAll", query = "SELECT t FROM TipoPariente t"),
//    @NamedQuery(name = "TipoPariente.findByTpaId", query = "SELECT t FROM TipoPariente t WHERE t.tpaId = :tpaId"),
//    @NamedQuery(name = "TipoPariente.findByTpaDes", query = "SELECT t FROM TipoPariente t WHERE t.tpaDes = :tpaDes")})
public class TipoPariente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tpa_id")
    private Integer tpaId;
    @Column(name = "tpa_des")
    private String tpaDes;

    public TipoPariente() {
    }

    public TipoPariente(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public TipoPariente(String tpaDes) {        
        this.tpaDes = tpaDes;
    }

    public Integer getTpaId() {
        return tpaId;
    }

    public void setTpaId(Integer tpaId) {
        this.tpaId = tpaId;
    }

    public String getTpaDes() {
        return tpaDes;
    }

    public void setTpaDes(String tpaDes) {
        this.tpaDes = tpaDes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tpaId != null ? tpaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPariente)) {
            return false;
        }
        TipoPariente other = (TipoPariente) object;
        if ((this.tpaId == null && other.tpaId != null) || (this.tpaId != null && !this.tpaId.equals(other.tpaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.TipoPariente[ tpaId=" + tpaId + " ]";
    }
    
}
