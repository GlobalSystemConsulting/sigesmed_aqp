package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.RolFuncionModel;
import java.util.List;

public interface FuncionSistemaDao extends GenericDao<FuncionSistema> {

    public List<FuncionSistema> buscarConSubModulo();

    public List<FuncionSistema> buscarPorRol(int rolId);

    public List<RolFuncionModel> buscarFuncionesPersonalizadasPorRol(int rolId);

    List<FuncionSistema> listarFunciones(int subModCod, int rolCod);
}
