/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */

@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CarpetaPedagogica")
@Table(name = "carpeta_pedagogica", schema = "pedagogico")

public class CarpetaPedagogica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "car_dig_id")
    private Long carDigId;
    @Basic(optional = false)
    @Column(name = "eta")
    private short eta;
    @Basic(optional = false)
    @Column(name = "fec_cre")
    @Temporal(TemporalType.DATE)
    private Date fecCre;
    @Column(name = "des")
    private String des;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "est_reg")
    private Character estReg;
    @JoinTable(name = "secciones_carpeta_pedagogica", joinColumns = {
        @JoinColumn(name = "car_dig_id", referencedColumnName = "car_dig_id")}, inverseJoinColumns = {
        @JoinColumn(name = "sec_car_ped_id", referencedColumnName = "sec_car_ped_id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<SeccionCarpetaPedagogica> seccionCarpetaPedagogicaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carpetaPedagogica", fetch = FetchType.LAZY)
    private List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList;

    public CarpetaPedagogica() {
    }

    public CarpetaPedagogica(Long carDigId) {
        this.carDigId = carDigId;
    }

    public CarpetaPedagogica(Long carDigId, short eta, Date fecCre) {
        this.carDigId = carDigId;
        this.eta = eta;
        this.fecCre = fecCre;
    }

    public Long getCarDigId() {
        return carDigId;
    }

    public void setCarDigId(Long carDigId) {
        this.carDigId = carDigId;
    }

    public short getEta() {
        return eta;
    }

    public void setEta(short eta) {
        this.eta = eta;
    }

    public Date getFecCre() {
        return fecCre;
    }

    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @XmlTransient
    public List<SeccionCarpetaPedagogica> getSeccionCarpetaPedagogicaList() {
        return seccionCarpetaPedagogicaList;
    }

    public void setSeccionCarpetaPedagogicaList(List<SeccionCarpetaPedagogica> seccionCarpetaPedagogicaList) {
        this.seccionCarpetaPedagogicaList = seccionCarpetaPedagogicaList;
    }

    @XmlTransient
    public List<DocenteCarpetaPedagogica> getDocenteCarpetaPedagogicaList() {
        return docenteCarpetaPedagogicaList;
    }

    public void setDocenteCarpetaPedagogicaList(List<DocenteCarpetaPedagogica> docenteCarpetaPedagogicaList) {
        this.docenteCarpetaPedagogicaList = docenteCarpetaPedagogicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carDigId != null ? carDigId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarpetaPedagogica)) {
            return false;
        }
        CarpetaPedagogica other = (CarpetaPedagogica) object;
        if ((this.carDigId == null && other.carDigId != null) || (this.carDigId != null && !this.carDigId.equals(other.carDigId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.CarpetaPedagogica[ carDigId=" + carDigId + " ]";
    }
    
}
