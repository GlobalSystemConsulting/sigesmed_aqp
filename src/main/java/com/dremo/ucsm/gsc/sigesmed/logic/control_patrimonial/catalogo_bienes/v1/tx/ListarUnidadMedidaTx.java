/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.catalogo_bienes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.UnidadMedida;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.UnidadMedidaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class ListarUnidadMedidaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {

          JSONArray miArray = new JSONArray();
        try{
            List<UnidadMedida> unidades_medida = null;
            JSONObject requestData = (JSONObject)wr.getData();
            UnidadMedidaDAO uni_med_dao = (UnidadMedidaDAO)FactoryDao.buildDao("scp.UnidadMedidaDAO");
            unidades_medida = uni_med_dao.listarUnidadesMedida();
        
            for(UnidadMedida uni_med : unidades_medida){
               JSONObject oResponse = new JSONObject();
               oResponse.put("uni_med_id",uni_med.getUni_med_id());
               oResponse.put("nom",uni_med.getNom()); 
               miArray.put(oResponse);
           } 
        }  
        catch(Exception e){
             System.out.println("No se pudo Listar las Unidades de Medida\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades de Medida", e.getMessage() );        

        }
        
        return WebResponse.crearWebResponseExito("Se Listo Las Unidades de Medida Correctamente",miArray); 

    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
