/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.BandejaMensajeModel;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarMensajesNuevosPorUsuarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
              
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<BandejaMensajeModel> mensajes = null;
        MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
        try{
            mensajes = mensajeDao.listarMensajesNuevosNoVistosPorUsuario(wr.getIdUsuario());
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los mensajes nuevos sin ver del usuario"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los mensajes nuevos sin ver del usuario", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int i = 0;
        for(BandejaMensajeModel mensaje:mensajes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("bandejaMensajeID",mensaje.banMenId );
            
            //oResponse.put("mensajeID",mensaje.menEleId);
            oResponse.put("asunto",mensaje.asunto);
            oResponse.put("remitente",mensaje.nombres+" "+mensaje.apellido1);
            oResponse.put("fechaEnvio",sf.format(mensaje.fecEnv) );            
            
            //oResponse.put("estado",""+mensajeEstado());
            oResponse.put("i",i++);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los mensajes nuevos por usuario",miArray);        
        //Fin
    }
    
}


