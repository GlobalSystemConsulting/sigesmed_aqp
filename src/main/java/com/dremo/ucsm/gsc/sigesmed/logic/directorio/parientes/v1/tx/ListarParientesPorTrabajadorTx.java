/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.parientes.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.ParientesDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.Parientes;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarParientesPorTrabajadorTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int traId = requestData.getInt("traId");
        
        List<Parientes> parientes = null;
        ParientesDao parientesDao = (ParientesDao)FactoryDao.buildDao("di.ParientesDao");
        
        try{
            parientes = parientesDao.listarParientesxTrabajador(traId);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los parientes del trabajador \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los parientes del trabajador ", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Parientes pariente:parientes ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("parId",pariente.getPariente().getPerId());
            oResponse.put("parDni",pariente.getPariente().getDni());
            oResponse.put("parMat",pariente.getPariente().getApeMat());
            oResponse.put("parPat",pariente.getPariente().getApePat());
            oResponse.put("parNom",pariente.getPariente().getNom());
            oResponse.put("parDir",pariente.getPariente().getPerDir());
            oResponse.put("parTel",pariente.getPariente().getFij());
            oResponse.put("tpaDes",pariente.getParentesco().getTpaDes());
            oResponse.put("tpaId",pariente.getParentesco().getTpaId());
            //oResponse.put("traId",pariente.getTrabajador().getTraId());            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
                
    }   
    
}
