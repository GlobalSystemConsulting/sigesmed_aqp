/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.proyectos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.ProyectoRecursosDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoActividades;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ProyectoRecursos;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.Proyectos;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class RegistrarRecursosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        
        JSONObject requestData = (JSONObject)wr.getData();
        
        int pacid = requestData.getInt("pacid");
        double reccos = requestData.optDouble("reccos");
        String recnom = requestData.optString("recnom");
        
        ProyectoRecursos recurso = new ProyectoRecursos(recnom, reccos, new ProyectoActividades(pacid));
                
        //Fin
                
        /*
        *  Parte para la operacion en la Base de Datos
        */
        ProyectoRecursosDao recursoDao = (ProyectoRecursosDao)FactoryDao.buildDao("smdg.ProyectoRecursosDao");
        
        try{
            recursoDao.insert(recurso);
        
        }catch(Exception e){
            System.out.println("No se pudo registrar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("recid",recurso.getPruId());
        //oResponse.put("fecha",recurso.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la recurso se realizo correctamente", oResponse);
        //Fin
    }
}
