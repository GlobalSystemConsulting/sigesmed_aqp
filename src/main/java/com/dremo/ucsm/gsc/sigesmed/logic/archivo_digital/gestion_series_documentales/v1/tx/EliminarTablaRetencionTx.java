/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;


import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.TablaRetencionDocumentosDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.TablaRetencionDocumentos;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jeferson
 */
public class EliminarTablaRetencionTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int ser_doc_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            ser_doc_id = requestData.getInt("serie_id");
        }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar la Tabla de Retencion", e.getMessage() );
        }
            TablaRetencionDocumentosDAO trd_dao = (TablaRetencionDocumentosDAO)FactoryDao.buildDao("sad.TablaRetencionDocumentosDAO");
         try{
             trd_dao.deleteAbsolute(new TablaRetencionDocumentos(ser_doc_id));
         }catch(Exception e){
             System.out.println(e);
             return  WebResponse.crearWebResponseError("No se pudo eliminar la Tabla de Retencion", e.getMessage() );
         }   
            return WebResponse.crearWebResponseExito("La Tabla de Retencion se elimino correctamente");
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
