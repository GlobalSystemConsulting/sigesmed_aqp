/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.configuracion_inicial.usuario_sistema.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Rol;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Usuario;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author Administrador
 */
public class ActualizarPasswordTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int usuarioID = 0;
        String passwordNuevo ="";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            usuarioID = requestData.getInt("usuarioID");
            
            String password = requestData.getString("password");
            passwordNuevo = requestData.getString("nuevoPassword");
            
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la contrasena, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        UsuarioDao usuarioDao = (UsuarioDao)FactoryDao.buildDao("UsuarioDao");
        try{
            usuarioDao.cambiarPassword(usuarioID, passwordNuevo);
        
        }catch(Exception e){
            System.out.println("No se pudo actualizar la contrasena\n"+e);
            return WebResponse.crearWebResponseError("No se pudo actualizar la contrasena", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("Se actualizo la contraseña correctamente");
        //Fin
    }
    
}
