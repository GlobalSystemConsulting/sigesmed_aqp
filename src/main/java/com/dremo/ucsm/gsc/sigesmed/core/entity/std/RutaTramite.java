package com.dremo.ucsm.gsc.sigesmed.core.entity.std;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.TipoArea;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@IdClass(RutaTramiteId.class)
@Entity
@Table(name="ruta_tramite" ,schema="administrativo")
public class RutaTramite  implements java.io.Serializable {

    @Id 
    @Column(name="rut_tra_id", unique=true, nullable=false)
    private int rutTraId;
    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tip_tra_id", nullable=false)
    private TipoTramite tipoTramite;
    
    @Column(name="are_ori_id")
    private int areOriId;
    @Column(name="are_des_id")
    private int areDesId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_ori_id", insertable=false,updatable=false )
    private TipoArea tipoAreaOrigen;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_des_id", insertable=false,updatable=false)
    private TipoArea tipoAreaDestino;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_ori_id", insertable=false,updatable=false )
    private Area areaOrigen;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="are_des_id", insertable=false,updatable=false)
    private Area areaDestino;
    
    
    @Column(name="des", length=256)
    private String des;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_mod", length=29)
    private Date fecMod;
    @Column(name="usu_mod")
    private Integer usuMod;
    @Column(name="est_reg", length=1)
    private char estReg;

    public RutaTramite() {
    }

	
    public RutaTramite(int rutTraId, TipoTramite tipoTramite) {
        this.rutTraId = rutTraId;
        this.tipoTramite = tipoTramite;
    }
    public RutaTramite(int rutTraId, TipoTramite tipoTramite, String des, int areaOriID, int areaDesID, Date fecMod, Integer usuMod, char estReg) {
       this.rutTraId = rutTraId;
       this.tipoTramite = tipoTramite;
       this.des = des;
       this.areOriId = areaOriID;
       this.areDesId = areaDesID;
       this.fecMod = fecMod;
       this.usuMod = usuMod;
       this.estReg = estReg;
    }
   
     
    public int getRutTraId() {
        return this.rutTraId;
    }
    public void setRutTraId(int rutTraId) {
        this.rutTraId = rutTraId;
    }

    public TipoTramite getTipoTramite() {
        return this.tipoTramite;
    }
    
    public void setTipoTramite(TipoTramite tipoTramite) {
        this.tipoTramite = tipoTramite;
    }

    
    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }

    public Date getFecMod() {
        return this.fecMod;
    }
    
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }
    
    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public char getEstReg() {
        return this.estReg;
    }
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    
    public int getAreOriId() {
        return this.areOriId;
    }
    public void setAreOriId(int areOriId) {
        this.areOriId = areOriId;
    }
    
    public int getAreDesId() {
        return this.areDesId;
    }
    public void setAreDesId(int areDesId) {
        this.areDesId = areDesId;
    }
    
    public Area getAreaOrigen() {
        return this.areaOrigen;
    }
    public void setAreaOrigen(Area areaOrigen) {
        this.areaOrigen = areaOrigen;
    }
    public Area getAreaDestino() {
        return this.areaDestino;
    }
    public void setAreaDestino(Area areaDestino) {
        this.areaDestino = areaDestino;
    }


    public TipoArea getTipoAreaOrigen() {
        return this.tipoAreaOrigen;
    }
    public void setTipoAreaOrigen(TipoArea tipoAreaOrigen) {
        this.tipoAreaOrigen = tipoAreaOrigen;
    }
    public TipoArea getTipoAreaDestino() {
        return this.tipoAreaDestino;
    }
    public void setTipoAreaDestino(TipoArea tipoAreaDestino) {
        this.tipoAreaDestino = tipoAreaDestino;
    }


}


