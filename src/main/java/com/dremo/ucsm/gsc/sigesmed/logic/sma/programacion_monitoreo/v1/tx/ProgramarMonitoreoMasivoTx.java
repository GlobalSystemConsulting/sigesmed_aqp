/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.DocenteDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.MonitoreoDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.Docente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Monitoreo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.MonitoreoDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Felipe
 */
public class ProgramarMonitoreoMasivoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(ProgramarMonitoreoMasivoTx.class.getName());

    @Override
     public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        
        String tipUgel = data.optString("tipUgel");
        Integer ugelId = data.optInt("ugelId");
        Integer idUsuario = wr.getIdUsuario();
                
        return calcularNumeroIEsAMonitorear(tipUgel, ugelId, idUsuario);
    }

    private WebResponse calcularNumeroIEsAMonitorear(String tipUgel, Integer ugelId, Integer idUsuario) {
        try{
            List<Organizacion> IEs_sel = new ArrayList<Organizacion>();
            Integer N = 0;
            List<Integer> institucionesSel = new ArrayList<Integer>();
            
            List<Organizacion> organizaciones = null;
            OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("sma.OrganizacionDao");
            organizaciones = organizacionDao.listarInstitucionesxUgel(ugelId);
            N = organizaciones.size();
            
            Integer numIEs = obtenerNumeroIEs(tipUgel, N);
            
            for (int i=0; i<numIEs; i++){
                int numero = (int) (Math.random() * N) + 1;
                institucionesSel.add(numero);
            }
            
            for(int i=0; i<institucionesSel.size(); i++){
                IEs_sel.add(organizaciones.get(i));
            }
            JSONArray data = programarMonitoreos(IEs_sel, idUsuario);
            return WebResponse.crearWebResponseExito("Las instituciones fueron elegidas exitosamente", data);
        }catch (Exception e){
            logger.log(Level.SEVERE,"programarMonitoreoMasivo", e);
            System.out.println("No se pudo programar el monitoreo\n" + e);
            return WebResponse.crearWebResponseError("No se pudo programar el monitoreo", e.getMessage());
        }
    }
    private JSONArray programarMonitoreos(List<Organizacion> IEs_sel, Integer idUsuario) throws Exception{
        List<Docente> docentes = null;
        DocenteDao docenteDao = (DocenteDao)FactoryDao.buildDao("maestro.DocenteDao");
            
        List<Monitoreo> monitoreos = new ArrayList<Monitoreo>();
        List<MonitoreoDetalle> monitoreosDetalle = new ArrayList<MonitoreoDetalle>();
        List<MonitoreoDetalle> monitoreoDetalleInsertados = new ArrayList<MonitoreoDetalle>();
        List<Monitoreo> monitoreosInsertados = new ArrayList<Monitoreo>();
        
        MonitoreoDao monitoreoDao = (MonitoreoDao) FactoryDao.buildDao("sma.MonitoreoDao");
        MonitoreoDetalleDao monitoreoDetalleDao = (MonitoreoDetalleDao) FactoryDao.buildDao("sma.MonitoreoDetalleDao");
        
        List<String> nomMon = new ArrayList<String>();
        nomMon.add("Primera visita: Entrada");
        nomMon.add("Segunda visita: Proceso");
        nomMon.add("Tercera visita: Salida");
        
        List<Character> etaMon = new ArrayList<Character>();
        etaMon.add('1');
        etaMon.add('2');
        etaMon.add('3');
        
        JSONArray salida = new JSONArray();
        
        Integer numDoc = 0;
        for (Organizacion ie: IEs_sel){
            docentes = docenteDao.buscarDocentesIE(ie.getOrgId());
            numDoc = docentes.size();
            
            for(int i=0; i<3; i++){
                Monitoreo monitoreo = null;
                monitoreo = new Monitoreo(nomMon.get(i), ie, 0, numDoc, idUsuario);
                monitoreos.add(monitoreo);
                
                for(int j = 0; j < numDoc; j++){
                    MonitoreoDetalle monitoreoDetalle = null;
                    monitoreoDetalle = new MonitoreoDetalle(monitoreo, docentes.get(j).getDoc_id(), etaMon.get(i), idUsuario);
                    monitoreosDetalle.add(monitoreoDetalle);
                }
            }
        }
        
        for(Monitoreo mon: monitoreos){
            monitoreoDao.insert(mon);
            monitoreosInsertados.add(mon);
        }
        
         for(MonitoreoDetalle monDet: monitoreosDetalle){
            monitoreoDetalleDao.insert(monDet);
            monitoreoDetalleInsertados.add(monDet);
        }
        
        DateFormat sdo = new SimpleDateFormat("dd-MM-yyyy");
        for(Monitoreo mon: monitoreosInsertados){
            
            JSONObject aux = new JSONObject();
            String codNom = "PROMON" + mon.getMniId();
            mon.setMniCod(codNom);
            
            monitoreoDao.update(mon);
            
            aux.put("monId", mon.getMniId());
            aux.put("codMon", mon.getMniCod());
            aux.put("anioMon", mon.getMniYy());
            aux.put("nomMon", mon.getMniNom());
            aux.put("idIE", mon.getOrganizacion().getOrgId());
            aux.put("nomIE", mon.getOrganizacion().getNom());
            aux.put("nivIE", mon.getOrganizacion().getNivDes());
            aux.put("fecReg", sdo.format(mon.getFecReg()));
            aux.put("numEsp", mon.getNumEsp());
            aux.put("numDoc", mon.getNumDoc());
            aux.put("estMon", mon.getEstMon());
            salida.put(aux);
        }
        return salida;
    }
    private Integer obtenerNumeroIEs(String tipUgel, Integer N){
        double zcuadrado = 2.70603;
        double p = 0.5, q = 0.5;
        double ecuadrado = 0.0;
        double resultado = 0.0;
        
        switch (tipUgel){
            case "A": ecuadrado = 0.0025;
                      break;
            case "BC": ecuadrado = 0.0025;
                      break;  
            case "D": ecuadrado = 0.0036;
                      break;
            case "E": ecuadrado = 0.0036;
                      break;
            case "F": ecuadrado = 0.0036;
                      break;
            case "I": ecuadrado = 0.0049;
                      break;
            case "GH": ecuadrado = 0.0049;
                      break;
        }
        
        resultado = (zcuadrado*p*q*N)/((N-1)*ecuadrado + zcuadrado*p*q);
        return (int)resultado;
    }
}
