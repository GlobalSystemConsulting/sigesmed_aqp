/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;

import java.math.BigDecimal;

/**
 *
 * @author Administrador
 */
public class ResultadoMensual {
   private int mes;
   private String nombreMes;
   private BigDecimal debe;
   private BigDecimal haber;
   private BigDecimal saldo;

   public ResultadoMensual( int mes , BigDecimal saldo) {      
        this.mes = mes;
        this.nombreMes =getDatoMes(mes);
        this.saldo = saldo;
        
    }
 
public ResultadoMensual( BigDecimal debe, BigDecimal haber) {      
        this.debe = debe==null? BigDecimal.ZERO :debe;
        this.haber = haber==null? BigDecimal.ZERO :haber;
        this.saldo = (haber==null || debe==null)? null : this.debe.subtract(this.haber).setScale(2);
    }
 
    public ResultadoMensual( int mes,BigDecimal debe, BigDecimal haber) {      
        this.debe = debe;
        this.haber = haber;
        this.mes = mes;
        this.nombreMes = getData(mes);       
        this.saldo = (haber==null || debe==null)? null : debe.subtract(haber).setScale(2);
    }
    private String getData(int mes){
        String des="";
        switch(mes){
            case 0:  des= "Enero";
                     break;                                          
            case 1:  des="Febrero";
                    break;
            case 2:  des="Marzo";
                    break;
            case 3:  des= "Abril";
                     break;                      
            case 4:  des="Mayo";
                     break;
            case 5:  des="Junio";
                     break;
            case 6:  des= "Julio";
                      break;                    
            case 7:  des="Agosto";
                     break;
            case 8:  des="Septiembre";
                     break;
            case 9:  des= "Octubre";
                      break;                  
            case 10: des="Noviembre";
                     break;
            case 11: des="Diciembre";
                     break;
            default: des = "Invalid month";
                     break;
        }                             
         
        return des;
    }
 private String getDatoMes(int mes){
        String des="";
        switch(mes){
            case 1:  des= "Enero";
                     break;                                          
            case 2:  des="Febrero";
                    break;
            case 3:  des="Marzo";
                    break;
            case 4:  des= "Abril";
                     break;                      
            case 5:  des="Mayo";
                     break;
            case 6:  des="Junio";
                     break;
            case 7:  des= "Julio";
                      break;                    
            case 8:  des="Agosto";
                     break;
            case 9:  des="Septiembre";
                     break;
            case 10:  des= "Octubre";
                      break;                  
            case 11: des="Noviembre";
                     break;
            case 12: des="Diciembre";
                     break;
            default: des = "Invalid month";
                     break;
        }                             
         
        return des;
    }
    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getNombreMes() {
        return nombreMes;
    }

    public void setNombreMes(int mes) {
        this.nombreMes = getData(mes);
        this.mes = mes;
        
    }

    public BigDecimal getDebe() {
        return debe;
    }

    public void setDebe(BigDecimal debe) {
        this.debe = debe;
        this.saldo = debe.subtract(this.haber).setScale(2);
    }

    public BigDecimal getHaber() {
        return haber;
    }

    public void setHaber(BigDecimal haber) {
        this.haber = haber;
        this.saldo = debe.subtract(this.haber).setScale(2);
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }
   
   
}
