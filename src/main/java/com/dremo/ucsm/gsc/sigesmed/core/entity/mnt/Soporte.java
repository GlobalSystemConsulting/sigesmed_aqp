/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name="soporte", schema="pedagogico")
public class Soporte implements java.io.Serializable{
    @Id
    @Column(name="sop_id", unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_sop", sequenceName="pedagogico.soporte_sop_id_seq" )
    @GeneratedValue(generator="secuencia_sop")
    private int soporteID;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fun_sis_id")
    private FuncionSistema funcion;
            
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="men_id")
    private Mensaje mensaje;
    
     @Column(name="sop_tip", nullable=false, length=1)
    private char sopTip;
    
//     @Column(name="est_reg", nullable=false, length=1)
//    private char estReg;
     public Soporte(){}
    public Soporte(int soporteID, FuncionSistema funcion, Mensaje mensaje) {
        this.soporteID = soporteID;
        this.funcion = funcion;
        this.mensaje = mensaje;
     
    }

    public char getSopTip() {
        return sopTip;
    }

    public void setSopTip(char sopTip) {
        this.sopTip = sopTip;
    }

    public int getSoporteID() {
        return soporteID;
    }

    public void setSoporteID(int soporteID) {
        this.soporteID = soporteID;
    }

    public FuncionSistema getFuncion() {
        return funcion;
    }

    public void setFuncion(FuncionSistema funcion) {
        this.funcion = funcion;
    }

    public Mensaje getMensaje() {
        return mensaje;
    }

    public void setMensaje(Mensaje mensaje) {
        this.mensaje = mensaje;
    }

//    public char getEstReg() {
//        return estReg;
//    }
//
//    public void setEstReg(char estReg) {
//        this.estReg = estReg;
//    }
     
}
