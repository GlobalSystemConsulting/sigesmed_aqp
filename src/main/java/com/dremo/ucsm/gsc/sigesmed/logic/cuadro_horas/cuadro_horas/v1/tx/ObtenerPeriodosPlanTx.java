/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular.ProgramacionAnual;
import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PeriodosPlanEstudios;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

/**
 *
 * @author abel
 */
public class ObtenerPeriodosPlanTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int uni_did_id ;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            uni_did_id = requestData.getInt("unidadID");
            
             ProgramacionAnualDao prog_an_dao = (ProgramacionAnualDao)FactoryDao.buildDao("maestro.plan.ProgramacionAnualDao");
             PeriodosPlanEstudios per_plan = prog_an_dao.obtenerPeriodoPlan(uni_did_id);
             
              JSONObject oResponse = new JSONObject();
              oResponse.put("tip_per",per_plan.getTip_per());
              oResponse.put("num_per",per_plan.getPerPlaEstId());
              return WebResponse.crearWebResponseExito("Se Obtuvo el Periodo  Correctamente",oResponse);        
            
             
             
             
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Obtener el Periodo ", e.getMessage() ); 

        }
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
