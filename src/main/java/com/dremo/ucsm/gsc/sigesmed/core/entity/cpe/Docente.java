/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.cpe;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Persona;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author carlos
 */
@Entity(name="cpe.Docente")
@Table(name="pedagogico.docente" )
public class Docente implements java.io.Serializable{
    
    @Id
    @Column(name="doc_id", unique=true, nullable=false)
    private int docId;
    @Column(name="cod_mod")
    private String codMod;
    
    @Column(name="niv_mag")
    private String nivMag;
    @Column(name="esp")
    private String esp;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="doc_id", nullable=false,updatable = false,insertable = false)
    private Persona persona;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "docente")
    private List<AsistenciaAula>  asistencias;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "docente")
    private List<PlazaMagisterial>  plazas;
    
    public Docente(){
        
    }
    public Docente(int docId){
        this.docId = docId;        
    }
    public int getDocId(){
        return docId;
    }
    public void setDocId(int docId){
        this.docId = docId;
    }
    
    public String getCodMod(){
        return codMod;
    }
    public void setCodMod(String codMod){
        this.codMod = codMod;
    }
    
    public String getNivMag(){
        return nivMag;
    }
    public void setNivMag(String nivMag){
        this.nivMag = nivMag;
    }
    
    public String getEsp(){
        return esp;
    }
    public void setEsp(String esp){
        this.esp = esp;
    }
    
    public Persona getPersona() {
        return this.persona;
    }
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<AsistenciaAula> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<AsistenciaAula> asistencias) {
        this.asistencias = asistencias;
    }

    public List<PlazaMagisterial> getPlazas() {
        return plazas;
    }

    public void setPlazas(List<PlazaMagisterial> plazas) {
        this.plazas = plazas;
    }
    
    
}
