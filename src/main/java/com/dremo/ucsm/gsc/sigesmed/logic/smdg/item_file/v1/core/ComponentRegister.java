/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.ActualizarItemTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.EliminarItemTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.GuardarObjetivosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.ListarItemsxOrganizacionTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.ListarObjDetalleTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.ListarObjetivosTx;
import com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx.RegistrarItemTx;

/**
 *
 * @author Administrador
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_MONITOREO_DOCUMENTOS_GESTION);        
        
        //Registrando el Nombre del componente
        component.setName("item_file");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        component.addTransactionGET("listarItemsxOrganizacion", ListarItemsxOrganizacionTx.class);
        component.addTransactionGET("listarObjetivos", ListarObjetivosTx.class);
        component.addTransactionGET("listarObjDetalle", ListarObjDetalleTx.class);
        component.addTransactionPUT("actualizarItem", ActualizarItemTx.class);
        component.addTransactionPOST("registrarItem", RegistrarItemTx.class);
        component.addTransactionPUT("eliminarItem", EliminarItemTx.class);
        component.addTransactionPUT("guardarObjetivos", GuardarObjetivosTx.class);
        
        return component;
    }
}

