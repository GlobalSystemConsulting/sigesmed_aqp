/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.se;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Publicacion;
import java.util.List;

/**
 *
 * @author gscadmin
 */
public interface PublicacionDao extends GenericDao<Publicacion>{
    public List<Publicacion> listarxFichaEscalafonaria(int ficEscId);
    public Publicacion buscarPorId(Integer pubId);
}
