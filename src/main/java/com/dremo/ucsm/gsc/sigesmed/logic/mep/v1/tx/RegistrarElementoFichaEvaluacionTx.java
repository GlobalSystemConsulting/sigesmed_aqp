/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ContenidoFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.ElementoFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.EscalaValoracionFicha;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.IndicadorFichaEvaluacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.RangoPorcentualFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class RegistrarElementoFichaEvaluacionTx implements ITransaction{

    private final static Logger logger = Logger.getLogger(RegistrarElementoFichaEvaluacionTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        String tipo = wr.getMetadataValue("tipo");
        JSONObject data = (JSONObject)wr.getData();
        ElementoFicha elementoFicha = null;
        try{
            switch(tipo){
                case MEP.TIPO_ELEMENTO_ESCALA:
                    elementoFicha = new EscalaValoracionFicha(
                            data.getString(MEP.CAMPO_ESCALA_NOMBRE),
                            data.getString(MEP.CAMPO_ESCALA_DESCRIPCION),
                            data.getInt(MEP.CAMPO_ESCALA_VAL)); break;
                case MEP.TIPO_ELEMENTO_RANGO:
                    elementoFicha = new RangoPorcentualFicha(
                            data.getInt(MEP.CAMPO_RANGO_MIN),
                            data.getInt(MEP.CAMPO_RANGO_MAX),
                            data.getString(MEP.CAMPO_RANGO_DESCRIPCION)); break;
                case MEP.TIPO_ELEMENTO_CONTENIDO:
                    elementoFicha = new ContenidoFichaEvaluacion(
                            data.getString(MEP.CAMPO_CONTENIDO_TIPO).charAt(0),
                            data.getString(MEP.CAMPO_CONTENIDO_NOMBRE)); break;
                case MEP.TIPO_ELEMENTO_INDICADOR :
                     IndicadorFichaEvaluacion ife = new IndicadorFichaEvaluacion(
                        data.getString(MEP.CAMPO_INDICADOR_NOMBRE),
                        data.getString(MEP.CAMPO_INDICADOR_DESC),
                        data.optString(MEP.CAMPO_INDICADOR_DOC_VER,null));
                    return registrarIndicador(data.getInt(MEP.CAMPO_INDICADOR_CONTENIDO),ife);
                    
            }
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName() + ": insertElementoFicha",e);
        }
        return registrarElementoFicha(data.getInt(MEP.CAMPO_ELEMENTO_FICHA), elementoFicha);
    }
    private WebResponse registrarElementoFicha(int idFicha,ElementoFicha elementoFicha){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        
        FichaEvaPerslDaoHibernate fevpDao = new FichaEvaPerslDaoHibernate();
        ElementoFicha ef = fevpDao.insertElementoFicha(idFicha, elementoFicha);
        if(ef != null){
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_INS);
            response.setData(new JSONObject(ef.toString()));
        }else{
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_INS);
        }
        return response;
    }
    private WebResponse registrarIndicador(int idContenido,IndicadorFichaEvaluacion ife){
        WebResponse response = new WebResponse();
        response.setScope("web");
        response.setResponseSta(true);
        FichaEvaPerslDaoHibernate fevpDao = new FichaEvaPerslDaoHibernate();
        if(fevpDao.insertIndicadorFichaEvaluacion(idContenido, ife)){
            response.setResponse(MEP.SUCC_RESPONSE_COD);
            response.setResponseMsg(MEP.SUCC_RESPONSE_MESS_INS);
        }else{
            response.setResponse(MEP.ERR_RESPONSE_COD);
            response.setResponseMsg(MEP.ERR_RESPONSE_MESS_INS);
        }
        return response;
    }
}
