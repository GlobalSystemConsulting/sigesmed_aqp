/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.web.BandejaMensaje.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.web.MensajeElectronicoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.web.MensajeElectronico;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
/**
 *
 * @author abel
 */
public class EnviarMensajeTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */        
        
        MensajeElectronico mensaje = new MensajeElectronico();
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        List<Integer> listaDestinatarios = new ArrayList<Integer>();
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            String asunto = requestData.getString("asunto");
            String descripcion = requestData.getString("descripcion");
            /*
            String grupos = requestData.optString("grupos");
            String usuarios = requestData.optString("usuarios");*/
            
            JSONArray destinatarios = requestData.getJSONArray("destinatarios");
            JSONArray documentos = requestData.getJSONArray("documentos");
            
            //por seguridad tiene que haber destinatarios
            if(destinatarios.length()==0){
                System.out.println("No se pudo enviar el mensaje, no hay destinatarios");
                return WebResponse.crearWebResponseError("No se pudo enviar el mensaje, no hay destinatarios" );
            }
            
            String usuarios = "";
            //leendo los destinatarios
            for(int i = 0; i < destinatarios.length();i++){
                JSONObject bo = destinatarios.getJSONObject(i);
                int sessionID = bo.getInt("sessionID");
                listaDestinatarios.add( sessionID );
                usuarios += sessionID +" ";
            }
            
            mensaje = new MensajeElectronico(0, asunto, descripcion, "", usuarios, new Date(), wr.getIdUsuario());
            
            //leendo los documentos adjuntos            
            if(documentos.length() > 0){
                mensaje.setDocumentos( new ArrayList<MensajeDocumento>());
                for(int i = 0; i < documentos.length();i++){
                    JSONObject bo = documentos.getJSONObject(i);

                    String nombreArchivo = "";                    
                    //verificamos si existe un archivo adjunto al mensaje
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,mensaje.getUsuSesId()+"_men_doc_"+(i+1));
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    mensaje.getDocumentos().add( new MensajeDocumento(i+1, mensaje,nombreArchivo) );
                }
            }
        
        }catch(Exception e){
            System.out.println("No se pudo enviar el mensaje al destinatario, datos incorrectos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar el mensaje al destinatario, datos incorrectos", e.getMessage() );
        }
        //Fin       
        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        MensajeElectronicoDao mensajeDao = (MensajeElectronicoDao)FactoryDao.buildDao("web.MensajeElectronicoDao");
        try{
            mensajeDao.enviarMensaje(mensaje,listaDestinatarios);
        }catch(Exception e){
            System.out.println("No se pudo enviar el mensaje al destinatario\n"+e);
            return WebResponse.crearWebResponseError("No se pudo enviar el mensaje al destinatario", e.getMessage() );
        }
        //Fin
        
        //si ya se registro el mensaje
        //ahora creamos los archivos que se adjuntaron
        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64(Mensaje.BANDEJA_MENSAJE_PATH, archivo.getName(), archivo.getData());
        }
        /*
        *  Repuesta Correcta
        */
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        JSONObject oResponse = new JSONObject();
        oResponse.put("mensajeID",mensaje.getMenEleId());
        oResponse.put("fechaEnvio",sf.format(mensaje.getFecEnv()));
        oResponse.put("usuarios",""+mensaje.getLisUsu());
        oResponse.put("grupos",""+mensaje.getLisGru());
        return WebResponse.crearWebResponseExito("El mensaje se envio correctamente",oResponse);
        //Fin
    }    
    
    
}
