/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ficha_escalafonaria", schema="administrativo")

public class FichaEscalafonaria implements Serializable {
    
    @Id
    @Column(name = "fic_esc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ficha_escalafonaria", sequenceName="administrativo.ficha_escalafonaria_fic_esc_id_seq")
    @GeneratedValue(generator="secuencia_ficha_escalafonaria")
    private Integer ficEscId;
    
    @Column(name = "aut_ess", length=15)
    private String autEss;
    
    @Column(name = "sis_pen")
    private String sisPen;
    
    @Column(name = "nom_afp")
    private String nomAfp;
    
    @Column(name = "cod_cuspp", length=12)
    private String codCuspp;
    
    @Column(name = "fec_ing_afp")
    @Temporal(TemporalType.DATE)
    private Date fecIngAfp;
    
    @Column(name = "per_dis")
    private Boolean perDis;
    
    @Column(name = "reg_con")
    private String regCon;
    
    @Column(name = "gru_ocu")
    private Character gruOcu;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "tra_id")
    private Trabajador trabajador;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Ascenso> ascensos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Demerito> demeritos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Desplazamiento> desplazamientos = new HashSet(0);    
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<EstudioComplementario> estudiosComplementarios = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<EstudioPostgrado> estudiosMaestrias = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Exposicion> expocisiones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<FormacionEducativa> formacionesEducativas = new HashSet(0);
      
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Publicacion> publicaciones = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Reconocimiento> reconocimientos = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<Reconocimiento> colegiaturas = new HashSet(0);
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "fichaEscalafonaria")
    Set<LegajoPersonal> legajos = new HashSet(0);

    public FichaEscalafonaria() {
    }
    
    public FichaEscalafonaria(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, Boolean perDis, String regCon, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.autEss = autEss;
        this.perDis = perDis;
        this.regCon = regCon;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, String nomAfp, String codCuspp, Date fecIngAfp, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.autEss = autEss;
        this.sisPen = sisPen;
        this.nomAfp = nomAfp;
        this.codCuspp = codCuspp;
        this.fecIngAfp = fecIngAfp;
        this.perDis = perDis;
        this.regCon = regCon;
        this.gruOcu = gruOcu;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    
    public FichaEscalafonaria(Trabajador trabajador, String autEss, String sisPen, Boolean perDis, String regCon, Character gruOcu, Integer usuMod, Date fecMod, Character estReg) {
        this.trabajador = trabajador;
        this.autEss = autEss;
        this.sisPen = sisPen;
        this.perDis = perDis;
        this.regCon = regCon;
        this.gruOcu = gruOcu;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }


    public int getFicEscId() {
        return ficEscId;
    }

    public void setFicEscId(Integer ficEscId) {
        this.ficEscId = ficEscId;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
    
    public String getAutEss() {
        return autEss;
    }

    public void setAutEss(String autEss) {
        this.autEss = autEss;
    }

    public String getSisPen() {
        return sisPen;
    }

    public void setSisPen(String sisPen) {
        this.sisPen = sisPen;
    }

    public String getNomAfp() {
        return nomAfp;
    }

    public void setNomAfp(String nomAfp) {
        this.nomAfp = nomAfp;
    }

    public String getCodCuspp() {
        return codCuspp;
    }

    public void setCodCuspp(String codCuspp) {
        this.codCuspp = codCuspp;
    }

    public Date getFecIngAfp() {
        return fecIngAfp;
    }

    public void setFecIngAfp(Date fecIngAfp) {
        this.fecIngAfp = fecIngAfp;
    }

    public Character getGruOcu() {
        return gruOcu;
    }

    public void setGruOcu(Character gruOcu) {
        this.gruOcu = gruOcu;
    }
    
    
    
    public Boolean getPerDis() {
        return perDis;
    }

    public void setPerDis(Boolean perDis) {
        this.perDis = perDis;
    }
    
    public String getRegCon() {
        return regCon;
    }

    public void setRegCon(String regCon) {
        this.regCon = regCon;
    }
    
    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public String toString() {
        return "FichaEscalafonaria{" + "ficEscId=" + ficEscId + '}';
    }
   
    
}
