package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "metodologia_curso_capacitacion", schema = "pedagogico")
public class MetodologiaCursoCapacitacion implements Serializable {

    @Id
    @Column(name = "met_cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_metodologia_curso_capacitacion", sequenceName = "pedagogico.metodologia_curso_capacitacion_met_cur_cap_id_seq")
    @GeneratedValue(generator = "secuencia_metodologia_curso_capacitacion")
    private int metCurCapId;

    @Column(name = "nom_act")
    private String nomAct;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cur_cap_id")
    private CursoCapacitacion cursoCapacitacion;

    public MetodologiaCursoCapacitacion() {}

    public MetodologiaCursoCapacitacion(String nomAct) {
        this.nomAct = nomAct;
    }

    public int getMetCurCapId() {
        return metCurCapId;
    }

    public void setMetCurCapId(int metCurCapId) {
        this.metCurCapId = metCurCapId;
    }

    public String getNomAct() {
        return nomAct;
    }

    public void setNomAct(String nomAct) {
        this.nomAct = nomAct;
    }

    public CursoCapacitacion getCursoCapacitacion() {
        return cursoCapacitacion;
    }

    public void setCursoCapacitacion(CursoCapacitacion cursoCapacitacion) {
        this.cursoCapacitacion = cursoCapacitacion;
    }
}
