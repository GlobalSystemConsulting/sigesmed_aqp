/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author CArlos
 */
@Entity(name="com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.DatosNacimiento")
@Table(name = "datos_nacimiento", schema = "pedagogico"
)
public class DatosNacimiento implements java.io.Serializable {

    @Id
    @Column(name = "per_id", unique = true, nullable = false)
    private long perId;
    
    @OneToOne(fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Estudiante estudiante;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pai_id")
    private Pais pais;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_nac")
    private Date fecNac;
    
    @Column(name = "nac_reg")
    private Boolean nacReg;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;
    
    @Column(name = "ubi_cod")
    private String ubiCod;

    public DatosNacimiento() {
    }

    public DatosNacimiento(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public DatosNacimiento(Estudiante estudiante, Pais pais, Date fecNac, Boolean nacReg, Integer usuMod, Date fecMod, Character estReg, String ubiCod) {
        this.estudiante = estudiante;
        this.pais = pais;
        this.fecNac = fecNac;
        this.nacReg = nacReg;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
        this.ubiCod = ubiCod;
    }

    
    public long getPerId() {
        return this.perId;
    }

    public void setPerId(long perId) {
        this.perId = perId;
    }

    
    public Estudiante getEstudiante() {
        return this.estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    
    public Pais getPais() {
        return this.pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    
    public Date getFecNac() {
        return this.fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    
    public Boolean getNacReg() {
        return this.nacReg;
    }

    public void setNacReg(Boolean nacReg) {
        this.nacReg = nacReg;
    }

    
    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    
    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    
    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

   
    public String getUbiCod() {
        return this.ubiCod;
    }

    public void setUbiCod(String ubiCod) {
        this.ubiCod = ubiCod;
    }

}
