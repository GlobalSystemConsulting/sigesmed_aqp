/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;


import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;


/**
 *
 * @author Administrador
 */
@Entity
@Table(name="tipo_propiedad", schema="administrativo")
public class TipoPropiedad {
    
    @Id
    @Column(name="tip_prop_id", unique= true , nullable=false)
    @SequenceGenerator(name="secuencia_det_inv_tra",sequenceName="administrativo.tipo_propiedad_tip_prop_id_seq")
    @GeneratedValue(generator="secuencia_det_inv_tra")
    private int tip_prop_id;
    
    @Column(name="tip_prop_nom")
    private String tip_prop_nom;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public TipoPropiedad() {
    }

    public TipoPropiedad(int tip_prop_id, String tip_prop_nom, int usu_mod, Date fec_mod, char est_reg) {
        this.tip_prop_id = tip_prop_id;
        this.tip_prop_nom = tip_prop_nom;
        this.usu_mod = usu_mod;
        this.fec_mod = fec_mod;
        this.est_reg = est_reg;
    }

    public void setTip_prop_id(int tip_prop_id) {
        this.tip_prop_id = tip_prop_id;
    }

    public void setTip_prop_nom(String tip_prop_nom) {
        this.tip_prop_nom = tip_prop_nom;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getTip_prop_id() {
        return tip_prop_id;
    }

    public String getTip_prop_nom() {
        return tip_prop_nom;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }
    
    
    
    
    
}
