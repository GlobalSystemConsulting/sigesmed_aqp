/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
//import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx.InsertarSerieDocumentalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx.EliminarSerieDocumentalTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx.ListarAreasTx;
import com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx.InsertarAreaTx;

public class ComponentRegister implements IComponentRegister{
    
    @Override
    public WebComponent createComponent() {
        
        // Asignando al modulo al cual pertenece
        WebComponent component = new WebComponent(Sigesmed.SISTEMA_ARCHIVO_DIGITAL);
        
        //Registrando el Nombre del Componente
        component.setName("serie_documental");
        //version del componente
        component.setVersion(1);
        
        //Lista de Operaciones de Logica , propias del componente
        
    //    component.addTransactionPOST("insertarSerieDocumental", InsertarSerieDocumentalTx.class);
        component.addTransactionDELETE("eliminarSerieDocumental", EliminarSerieDocumentalTx.class);
        component.addTransactionGET("listarArea", ListarAreasTx.class);
        component.addTransactionPOST("insertarArea",InsertarAreaTx.class);
        
        
        return component;
       
      ///  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
