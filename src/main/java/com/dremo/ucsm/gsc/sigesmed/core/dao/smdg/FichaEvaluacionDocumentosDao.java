/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaEvaluacionDocumentos;
import java.util.List;

/**
 *
 * @author Administrador
 */
public interface FichaEvaluacionDocumentosDao extends GenericDao<FichaEvaluacionDocumentos>{
    public String buscarUltimoCodigo();
    public List<Object[]> listarFichasxOrganizacion(int orgide);
    public List<Object[]> listarFichasxOrganizacion(int orgide, int iteide);
    public FichaEvaluacionDocumentos buscarxId(int ficId);
    
}
