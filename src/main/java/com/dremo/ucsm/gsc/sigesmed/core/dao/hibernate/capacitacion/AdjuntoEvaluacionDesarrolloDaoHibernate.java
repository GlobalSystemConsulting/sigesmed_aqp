package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.AdjuntoEvaluacionDesarrolloDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.AdjuntoEvaluacionDesarrollo;

public class AdjuntoEvaluacionDesarrolloDaoHibernate extends GenericDaoHibernate<AdjuntoEvaluacionDesarrollo> implements AdjuntoEvaluacionDesarrolloDao {
    
}
