/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.legajo_personal.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.LegajoPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.LegajoPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class EliminarLegajoPorConsecuenciaTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(EliminarLegajoPorConsecuenciaTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {       
         
        Integer codAspOri = 0;
        Character catLeg = ' ', subCat = ' ';
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            codAspOri = requestData.getInt("codAspOri");
            catLeg = requestData.getString("catLeg").charAt(0);  
            subCat = requestData.getString("subCat").charAt(0); 
        
        }catch(Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"eliminarLegajo",e);
            return WebResponse.crearWebResponseError("No se pudo eliminar", e.getMessage() );
        }
        
        LegajoPersonalDao legajoPersonalDao = (LegajoPersonalDao)FactoryDao.buildDao("se.LegajoPersonalDao");
        try{
            legajoPersonalDao.delete(new LegajoPersonal(catLeg, subCat, codAspOri));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el legajo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el legajo", e.getMessage() );
        }
        return WebResponse.crearWebResponseExito("El legajo se elimino correctamente");
    
    }
    
}
