package com.dremo.ucsm.gsc.sigesmed.core.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="usuario_session" )
public class UsuarioSessionPersona  implements java.io.Serializable {

    
    private int usuSesId;    
    private Persona persona;    
    private Date fecCre;    
    private char estReg;

    public UsuarioSessionPersona() {
    }
    public UsuarioSessionPersona(int usuSesId) {
        this.usuSesId = usuSesId;
    }
    @Id 
    @Column(name="usu_ses_id", unique=true, nullable=false) 
    public int getUsuSesId() {
        return this.usuSesId;
    }    
    public void setUsuSesId(int usuSesId) {
        this.usuSesId = usuSesId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_cre", nullable=false, length=29,updatable = false)
    public Date getFecCre() {
        return this.fecCre;
    }
    
    public void setFecCre(Date fecCre) {
        this.fecCre = fecCre;
    }

    @Column(name="est_reg", nullable=false, length=1)
    public char getEstReg() {
        return this.estReg;
    }
    
    public void setEstReg(char estReg) {
        this.estReg = estReg;
    }
    @ManyToOne
    @JoinColumn(name="usu_id", nullable=false,updatable = false,insertable = false)
    public Persona getPersona() {
        return this.persona;
    }
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    @Transient
    public String getNombrePersona() {
        return this.persona.getNombrePersona();
    }

}