/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.ValorContable;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.MovimientoIngresos;

import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.ValorContableDAO;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.MovimientoIngresosDAO;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.text.SimpleDateFormat;

/**
 *
 * @author Administrador
 */


public class RegistrarBienMuebleTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject requestData = (JSONObject)wr.getData();
        MovimientoIngresos mov_ing = null;
        BienesMuebles bm = null;
        DetalleTecnicoMuebles dtm = null;
        ValorContable vc = null;
        
        try{
            
        /*Datos del Movimiento*/
        JSONObject mov = requestData.getJSONObject("movimiento");    
        int tipo_mov_ing = mov.getInt("tip_mov_ing");
        int con_pat_id = mov.getInt("con_pat_id");
        String num_res = mov.getString("num_res");
     //   Date fec_res = formato2.parse(mov.getString("fec_res"));
        Date fec_mov = new Date();
        String obs = mov.getString("obs");
        
        
       
        
        /*Cabecera del Bien Mueble*/
        
        int cat_bie_id = requestData.getInt("cat_bie_id");
        int amb_id     = requestData.getInt("amb_id");
        int an_id      = requestData.getInt("an_id");
        String des_bie = requestData.getString("des_bie");
        Date fec_reg   = new Date();
        String cod_int = requestData.getString("cod_int");
        int cantidad   = requestData.getInt("cant");
        String estado_bie = requestData.getString("cond");
     //   FileJsonObject doc_bie = new FileJsonObject( requestData.getJSONObject("rut_doc_bie") );
   //     String rut_doc_bie = doc_bie.getName();
         String cod_ba_bie  = requestData.getString("cod_ba_bie");
        int usu_mod        = requestData.getInt("usu_mod");
        String verificar = requestData.getString("verificar");
        Date fec_mod    = new Date();
        int org_id = requestData.getInt("org_id");
        char est_reg = 'A';
    
        
       
        /*Registramos el Detalle Tecnico */
        
        String marc = requestData.optString("marc");
        String mod  = requestData.optString("mod");
        String cond = requestData.optString("cond");
        int dim     = requestData.optInt("dim");
        String ser  = requestData.optString("ser");
        String col  = requestData.optString("col");
        String tip  = requestData.optString("tip");
        String nro_mot = requestData.optString("nro_mot");
        String nro_pla = requestData.optString("nro_pla");
        String nro_cha  = requestData.optString("nro_cha");
        String raza     = requestData.optString("raza");
        int edad        = requestData.optInt("edad");
        
        JSONObject doc_ref_obj = requestData.optJSONObject("rut_doc_bie");
        JSONObject rut_imag_1_obj = requestData.optJSONObject("rut_imag_1");
        JSONObject rut_imag_2_obj = requestData.optJSONObject("rut_imag_2");
        JSONObject rut_aut_img_obj = requestData.optJSONObject("rut_aut_img");
        
      //  FileJsonObject doc_ref = new FileJsonObject(requestData.optJSONObject("doc_ref"));
      //   FileJsonObject imag_1 = new FileJsonObject( requestData.optJSONObject("rut_imag_1") );
     //   FileJsonObject imag_2 = new FileJsonObject( requestData.optJSONObject("rut_imag_2") );
     //   FileJsonObject aut_imag = new FileJsonObject( requestData.optJSONObject("rut_aut_img") );
        String rut_imag_1 = "";
        String rut_imag_2 = "";
        String rut_aut_imag = "";
        String rut_doc_ref = "";
        FileJsonObject doc_ref = null;
        FileJsonObject imag_1 = null;        
        FileJsonObject imag_2= null;
        FileJsonObject aut_imag = null;        
        if(doc_ref_obj!=null){
            doc_ref = new FileJsonObject(doc_ref_obj);
            rut_doc_ref = doc_ref.getName();
        //    BuildFile.buildFromBase64("scp",rut_doc_ref, doc_ref.getData());
        }
        
        if(rut_imag_1_obj!=null){
             imag_1 = new FileJsonObject(rut_imag_1_obj);
             rut_imag_1   = imag_1.getName();
             if(verificar_imagen(rut_imag_1)!=true){
                 
                  return WebResponse.crearWebResponseError("ERROR: Formato de Imagen Adjunta No Valido" );
             } 
        }
        if(rut_imag_2_obj!=null){
             imag_2 = new FileJsonObject(rut_imag_2_obj);
            
            rut_imag_2   = imag_2.getName();
            if(verificar_imagen(rut_imag_2)!=true){
                
                 return WebResponse.crearWebResponseError("ERROR: Formato de Imagen Adjunta No Valido" );
            }
        }
        if(rut_aut_img_obj!=null){
            aut_imag = new FileJsonObject(rut_aut_img_obj);
            rut_aut_imag = aut_imag.getName();
            if(verificar_imagen(rut_aut_imag)!=true){
                
                 return WebResponse.crearWebResponseError("ERROR: Formato de Imagen Adjunta No Valido");
            }
        }
        
        /*Construimos los Archivos*/
       if(doc_ref!=null && imag_1!=null && imag_2!=null && aut_imag!=null){
            BuildFile.buildFromBase64("scp",rut_doc_ref,doc_ref.getData());
            BuildFile.buildFromBase64("scp",rut_imag_1 , imag_1.getData());
            BuildFile.buildFromBase64("scp",rut_imag_2 , imag_2.getData());
            BuildFile.buildFromBase64("scp",rut_aut_imag , aut_imag.getData());
        
       }
        
        BienesMueblesDAO bienes_dao = (BienesMueblesDAO)FactoryDao.buildDao("scp.BienesMueblesDAO");     
        bm = new BienesMuebles(0,cat_bie_id,amb_id,an_id,cantidad,des_bie,fec_reg,cod_int,estado_bie,rut_doc_ref,cod_ba_bie,usu_mod,fec_mod,org_id,est_reg);
        bm.setCon_pat_id(con_pat_id);
        bm.setVerificar(verificar);
       
       
       
         /*Registramos la Cuenta Asociada al Bien Mueble*/
        String cod_cue = requestData.getString("cod_cuenta");
        int val_con    = requestData.getInt("valor_cont");
        char act_dep;
        boolean activo   = requestData.getBoolean("act_dep");
        if(activo){act_dep='A';}
        else{act_dep='N';}
        
        
        boolean tipo_registro = requestData.getBoolean("actualizar_bien");
        /*ACTUALIZAR*/ //actualizar=true , insertar=false
        if(tipo_registro == true){
            
            /*Actualizamos la ruta de los archivos*/
            bm.setRut_doc_bie(get_nombre_archivo(requestData.optString("doc_referencia")));
            
            
            Date fec_res = formato2.parse(mov.getString("fec_res"));
            int mov_ing_id = requestData.getInt("mov_ing_id");
            mov_ing = new MovimientoIngresos(mov_ing_id,fec_mov,num_res,fec_res,usu_mod,est_reg,con_pat_id);
            mov_ing.setTip_mov_ing_id(tipo_mov_ing);
            mov_ing.setObs(obs);
            MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO"); 
            mov_ing_dao.update(mov_ing);

            int bm_id = requestData.getInt("id_bien");
            bm.setCod_bie(bm_id);
            
            bm.setMov_ing_id(mov_ing.getMov_ing_id());
            bienes_dao.update(bm);
            dtm = new DetalleTecnicoMuebles(bm.getCod_bie(),marc,mod,cond,dim,ser,col,tip,nro_mot,nro_pla,nro_cha,raza,edad,rut_imag_1,rut_imag_2,rut_aut_imag,est_reg);
            
            dtm.setRut_imag_1(get_nombre_archivo(requestData.optString("imag_1")));
            dtm.setRut_imag_2(get_nombre_archivo(requestData.optString("imag_2")));
            dtm.setRut_aut_img(get_nombre_archivo(requestData.optString("autopartes")));
            
            DetalleTecnicoMueblesDAO det_tec_dao = (DetalleTecnicoMueblesDAO)FactoryDao.buildDao("scp.DetalleTecnicoMueblesDAO");
            det_tec_dao.update(dtm);
            int val_cont_id = requestData.getInt("val_cont_id");
            vc = new ValorContable(val_cont_id,est_reg,bm.getCod_bie(),act_dep,cod_cue,val_con);
            ValorContableDAO val_cont_dao = (ValorContableDAO)FactoryDao.buildDao("scp.ValorContableDAO");     
            val_cont_dao.update(vc);
            
            
            
        }else{
            /*INSERT*/
        Date fec_res = formatter.parse(mov.getString("fec_res"));    
        mov_ing = new MovimientoIngresos(0,fec_mov,num_res,fec_res,usu_mod,est_reg,con_pat_id);
        mov_ing.setTip_mov_ing_id(tipo_mov_ing);
        mov_ing.setObs(obs);
        MovimientoIngresosDAO mov_ing_dao = (MovimientoIngresosDAO)FactoryDao.buildDao("scp.MovimientoIngresosDAO"); 
        mov_ing_dao.insert(mov_ing);
        
        bm.setMov_ing_id(mov_ing.getMov_ing_id());
        bienes_dao.insert(bm);
        dtm = new DetalleTecnicoMuebles(bm.getCod_bie(),marc,mod,cond,dim,ser,col,tip,nro_mot,nro_pla,nro_cha,raza,edad,rut_imag_1,rut_imag_2,rut_aut_imag,est_reg);   
         
        DetalleTecnicoMueblesDAO det_tec_dao = (DetalleTecnicoMueblesDAO)FactoryDao.buildDao("scp.DetalleTecnicoMueblesDAO");
        det_tec_dao.insert(dtm);
        
        vc = new ValorContable(0,est_reg,bm.getCod_bie(),act_dep,cod_cue,val_con);
        ValorContableDAO val_cont_dao = (ValorContableDAO)FactoryDao.buildDao("scp.ValorContableDAO");     
        val_cont_dao.insert(vc);
            
        }
        
        
        
        
        
        }
        catch(Exception e){
             System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Bien Mueble , datos incorrectos", e.getMessage() );
        }
          JSONObject oResponse = new JSONObject();
          oResponse.put("cod_bie",bm.getCod_bie());
          return WebResponse.crearWebResponseExito("El registro del Bien y Detalle Tecnico se realizo correctamente",oResponse);
 
    }
    //Funcion que verifica la extension de la imagen (.PNG , .JPEG)
    public boolean verificar_imagen(String imag){
        int size = imag.length();
        String jpg = ".jpg";String png=".png";String bmp=".bmp";
        String format = "";
        for(int i = 0 ; i<size ; i++){
            if(imag.charAt(i)=='.'){
                format = imag.substring(i);
                break;
            }
        }
        if(format.equals(jpg) | format.equals(png) | format.equals(format)){
            return true;
        }
        else{return false;}
    }
    
    public String get_nombre_archivo(String path){
        
        if(!path.equals("")){return path.substring(13);}
        else{return "";}
    }
    
    
    
}
