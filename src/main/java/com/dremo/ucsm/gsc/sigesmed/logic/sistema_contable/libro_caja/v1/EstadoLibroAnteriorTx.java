/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentaCorriente;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.CuentasEfectivo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class EstadoLibroAnteriorTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        LibroCaja libro=null;
        int organizacionID = 0;
       
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();           
            organizacionID = requestData.getInt("organizacionID");
           
           
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo obtener la organizacion a la que pertenece", e.getMessage() );
        }
        
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
       
       
        try {
            libro= libroDao.estadoLibroAnterior(organizacionID);
            
        } catch (Exception e) {
            System.out.println("No se pudo encontrar Libro\n"+e);
            return WebResponse.crearWebResponseError("No se pudo encontrar Libro", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */ 
         
         JSONObject oResponse = new JSONObject(); 
         JSONArray cuentasEfectivo = new JSONArray();
        if(libro!=null){  
            
            for(CuentasEfectivo cE:libro.getCuentasEfectivos()){
              JSONObject cuenta = new JSONObject();
                cuenta.put("nombre",cE.getCuenta().getNomCue());
                cuenta.put("numero",cE.getCuenta().getNumCue());
                cuenta.put("importe",cE.getSalAct());
                for(CuentaCorriente cC:libro.getCuentaCorrientes()){
                    if(cC.getCuenta().getNumCue()==cE.getCuenta().getNumCue()){
                        cuenta.put("flag",true);  
                        cuenta.put("banco",cC.getBanco().getRazSoc());  
                        cuenta.put("numeroCuenta",cC.getNum()); 
                    }
                    else{
                        cuenta.put("flag",false); 
                    }
                }
                 cuentasEfectivo.put(cuenta);
            }
            
             oResponse.put("saldo",libro.getSalAct()); 
             oResponse.put("cuentasEfectivo",cuentasEfectivo); 

         }                           
        return WebResponse.crearWebResponseExito("Se Listo correctamente",oResponse);        
        //Fin
    }
    
}
