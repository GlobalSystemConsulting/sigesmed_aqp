/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.ficha_evaluacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.FichaDetalleDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaGrupoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.FichaDetalle;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTx;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ImprimirFichaTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        
        JSONObject requestData = (JSONObject)wr.getData();
        int ficId = requestData.getInt("ficide");
        int plaid = requestData.getInt("plaid");
        JSONObject evaluador = requestData.getJSONObject("evaluador");
                
        PlantillaFichaInstitucional plantilla = null;
        PlantillaFichaInstitucionalDao plantillasDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");        
        
        try{
            plantilla = plantillasDao.obtenerPlantilla(plaid);
        }catch(Exception e){                    
            return WebResponse.crearWebResponseError("No se encontro la plantilla", e.getMessage() );
        }
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        
        List<PlantillaGrupo> grupos = plantilla.getGrupos();
        
        int sumaGrupo = 0;
        int sumaTotal = 0;
        
        PlantillaGrupoDao grupoDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");        
                
        Table tablaEvaluador = new Table(6);
        tablaEvaluador.setFontSize(8);
        
        Table tablaCuerpo = new Table(6);
        tablaCuerpo.setFontSize(8);
        
        Table tablaResultados = new Table(6);
        tablaResultados.setFontSize(8);
                
        try{

            tablaEvaluador.addCell(new Cell(1, 6).add("Codigo: " + plantilla.getPfiCod()).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 6).add("Nombre: " + plantilla.getPfiNom()).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 6).add("Tipo: " + plantilla.getTipo().getPtiNom()).setBorder(Border.NO_BORDER));
            
            tablaEvaluador.addCell(new Cell(1, 2).add("Aplicador cargo: " + evaluador.optString("cargo")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 2).add("Nombre de Aplicador: " + evaluador.optString("nombres")).setBorder(Border.NO_BORDER));
            tablaEvaluador.addCell(new Cell(1, 2).add("Fecha de Evaluacion: " + evaluador.optString("fecha")).setBorder(Border.NO_BORDER));
            
            int numInd = 0;
            
            Map<String,Integer> grupun = new LinkedHashMap<String, Integer>();
            
            for(PlantillaGrupo g:grupos){
                tablaCuerpo.addCell(new Cell(1, 5).add(g.getPgrDes()).setBackgroundColor(Color.LIGHT_GRAY));
                tablaCuerpo.addCell(new Cell(1, 1).add("Puntaje").setBackgroundColor(Color.LIGHT_GRAY));               
                List<PlantillaIndicadores> indicadores = grupoDao.obtenerIndicadores(g.getPgrId());
                
                sumaGrupo = 0;
                
                for(PlantillaIndicadores i:indicadores){
                    tablaCuerpo.addCell(new Cell(1, 5).add("   " + i.getPinDes()));
                    
                    FichaDetalle detalle = null;
                    FichaDetalleDao detalleDao = (FichaDetalleDao)FactoryDao.buildDao("smdg.FichaDetalleDao");        

                    detalle = detalleDao.obtenerDetalle(i.getPinId(), ficId);
                                        
                    tablaCuerpo.addCell(new Cell(1, 1).add(detalle.getFdePun() + ""));
                    
                    sumaGrupo += detalle.getFdePun();
                    numInd++;
                    
                }
                sumaTotal += sumaGrupo;
                grupun.put(g.getPgrDes(), sumaGrupo);
                
            }
            
            tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add("Suma Total: ").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add(sumaTotal+"").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 6).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 2).add("Resultados por Indicadores:").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 4).add("").setBorder(Border.NO_BORDER));
            
            double maxPunt = numInd * 3;            
            for (Entry<String, Integer> gp : grupun.entrySet()){
                tablaResultados.addCell(new Cell(1, 2).add(gp.getKey()).setBorder(Border.NO_BORDER));                
                tablaResultados.addCell(new Cell(1, 1).add(toPercentage(gp.getValue()*100/maxPunt) + " %").setBorder(Border.NO_BORDER));
                tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            }
            
            tablaResultados.addCell(new Cell(1, 1).add("").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add("Total(%): ").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 1).add(toPercentage(sumaTotal*100/maxPunt) + " %").setBorder(Border.NO_BORDER));
            tablaResultados.addCell(new Cell(1, 3).add("").setBorder(Border.NO_BORDER));
            
            
        }catch(Exception e){
            System.out.println(e);  
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext(Boolean.FALSE);
                        
            r.agregarTitulo("REPORTE DE FICHA DE EVALUACION");
            r.newLine(1);
            r.agregarTabla(tablaEvaluador);
            r.newLine(1);
            r.agregarTabla(tablaCuerpo);
            r.newLine(1);
            r.agregarTabla(tablaResultados);
            
            r.cerrarDocumento();
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
    
    public String toPercentage(double d){
        String result = String.valueOf(new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP));
        return result;
    }
}
