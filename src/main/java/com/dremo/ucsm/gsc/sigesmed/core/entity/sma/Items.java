/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "items", schema = "pedagogico")

public class Items implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ite_id")
    private Integer iteId;
    
    @Column(name = "ite_des")
    private String iteDes;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.DATE)
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg")
    private String estReg;
    
    @JoinColumn(name = "cge_id", referencedColumnName = "cge_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CompromisosGestion cgeId;

    

    public Items() {
    }

    public Items(Integer iteId) {
        this.iteId = iteId;
    }
    
    public Items(String iteDes, CompromisosGestion cgeId) {        
        this.iteDes = iteDes;
        this.cgeId = cgeId;
    }
    
    public Items(Integer iteId, CompromisosGestion cgeId) {        
        this.iteId = iteId;
        this.cgeId = cgeId;
    }
    
    public Items(Integer iteId, CompromisosGestion cgeId, String iteDes) {
        this.iteId = iteId;
        this.cgeId = cgeId;
        this.iteDes = iteDes;
    }
    

    public Integer getIteId() {
        return iteId;
    }

    public void setIteId(Integer iteId) {
        this.iteId = iteId;
    }

    public String getIteDes() {
        return iteDes;
    }

    public void setIteDes(String iteDes) {
        this.iteDes = iteDes;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public CompromisosGestion getCgeId() {
        return cgeId;
    }

    public void setCgeId(CompromisosGestion cgeId) {
        this.cgeId = cgeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iteId != null ? iteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Items)) {
            return false;
        }
        Items other = (Items) object;
        if ((this.iteId == null && other.iteId != null) || (this.iteId != null && !this.iteId.equals(other.iteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.Items[ iteId=" + iteId + " ]";
    }
    
}
