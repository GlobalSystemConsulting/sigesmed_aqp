/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.experiencia_laboral.desplazamiento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.se.DesplazamientoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.se.Desplazamiento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Yemi
 */
public class ActualizarDesplazamientoTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ActualizarDesplazamientoTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try{
     
            DateFormat sdi = new SimpleDateFormat("yyyy-MM-dd");
            
            JSONObject requestData = (JSONObject)wr.getData();
        
            Integer desId = requestData.getInt("desId");
            Character tip = requestData.getString("tip").charAt(0);
            String numRes = requestData.getString("numRes");
            Date fecRes = sdi.parse(requestData.getString("fecRes").substring(0, 10));
            String insEdu = requestData.getString("insEdu");
            String car = requestData.getString("car");
            String jorLab = requestData.getString("jorLab");
            Date fecIni = sdi.parse(requestData.getString("fecIni").substring(0, 10));
            Date fecTer = sdi.parse(requestData.getString("fecTer").substring(0, 10));
            
            return actualizarDesplazamiento(desId, tip, numRes, fecRes, insEdu, car, jorLab, fecIni, fecTer);
        }catch (Exception e){
            System.out.println(e);
            logger.log(Level.SEVERE,"Actualizar desplazamientoa",e);
            return WebResponse.crearWebResponseError("No se pudo actualizar, datos incorrectos", e.getMessage());
        } 
    }
    
    private WebResponse actualizarDesplazamiento(Integer desId, Character tip, String numRes, Date fecRes,
            String insEdu, String car, String jorLab, Date fecIni, Date fecTer) {
        try{
            DesplazamientoDao desplazamientoDao = (DesplazamientoDao)FactoryDao.buildDao("se.DesplazamientoDao");        
            Desplazamiento desplazamiento = desplazamientoDao.buscarPorId(desId);

            desplazamiento.setTip(tip);
            desplazamiento.setNumRes(numRes);
            desplazamiento.setFecRes(fecRes);
            desplazamiento.setInsEdu(insEdu);
            desplazamiento.setCar(car);
            desplazamiento.setJorLab(jorLab);
            desplazamiento.setFecIni(fecIni);
            desplazamiento.setFecTer(fecTer);
            
            desplazamientoDao.update(desplazamiento);

            JSONObject oResponse = new JSONObject();
            DateFormat sdo = new SimpleDateFormat("yyyy-MM-dd");
            oResponse.put("desId", desplazamiento.getDesId());
            oResponse.put("tip", desplazamiento.getTip());
            oResponse.put("tipDes", "");
            oResponse.put("numRes", desplazamiento.getNumRes());
            oResponse.put("fecRes", sdo.format(desplazamiento.getFecRes()));
            oResponse.put("insEdu", desplazamiento.getInsEdu());
            oResponse.put("car", desplazamiento.getCar());
            oResponse.put("jorLab", desplazamiento.getJorLab());
            oResponse.put("fecIni", sdo.format(desplazamiento.getFecIni()));
            oResponse.put("fecTer", sdo.format(desplazamiento.getFecTer()));
            return WebResponse.crearWebResponseExito("Desplazamiento actualizado exitosamente",oResponse);
            
        }catch (Exception e){
            logger.log(Level.SEVERE,"actualizarDesplazamiento",e);
            return WebResponse.crearWebResponseError("Error, el desplazamiento no fue actualizad");
        }
    } 
    
}
