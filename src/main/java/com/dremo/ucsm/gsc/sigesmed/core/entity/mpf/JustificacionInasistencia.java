/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;


import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author Carlos
 */
@Entity(name = "JustificacionInasistenciaMpf")
@Table(name="justificacion_inasistencia", schema="pedagogico")
@DynamicUpdate(value = true)
public class JustificacionInasistencia implements java.io.Serializable{
    @Id
    @Column(name="asi_est_id")
    private Long justificacionId;
    
    @OneToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="asi_est_id")
    private AsistenciaEstudiante asistencia;
    
    @Column(name="des_jus", length = 20)
    private String desJus;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="fec_jus", length = 20)
    private Date fecJus;
    
    @Column(name="tip_doc_jus", length = 40)
    private String tipDocJus;
    
    @Column(name="doc_adj", length = 40)
    private String docAdjJus;

    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public JustificacionInasistencia(AsistenciaEstudiante asistencia) {
        this.asistencia = asistencia;
        this.fecMod = new Date();
        this.estReg= 'A';
    }

    public JustificacionInasistencia(AsistenciaEstudiante asistencia,String desJus, Date fecJus) {

        this.asistencia = asistencia;
        this.desJus = desJus;
        this.fecJus = fecJus;
        this.docAdjJus = docAdjJus;
        this.fecMod = new Date();
        this.estReg= 'A';
    }

    public Long getJustificacionId() {
        return justificacionId;
    }

    public void setJustificacionId(Long justificacionId) {
        this.justificacionId = justificacionId;
    }

    public AsistenciaEstudiante getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(AsistenciaEstudiante asistencia) {
        this.asistencia = asistencia;
    }

    public String getDesJus() {
        return desJus;
    }

    public void setDesJus(String desJus) {
        this.desJus = desJus;
    }

    public Date getFecJus() {
        return fecJus;
    }

    public void setFecJus(Date fecJus) {
        this.fecJus = fecJus;
    }

    public String getTipDocJus() {
        return tipDocJus;
    }

    public void setTipDocJus(String tipDocJus) {
        this.tipDocJus = tipDocJus;
    }

    public String getDocAdjJus() {
        return docAdjJus;
    }

    public void setDocAdjJus(String docAdjJus) {
        this.docAdjJus = docAdjJus;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }



}
