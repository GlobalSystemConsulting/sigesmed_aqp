/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarTipoDocumentoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TipoDocumento> tipoDocumentos = null;
        TipoDocumentoDao documentoDao = (TipoDocumentoDao)FactoryDao.buildDao("std.TipoDocumentoDao");
        try{
            tipoDocumentos = documentoDao.buscarTodos(TipoDocumento.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Tipo de Documentos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Tipos de Documentos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoDocumento documento:tipoDocumentos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoDocumentoID",documento.getTipDocId() );
            oResponse.put("nombre",documento.getNom());
            oResponse.put("descripcion",documento.getDes());
            oResponse.put("fecha",documento.getFecMod().toString());
            oResponse.put("estado",""+documento.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}

