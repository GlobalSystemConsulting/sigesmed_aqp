package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi;

import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.EstudianteMMI;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class DomicilioDaoHibernate extends GenericMMIDaoHibernate<Domicilio> {

    public List<Domicilio> find4Estudiante(long estCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Domicilio> domicilios = null;
        String hql;
        Query query;
        try {
            hql = "FROM Domicilio dom WHERE dom.estReg != 'E' and dom.id.perId = :hdlEstCod";
            query = session.createQuery(hql);
            query.setLong("hdlEstCod", estCod);
            domicilios = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar Domicilios con codigo estudiante: " + estCod);
            throw ex;
        } finally {
            session.close();
        }
        return domicilios;
    }
    
    public List<Domicilio> domiciliosByEstIdOrdered(long estCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Domicilio> domicilios = null;
        String hql;
        Query query;
        try {
            hql = "FROM Domicilio dom WHERE dom.estReg != 'E' and dom.id.perId = :hdlEstCod ORDER BY dom.fecMod DESC";
            query = session.createQuery(hql);
            query.setLong("hdlEstCod", estCod);
            domicilios = query.list();
        } catch (Exception ex) {
            System.out.println("Error al buscar Domicilios con codigo estudiante: " + estCod);
            throw ex;
        } finally {
            session.close();
        }
        return domicilios;
    }

    public long llaveDomicilio(long perId) {
        Class dato = Domicilio.class;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object result = null;
        String hql;
        Query query;
        try {
            hql = "SELECT MAX(dom.id.domId) FROM " + dato.getName() + " dom WHERE dom.id.perId = :hqlPerId";
            query = session.createQuery(hql);
            query.setLong("hqlPerId", perId);
            query.setMaxResults(1);
            result = query.uniqueResult();
        } catch (Exception ex) {
            System.out.println("No se puede ejecutar llave Domicilio por Persona " + dato.getClass().getName() + ".\n" + ex.getMessage());
            throw ex;
        } finally {
            session.close();
        }
        if (result == null) {
            return (long) 0;
        } else {
            return Long.parseLong(result.toString());
        }
    }

}
