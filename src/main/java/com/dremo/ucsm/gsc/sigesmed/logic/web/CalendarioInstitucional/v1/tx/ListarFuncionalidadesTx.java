package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FuncionSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.SubModuloSistemaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.SubModuloSistema;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarFuncionalidadesTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(ListarFuncionalidadesTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarModulos();
                break;

            case 1:
                response = listarSubModulos(data.getInt("cod"));
                break;

            case 2:
                response = listarFuncionalidades(data.getInt("cod"), data.getInt("rol"));
                break;
        }

        return response;
    }

    private WebResponse listarModulos() {
        try {
            ModuloSistemaDao moduloSistemaDao = (ModuloSistemaDao) FactoryDao.buildDao("ModuloSistemaDao");
            List<ModuloSistema> modules = moduloSistemaDao.listarModulos();
            JSONArray array = new JSONArray();

            for (ModuloSistema module : modules) {
                JSONObject object = new JSONObject();
                object.put("cod", module.getModSisId());
                object.put("nom", module.getNom());
                array.put(object);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarModulos", e);
            return WebResponse.crearWebResponseError("Error al listar los módulos activos", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarSubModulos(int modCod) {
        try {
            SubModuloSistemaDao subModuloSistemaDao = (SubModuloSistemaDao) FactoryDao.buildDao("SubModuloSistemaDao");
            List<SubModuloSistema> subModules = subModuloSistemaDao.listarSubModulos(modCod);
            JSONArray array = new JSONArray();

            for (SubModuloSistema subModule : subModules) {
                JSONObject object = new JSONObject();
                object.put("cod", subModule.getSubModSisId());
                object.put("nom", subModule.getNom());
                array.put(object);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarSubModulos", e);
            return WebResponse.crearWebResponseError("Error al listar los submódulos activos", WebResponse.BAD_RESPONSE);
        }
    }

    private WebResponse listarFuncionalidades(int subModCod, int rolCod) {
        try {
            FuncionSistemaDao funcionSistemaDao = (FuncionSistemaDao) FactoryDao.buildDao("FuncionSistemaDao");
            List<FuncionSistema> functions = funcionSistemaDao.listarFunciones(subModCod, rolCod);
            JSONArray array = new JSONArray();

            for (FuncionSistema function : functions) {
                JSONObject object = new JSONObject();
                object.put("cod", function.getFunSisId());
                object.put("nom", function.getNom());
                array.put(object);
            }
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarSubModulos", e);
            return WebResponse.crearWebResponseError("Error al listar los submódulos activos", WebResponse.BAD_RESPONSE);
        }
    }
}
