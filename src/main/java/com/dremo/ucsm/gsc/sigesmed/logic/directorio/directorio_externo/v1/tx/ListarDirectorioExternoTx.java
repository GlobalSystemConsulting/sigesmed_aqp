/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.directorio_externo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.di.DirectorioExternoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.DirectorioExterno;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class ListarDirectorioExternoTx implements ITransaction{
        
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<DirectorioExterno> direxs = null;
        DirectorioExternoDao direx = (DirectorioExternoDao)FactoryDao.buildDao("di.DirectorioExternoDao");
        try{
            direxs = direx.buscarTodos(DirectorioExterno.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los Directorios Externos\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Directorios Externos", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(DirectorioExterno d:direxs ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("dexId",d.getDexId());
            oResponse.put("dexDir",d.getDexDir());            
            oResponse.put("dexEma",d.getDexEma());            
            oResponse.put("dexNom",d.getDexNombre());
            oResponse.put("dexTel",d.getDexTel());
            oResponse.put("estReg",d.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
}
