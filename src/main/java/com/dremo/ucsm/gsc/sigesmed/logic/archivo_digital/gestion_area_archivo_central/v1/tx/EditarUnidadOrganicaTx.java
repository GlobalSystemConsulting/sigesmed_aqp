/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public class EditarUnidadOrganicaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        UnidadOrganica uni_org = null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            int uni_org_id = requestData.getInt("uni_org_id");
            int areaID = requestData.getInt("codigo_area");
            String abr = requestData.getString("abrev");
            String nombre = requestData.getString("nom_uni");
            String des = requestData.getString("descripcion");
            
            uni_org = new UnidadOrganica(uni_org_id,areaID,abr,nombre,des);
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar la Unidad Organica, datos incorrectos", e.getMessage() ); 
        }
         UnidadOrganicaDAO uni_org_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("UnidadOrganicaDAO");
        try{
            uni_org_dao.update(uni_org);
        }catch(Exception e){
             System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo editar la Unidad Organica ", e.getMessage() );
        } 
        return  WebResponse.crearWebResponseExito("La Unidad Organica se pudo editar correctamente");
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
