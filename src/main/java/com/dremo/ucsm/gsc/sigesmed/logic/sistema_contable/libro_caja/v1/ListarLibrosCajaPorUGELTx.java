/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.LibroCajaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.LibroCaja;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.Tesorero;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarLibrosCajaPorUGELTx implements ITransaction{
    
     @Override
    public WebResponse execute(WebRequest wr){
        //Parte de la operacion con la Base de Datos
        
        JSONObject requestData = (JSONObject)wr.getData();
        int organizacionID = requestData.getInt("organizacionID");
        int fecha= requestData.getInt("fechaControl");
         
     
      
        
        List<LibroCaja> libros=null;      
        LibroCajaDao libroDao = (LibroCajaDao)FactoryDao.buildDao("sci.LibroCajaDao");
       
       
        try {
            libros= libroDao.listarLibrosCajaPorUGEL(organizacionID,fecha);
            
        } catch (Exception e) {
            System.out.println("No se pudo Listar los Libros\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los Libros", e.getMessage() );
        }
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        
        for(LibroCaja lib:libros ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("codigo",lib.getOrganizacion().getCod());
            oResponse.put("organizacionID",lib.getOrganizacion().getOrgId());
            oResponse.put("libroID",lib.getLibCajId());
            oResponse.put("fechaApertura",lib.getFecApe());          
            oResponse.put("institucion",lib.getOrganizacion().getNom());
            oResponse.put("director",lib.getPersona().getNombrePersona());
            oResponse.put("personaID",lib.getPersona().getPerId());
            
            
            oResponse.put("fechaCierre",lib.getFecCie());    
            oResponse.put("saldoI",lib.getSalApe());
            oResponse.put("saldoA",lib.getSalAct());
            oResponse.put("estado",""+lib.getEstReg());
            
            
            miArray.put(oResponse);
        }
        
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente",miArray);        
        //Fin
    }
    
}
