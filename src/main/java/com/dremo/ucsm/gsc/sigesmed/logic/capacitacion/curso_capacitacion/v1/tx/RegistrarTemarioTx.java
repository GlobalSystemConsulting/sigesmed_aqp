package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.SedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.SedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONArray;
import org.json.JSONObject;

public class RegistrarTemarioTx implements ITransaction {

    private static final Logger logger = Logger.getLogger(RegistrarTemarioTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject data = (JSONObject) wr.getData();
            int usuMod = data.getInt("usuMod");
            JSONArray contenidos = data.getJSONArray("contenidos");

            SedeCapacitacionDao sedeCapacitacionDao = (SedeCapacitacionDao) FactoryDao.buildDao("capacitacion.SedeCapacitacionDao");
            TemarioCursoCapacitacionDao temarioCursoCapacitacionDao = (TemarioCursoCapacitacionDao) FactoryDao.buildDao("capacitacion.TemarioCursoCapacitacionDao");
            JSONArray temarioList = new JSONArray();

            for (int i = 0; i < contenidos.length(); i++) {
                SedeCapacitacion sede = sedeCapacitacionDao.buscarSedePorId(contenidos.getJSONObject(i).getInt("sed"));

                Date fecIni = HelpTraining.getStartOfDay(DatatypeConverter.parseDateTime(contenidos.getJSONObject(i).getString("ini")).getTime());
                Date fecFin = HelpTraining.getEndOfDay(DatatypeConverter.parseDateTime(contenidos.getJSONObject(i).getString("fin")).getTime());
                String tem = contenidos.getJSONObject(i).getString("tem");

                TemarioCursoCapacitacion temario = new TemarioCursoCapacitacion(sede, fecIni, fecFin, tem, usuMod);
                temarioCursoCapacitacionDao.insert(temario);

                JSONObject objTem = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"temCurCapId", "tem", "fecIni", "fecFin", "estReg"},
                        new String[]{"cod", "nom", "ini", "fin", "est"},
                        temario
                ));

                temarioList.put(objTem);
            }

            return WebResponse.crearWebResponseExito("El temario del curso de capacitación fue creado correctamente", WebResponse.OK_RESPONSE).setData(temarioList);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "registrarTemario", e);
            return WebResponse.crearWebResponseError("No se pudo registrar el temario del curso de capacitación", WebResponse.BAD_RESPONSE);
        }
    }

}
