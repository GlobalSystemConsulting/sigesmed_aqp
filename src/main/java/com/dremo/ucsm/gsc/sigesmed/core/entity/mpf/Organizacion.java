/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;
// Generated 13/02/2017 02:15:53 PM by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @autor:Carlos
 */
@Entity(name="com.dremo.ucsm.gsm.sigesmed.core.entity.mpf.Organizacion")
@Table(name = "organizacion", schema = "public")
public class Organizacion implements java.io.Serializable {

    @Id
    @Column(name = "org_id", unique = true, nullable = false)
    private Integer orgId;
    
    @ManyToOne()
    @JoinColumn(name = "org_pad_id")
    private Organizacion organizacion;

    @Column(name = "cod", nullable = false, length = 16)
    private String cod;

    @Column(name = "nom", nullable = false, length = 64)
    private String nom;

    @Column(name = "ali", length = 64)
    private String ali;

    @Column(name = "des", length = 256)
    private String des;
    
    @Column(name = "dir", length = 128)
    private String dir;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod")
    private Date fecMod;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "est_reg", length = 1)
    private Character estReg;
    
    @OneToMany(mappedBy = "organizacionByOrgOriId")
    private List<Matricula> matriculasForOrgOriId;
    
    @OneToMany(mappedBy = "organizacion")
    private List<Organizacion> organizacions;
    
    @OneToMany(mappedBy = "organizacionByOrgDesId")
    private List<Matricula> matriculasForOrgDesId;
    

    public Organizacion() {
    }
    public Organizacion(Integer  orgId) {
         this.orgId = orgId;
    }

    public Organizacion(Integer orgId, String cod, String nom) {
        this.orgId = orgId;
        this.cod = cod;
        this.nom = nom;
    }

    public Organizacion(Integer orgId, Organizacion organizacion, String cod, String nom, String ali, String des, String dir, Date fecMod, Integer usuMod, Character estReg, List<Matricula> matriculasForOrgOriId, List<Organizacion> organizacions, List<Matricula> matriculasForOrgDesId) {
        this.orgId = orgId;

        this.organizacion = organizacion;

        this.cod = cod;
        this.nom = nom;
        this.ali = ali;
        this.des = des;
        this.dir = dir;
        this.fecMod = fecMod;
        this.usuMod = usuMod;
        this.estReg = estReg;

        this.matriculasForOrgOriId = matriculasForOrgOriId;
        this.organizacions = organizacions;

        this.matriculasForOrgDesId = matriculasForOrgDesId;
    }

    public Integer getOrgId() {
        return this.orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Organizacion getOrganizacion() {
        return this.organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public String getCod() {
        return this.cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAli() {
        return this.ali;
    }

    public void setAli(String ali) {
        this.ali = ali;
    }

    public String getDes() {
        return this.des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getDir() {
        return this.dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public List<Matricula> getMatriculasForOrgOriId() {
        return this.matriculasForOrgOriId;
    }

    public void setMatriculasForOrgOriId(List<Matricula> matriculasForOrgOriId) {
        this.matriculasForOrgOriId = matriculasForOrgOriId;
    }

    public List<Organizacion> getOrganizacions() {
        return this.organizacions;
    }

    public void setOrganizacions(List<Organizacion> organizacions) {
        this.organizacions = organizacions;
    }

    public List<Matricula> getMatriculasForOrgDesId() {
        return this.matriculasForOrgDesId;
    }

    public void setMatriculasForOrgDesId(List<Matricula> matriculasForOrgDesId) {
        this.matriculasForOrgDesId = matriculasForOrgDesId;
    }

}
