/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.libro_caja.v1;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ReporteBalanceGeneralTx implements ITransaction{
    
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */        
      List<String[]> tablaIngresoC= new ArrayList<>();
      List<String[]> tablaEgresoC= new ArrayList<>();

      String[] cabeceraCon = new String[5];
      String[] resumenConI = new String[2];
      String[] resumenConE = new String[2];

       try{ 
           
           JSONObject request = (JSONObject)wr.getData();
           JSONObject cabecera= (JSONObject)request.get("cabecera");   
           
                double saldoInicialI = cabecera.getDouble("saldoInicial");            
                BigDecimal saldoInicial = new BigDecimal(saldoInicialI).setScale(2);
                double totalIngresoI = cabecera.getDouble("totalIngreso");            
                BigDecimal totalIngreso = new BigDecimal(totalIngresoI).setScale(2);
                double totalIng = cabecera.getDouble("totalI");            
                BigDecimal totalI = new BigDecimal(totalIng).setScale(2);
                double totalE = cabecera.getDouble("totalEgreso");            
                BigDecimal totalEgreso = new BigDecimal(totalE).setScale(2);
                double saldoAct = cabecera.getDouble("saldoA");            
                BigDecimal saldoA = new BigDecimal(saldoAct).setScale(2);
                
                String[] cabeceraC={"S/ "+String.valueOf(saldoInicial),"S/ "+String.valueOf(totalIngreso),"S/ "+String.valueOf(totalI),"S/ "+String.valueOf(totalEgreso),"S/ "+String.valueOf(saldoA)};
                cabeceraCon =cabeceraC;

           JSONArray tablaI = request.getJSONArray("tablaIngresos");  
          
           if(tablaI.length() > 0){
               for(int i = 0; i < tablaI.length();i++){                                   
                 
                   Object o =  tablaI.get(i);
                   JSONObject objOpe = (JSONObject)o;    
                   String cuenta = objOpe.getString("nombre");
                   
                
                   double total = objOpe.optDouble("total",0);
                   String totalCuenta = total==0? "": String.valueOf(new BigDecimal(total).setScale(2));
                   
                                                         
                    String[]row1 ={cuenta,"S/ "+totalCuenta}; 
                   
                   
                    tablaIngresoC.add(row1);
               }        
           }
           
           JSONObject totalesI= request.getJSONObject("totalesIngreso");   
           
          
                   double totalIn = totalesI.optDouble("totalIngresos",0);
                   String totalIngresos = totalIn==0? "": String.valueOf(new BigDecimal(totalIn).setScale(2));
                   
                                                         
                    String[]totalMensualI ={"INGRESOS S/ ","S/ "+totalIngresos}; 
                    resumenConI= totalMensualI;
                    
        JSONArray tablaE = request.getJSONArray("tablaEgresos");  
          
           if(tablaE.length() > 0){
               for(int i = 0; i < tablaE.length();i++){                                   
                 
                   Object o =  tablaE.get(i);
                   JSONObject objOpe = (JSONObject)o;    
                   String cuenta = objOpe.getString("nombre");
                   
                
                   double total = objOpe.optDouble("total",0);
                   String totalCuenta = total==0? "": String.valueOf(new BigDecimal(total).setScale(2));
                   
                                                         
                    String[]row1 ={cuenta,"S/ "+totalCuenta}; 
                   
                   
                    tablaEgresoC.add(row1);
               }        
           }
           
           JSONObject totalesE= request.getJSONObject("totalesEgreso");   
           
          
                   double totalEg = totalesE.optDouble("totalEgresos",0);
                   String totalEgresos = totalEg==0? "": String.valueOf(new BigDecimal(totalEg).setScale(2));
                   
                                                         
                    String[]totalMensualE ={"EGRESOS S/ ","S/ "+totalIngresos}; 
                    resumenConE= totalMensualE;
       }
        catch(Exception e){
            System.out.println("No se pudo Listar el directorio interno \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar el directorio interno ", e.getMessage() );
        }
      
      
   //   for(int i=0;i<1000;i++){
          
  //                String[]contenido ={"N","Fecha","Medio Pago","Descripción Operación","Razon Social","N° Doc.","Cod. Cta","Descripción Cuenta","Debe","Haber","Cod. Cta","Descripción Cuenta","Debe","Haber"};
   //       tablaC.add(contenido);
   //   }

     
        //Creando el reporte....        
        Mitext m = null;        
        try {
            m = new Mitext(true,"REPORTE DE BALANCE");
            m.agregarTitulo("REPORTE BALANCE GENERAL DEL LIBRO CAJA");

        } catch (Exception ex) {
            System.out.println("No se pudo crear el documento \n"+ex);
            Logger.getLogger(ReporteLibroCajaTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        // m.agregarParrafo("LIBRO CAJA SIGESMED");
        //agregar tabla
       float[] columnWidths={3,3,3,3,3};
        GTabla t = new GTabla(columnWidths);
        t.addHeaderCell(new Cell(1, 10).setBorder(Border.NO_BORDER).add(new Paragraph("MOVIMIENTO ECONOMICO ")).setFontSize(12).setBorder(Border.NO_BORDER));
      //  t.addFooterCell(new Cell(1,10));
        String[]encabezado ={"a) SALDO INICIAL ","b) INGRESOS","c) TOTAL INGRESOS(a+b)","d) TOTAL EGRESOS/GASTOS","e) SALDO ACTUAL(c-d)"};
        GCell[] cell ={t.createCellCenter(1,1),t.createCellCenter(1,1),t.createCellCenter(1,1),t.createCellCenter(1,1),t.createCellCenter(1,1)};
        try {
            t.build(encabezado);
        } catch (IOException ex) {
            Logger.getLogger(ReporteBalanceGeneralTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       //   String[]contenido ={"N°","Fecha","Glosa"};
      
              t.processLineCell(cabeceraCon,cell);
        
        
           
        
        
        //fin tabla
        
        m.agregarTabla(t);
        float[] columnWidthsD={5,1,5};
        GTabla tablaD = new GTabla(columnWidthsD);
      
        tablaD.addHeaderCell(new Cell(1, 2).add(new Paragraph("CLASIFICACION ")).setFontSize(12).setBorder(Border.NO_BORDER));
      //  tCuentas.addFooterCell(new Cell(1,14));
        
        
        float[] columnWidthsC={5,2};
        GTabla tCuentasI = new GTabla(columnWidthsC);
        
      //  tCuentas.addHeaderCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph("CLASIFICACION ")).setFontSize(12).setBorder(Border.NO_BORDER));
      //  tCuentas.addFooterCell(new Cell(1,14));
        String[]encabezadoCI ={"INGRESOS","TOTAL S/ "};
        GCell[] cellCI ={tCuentasI.createCellCenter(1,1),tCuentasI.createCellCenter(1,1)};                       
        try {
            tCuentasI.build(encabezadoCI);
        } catch (IOException ex) {
            Logger.getLogger(ReporteBalanceGeneralTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       //   String[]contenido ={"N°","Fecha","Glosa"};
      for(String [] c:tablaIngresoC){
              tCuentasI.processLineCell(c,cellCI);
      }
        
           
         tCuentasI.processLineCell(resumenConI,cellCI);
        
        //fin tabla
         
         
        GTabla tCuentasE = new GTabla(columnWidthsC);
        
      //  tCuentas.addHeaderCell(new Cell(1, 2).setBorder(Border.NO_BORDER).add(new Paragraph("CLASIFICACION ")).setFontSize(12).setBorder(Border.NO_BORDER));
      //  tCuentas.addFooterCell(new Cell(1,14));
        String[]encabezadoCE ={"EGRESOS","TOTAL S/ "};
        GCell[] cellCE ={tCuentasE.createCellCenter(1,1),tCuentasE.createCellCenter(1,1)};                       
        try {
            tCuentasE.build(encabezadoCE);
        } catch (IOException ex) {
            Logger.getLogger(ReporteBalanceGeneralTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       //   String[]contenido ={"N°","Fecha","Glosa"};
      for(String [] c:tablaEgresoC){
              tCuentasE.processLineCell(c,cellCE);
      }
        
           
         tCuentasE.processLineCell(resumenConE,cellCE);
       
        tablaD.addCell(new Cell(1,1).add(tCuentasI).setBorder(Border.NO_BORDER));
        tablaD.addCell(new Cell().setBorder(Border.NO_BORDER));
        tablaD.addCell(new Cell(1,1).add(tCuentasE).setBorder(Border.NO_BORDER));
        m.agregarTabla(tablaD);
        
//        agregar grafico
        
//        MChart chart = new MChart();
//        
//        DefaultPieDataset dataset = new DefaultPieDataset( );
//        dataset.setValue( "IPhone 5s" , new Double( 20 ) );  
//        dataset.setValue( "SamSung Grand" , new Double( 20 ) );   
//        dataset.setValue( "MotoG" , new Double( 40 ) );    
//        dataset.setValue( "Nokia Lumia" , new Double( 10 ) );  
//        
//        try {
//            m.agregarGrafico(chart.createPieChart(dataset, "Grafica de Ejemplo"), 400, 300);
//        } catch (IOException ex) {
//            System.out.println("No se pudo agregar el grafico \n"+ex);
//            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        m.cerrarDocumento();  
                                
        JSONArray miArray = new JSONArray();
        JSONObject oResponse = new JSONObject();        
        oResponse.put("datareporte",m.encodeToBase64());
        miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);        
        
    }
    
    
}