/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;

/**
 *
 * @author gscadmin
 */
public class MA_Constantes {
    public static final Integer NOTA_NUMERAL_MIN_AD = 18;
    public static final Integer NOTA_NUMERAL_MAX_AD = 20;
    public static final Integer NOTA_NUMERAL_MIN_A = 14;
    public static final Integer NOTA_NUMERAL_MAX_A = 17;
    public static final Integer NOTA_NUMERAL_MIN_B = 8;
    public static final Integer NOTA_NUMERAL_MAX_B = 13;
    public static final Integer NOTA_NUMERAL_MIN_C = 0;
    public static final Integer NOTA_NUMERAL_MAX_C = 7;
    
    public static Integer mostrarNotaNumeral(String nota){
        switch (nota){
            case "A": return NOTA_NUMERAL_MAX_A;
            case "B": return NOTA_NUMERAL_MAX_B;
            case "C": return NOTA_NUMERAL_MAX_C;
            case "AD": return NOTA_NUMERAL_MAX_AD;
            default: return 0;
        }
    }
    
    public static String mostrarNotaLiteral(Double nota){
        if(nota>=NOTA_NUMERAL_MIN_AD && nota<= NOTA_NUMERAL_MAX_AD)
            return "AD";
        else if(nota>=NOTA_NUMERAL_MIN_A && nota<= NOTA_NUMERAL_MAX_A)
            return "A";
        else if(nota>=NOTA_NUMERAL_MIN_B && nota<= NOTA_NUMERAL_MAX_B)
            return "B";
        else
            return "C";
    }
    
}

