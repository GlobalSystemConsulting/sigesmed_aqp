/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.service.base;

import com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.PersistentEnum;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.CargosTrabajadorDefault;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class EntityUtil {
    private final static Logger  logger = Logger.getLogger(EntityUtil.class.getName());
    public static String objectToJSONString(String[] properties, String[] alias,Object entity){
        if(entity == null) return "{}";
        Class mClass = entity.getClass();
        StringBuilder buffer = new StringBuilder();
        if(properties != null && properties.length > 0){
            if((alias == null ) ||(properties.length == alias.length)){
                try{
                    buffer.append("{");
                    for(int i = 0; i < properties.length; i++){
                        buffer.append("\"").append(alias != null ? alias[i] : properties[i]).append("\":");
                        Object value = new PropertyDescriptor(properties[i], mClass).getReadMethod().invoke(entity);
                        if(value instanceof String || value instanceof Character){
                            buffer.append("\"").append(value).append("\"");;
                        }else if(value instanceof Date){
                            //buffer.append("\"").append(new SimpleDateFormat("dd-MM-yyyy").format(value)).append("\"");
                            buffer.append(((Date) value).getTime());
                        }else if(value instanceof PersistentEnum){
                            buffer.append("\"").append(((PersistentEnum) value).getValue()).append("\"");;
                        }
                        else buffer.append(value);
                        if(i != (properties.length - 1)){
                            buffer.append(",");
                        }
                    }
                    buffer.append("}");
                }catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
                return buffer.toString();
            }else{
                logger.log(Level.SEVERE, logger.getName() + "objectToJSONString: {0}", "No coincide el size del alias y properties");
                return null;
            }
        }else{
            logger.log(Level.SEVERE, logger.getName() + "objectToJSONString: {0}", "Properties no esta definido");
            return null;
        }
    }
    public static String listToJSONString(String[] properties, String[] alias,List<? extends java.io.Serializable> entity){
        if(entity == null) return "[]";
        StringBuilder buffer = new StringBuilder();
        buffer.append("[");
        for(int i = 0; i < entity.size(); i++){
            String strObject = objectToJSONString(properties,alias,entity.get(i));
            buffer.append(strObject);
            if( i != (entity.size() - 1)){
                buffer.append(",");
            }
        }
        buffer.append("]");
        return buffer.toString();
    }
    public static String getTipoCargo(String rol){
        CargosTrabajadorDefault[] cargos = CargosTrabajadorDefault.values();
        String rpta = null;
        for(CargosTrabajadorDefault t : cargos){
            if(t.getNombre().toLowerCase().equals(rol.toLowerCase())){
                rpta = t.name();
                break;
            }
        }
        return rpta;
    }
}
