package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.TemarioCursoCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.TemarioCursoCapacitacion;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class TemarioCursoCapacitacionDaoHibernate extends GenericDaoHibernate<TemarioCursoCapacitacion> implements TemarioCursoCapacitacionDao {

    private static final Logger logger = Logger.getLogger(TemarioCursoCapacitacionDaoHibernate.class.getName());

    @Override
    public List<TemarioCursoCapacitacion> buscarPorSede(int codSed) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(TemarioCursoCapacitacion.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .addOrder(Order.asc("fecIni"))
                    .setFetchMode("sedCap", FetchMode.JOIN)
                    .createCriteria("sedCap")
                    .add(Restrictions.eq("sedCapId", codSed))
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            return query.list();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public TemarioCursoCapacitacion buscarPorId(int idTem) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Criteria query = session.createCriteria(TemarioCursoCapacitacion.class)
                    .add(Restrictions.ne("estReg", 'E'))
                    .add(Restrictions.eq("temCurCapId", idTem));

            return (TemarioCursoCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorId", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public Map<Integer, String> obtenerTemas(int sedCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String hql = "SELECT t.temCurCapId, t.tem FROM TemarioCursoCapacitacion t "
                    + "JOIN t.sedCap s "
                    + "WHERE s.sedCapId = :sedCapId AND t.estReg != :estReg "
                    + "ORDER BY t.fecIni";

            Query query = session.createQuery(hql);
            query.setParameter("sedCapId", sedCod);
            query.setParameter("estReg", 'E');

            List<Object[]> rows = query.list();
            Map<Integer, String> result = new HashMap<>();

            for (Object[] row : rows) {
                result.put(Integer.parseInt(row[0].toString()), row[1].toString());
            }

            return result;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "obtenerTemas", e);
            throw e;
        } finally {
            session.close();
        }
    }

    @Override
    public boolean verificarComentarios(int temCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            Criteria query = session.createCriteria(TemarioCursoCapacitacion.class)
                    .add(Restrictions.eq("temCurCapId", temCod))
                    .createAlias("comentarios", "com")
                    .add(Restrictions.ne("com.estReg", 'E'))                    
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)                    
                    .setProjection(Projections.rowCount());

            return (((Long) query.uniqueResult()).intValue() > 0);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "verificarComentarios", e);
            throw e;
        } finally {
            session.close();
        }
    }
    
    @Override
    public TemarioCursoCapacitacion buscarEliminar(int temCod) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String hql = "SELECT t FROM TemarioCursoCapacitacion t "
                    + "JOIN FETCH t.desarrollos d "
                    + "JOIN FETCH d.evaluaciones "
                    + "WHERE t.temCurCapId = :temCurCapId";
            
            Query query = session.createQuery(hql);
            query.setParameter("temCurCapId", temCod);

            return (TemarioCursoCapacitacion) query.uniqueResult();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "buscarPorSede", e);
            throw e;
        } finally {
            session.close();
        }
    }
}
