/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "desplazamiento", schema="administrativo")
public class Desplazamiento implements Serializable {
    @Id
    @Column(name = "des_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_desplazamiento", sequenceName="administrativo.desplazamiento_des_id_seq" )
    @GeneratedValue(generator="secuencia_desplazamiento")
    private Integer desId;
    
    @Column(name = "tip")
    private Character tip;

    @Column(name = "num_res")
    private String numRes;
    
    @Column(name = "fec_res")
    @Temporal(TemporalType.DATE)
    private Date fecRes;
    
    @Column(name = "ins_edu")
    private String insEdu;
    
    @Column(name = "car")
    private String car;
    
    @Column(name = "jor_lab")
    private String jorLab;
    
    @Column(name = "fec_ini")
    @Temporal(TemporalType.DATE)
    private Date fecIni;
    
    @Column(name = "fec_ter")
    @Temporal(TemporalType.DATE)
    private Date fecTer;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Desplazamiento() {
    }

    public Desplazamiento(Integer desId) {
        this.desId = desId;
    }
    
    public Desplazamiento(FichaEscalafonaria fichaEscalafonaria, Character tip, String numRes, Date fecRes, String insEdu, String car, String jorLab, Date fecIni, Date fecTer, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.tip = tip;
        this.numRes = numRes;
        this.fecRes = fecRes;
        this.insEdu = insEdu;
        this.car = car;
        this.jorLab = jorLab;
        this.fecIni = fecIni;
        this.fecTer = fecTer;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public Integer getDesId() {
        return desId;
    }

    public void setDesId(Integer desId) {
        this.desId = desId;
    }

    public Character getTip() {
        return tip;
    }

    public void setTip(Character tip) {
        this.tip = tip;
    }

    public String getNumRes() {
        return numRes;
    }

    public void setNumRes(String numRes) {
        this.numRes = numRes;
    }

    public Date getFecRes() {
        return fecRes;
    }

    public void setFecRes(Date fecRes) {
        this.fecRes = fecRes;
    }

    public String getInsEdu() {
        return insEdu;
    }

    public void setInsEdu(String insEdu) {
        this.insEdu = insEdu;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getJorLab() {
        return jorLab;
    }

    public void setJorLab(String jorLab) {
        this.jorLab = jorLab;
    }

    public Date getFecIni() {
        return fecIni;
    }

    public void setFecIni(Date fecIni) {
        this.fecIni = fecIni;
    }

    public Date getFecTer() {
        return fecTer;
    }

    public void setFecTer(Date fecTer) {
        this.fecTer = fecTer;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }
    
    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setDatEscId(FichaEscalafonaria datosEscalafon) {
        this.fichaEscalafonaria = datosEscalafon;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (desId != null ? desId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desplazamiento)) {
            return false;
        }
        Desplazamiento other = (Desplazamiento) object;
        if ((this.desId == null && other.desId != null) || (this.desId != null && !this.desId.equals(other.desId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Desplazamiento{" + "desId=" + desId + ", tip=" + tip + ", numRes=" + numRes + ", fecRes=" + fecRes + ", insEdu=" + insEdu + ", car=" + car + ", jorLab=" + jorLab + ", fecIni=" + fecIni + ", fecTer=" + fecTer + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }

    
    
}
