package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;
// Generated 15/07/2016 01:43:01 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author geank
 */
@Embeddable
public class IndicadoresEvaluarPersonalId  implements java.io.Serializable {
    
    @Column(name="det_eva_per_id", nullable=false)
    private int detEvaPerId;
    @Column(name="ind_fic_eva_id", nullable=false)
    private int indFicEvaId;

    public IndicadoresEvaluarPersonalId() {
    }

    public IndicadoresEvaluarPersonalId(int detEvaPerId, int indFicEvaId) {
       this.detEvaPerId = detEvaPerId;
       this.indFicEvaId = indFicEvaId;
    }
   
    public int getDetEvaPerId() {
        return this.detEvaPerId;
    }
    
    public void setDetEvaPerId(int detEvaPerId) {
        this.detEvaPerId = detEvaPerId;
    }

    public int getIndFicEvaId() {
        return this.indFicEvaId;
    }
    
    public void setIndFicEvaId(int indFicEvaId) {
        this.indFicEvaId = indFicEvaId;
    }

    @Override
    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( (other == null ) ) return false;
        if ( !(other instanceof IndicadoresEvaluarPersonalId) ) return false;
        IndicadoresEvaluarPersonalId castOther = ( IndicadoresEvaluarPersonalId ) other; 
        return (this.getDetEvaPerId()==castOther.getDetEvaPerId()) 
                && (this.getIndFicEvaId()==castOther.getIndFicEvaId());
    }    
   
    @Override
   public int hashCode() {
        int result = 17;
        result = 37 * result + this.getDetEvaPerId();
        result = 37 * result + this.getIndFicEvaId();
        return result;
   }
}


