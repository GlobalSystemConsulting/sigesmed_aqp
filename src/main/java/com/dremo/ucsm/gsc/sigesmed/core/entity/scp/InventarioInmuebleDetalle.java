/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;

/**
 *
 * @author Jeferson
 */

@Entity
@Table(name="inv_inm_det", schema="administrativo")
public class InventarioInmuebleDetalle {
    
    @Id
    @Column(name="inv_inm_id")
    private int inv_inm_id;
    
   
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
        @JoinColumn(name="inv_inm_id",referencedColumnName="inv_inm_id",insertable=false,updatable=false),
        @JoinColumn(name="mov_ing_id",referencedColumnName="mov_ing_id",insertable=false,updatable=false)
    })
    private InventarioInmueble inv_inmueble;
    
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="bie_inm_id" , insertable=false , updatable=false)
    private BienesInmuebles bien;

    public void setInv_inmueble(InventarioInmueble inv_inmueble) {
        this.inv_inmueble = inv_inmueble;
    }

    public void setBien(BienesInmuebles bien) {
        this.bien = bien;
    }

    public InventarioInmueble getInv_inmueble() {
        return inv_inmueble;
    }

    public BienesInmuebles getBien() {
        return bien;
    }
    
    
}
