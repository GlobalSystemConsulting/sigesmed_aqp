/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.smdg;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import java.util.List;
import javax.persistence.FetchType;

/**
 *
 * @author Administrador
 */
@Entity(name="com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.ItemFile")
@Table(name = "item_file", schema="institucional")

public class ItemFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ite_ide")
    private Integer iteIde;
    @Basic(optional = false)
    @Column(name = "ite_alt_ide")
    private String iteAltIde;
    @Column(name = "ite_nom")
    private String iteNom;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ite_tam")
    private Double iteTam;
    @Column(name = "pge_ava")
    private Double pgeAva;
    
    @Column(name = "ite_ver")
    private Short iteVer;
    @Column(name = "ite_fec_cre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iteFecCre;
    @Column(name = "ite_url_des")
    private String iteUrlDes;
//    @Column(name = "ite_org_ide")
//    private Integer iteOrgIde;
    @JoinColumn(name = "ite_org_ide", referencedColumnName = "org_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Organizacion organizacion; 
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
//    @Column(name = "ite_usu_ide")
//    private Integer iteUsuIde;
    @JoinColumn(name = "ite_usu_ide", referencedColumnName = "tra_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Trabajador trabajador;
    @Column(name = "est_reg")
    private String estReg;
    @Column(name = "ite_cod_cat")
    private String iteCodCat;
    
//    @OneToMany(mappedBy = "item")
//    private List<Objetivos> objetivos;
    
    @OneToMany(mappedBy = "ifdIteIde")
    private List<ItemFileDetalle> itemFileDetalle;
//    @OneToMany(mappedBy = "iteIde")
//    private List<FichaEvaluacionDocumentos> fichaEvaluacionDocumentosList;
//    @JoinColumn(name = "ite_tif_ide", referencedColumnName = "tif_ide")
//    @ManyToOne(fetch=FetchType.LAZY)
//    private TipoItemFile iteTifIde;
//    @JoinColumn(name = "ite_pla_ide", referencedColumnName = "plz_ide")
//    @ManyToOne
//    private Plazo itePlaIde;
//    @OneToMany(mappedBy = "itePadAdj")
//    private List<ItemFile> itemFile;
//    @JoinColumn(name = "ite_pad_adj", referencedColumnName = "ite_ide")
//    @ManyToOne(fetch=FetchType.LAZY)
//    private ItemFile itePadAdj;
    @OneToMany(mappedBy = "itePadIde")
    private List<ItemFile> itemFilePadre;
    @JoinColumn(name = "ite_pad_ide", referencedColumnName = "ite_ide")
    @ManyToOne
    private ItemFile itePadIde;

    public ItemFile() {
    }

    public ItemFile(Integer iteIde) {
        this.iteIde = iteIde;
    }

    public ItemFile(Integer iteIde, String iteAltIde) {
        this.iteIde = iteIde;
        this.iteAltIde = iteAltIde;
    }

    public Integer getIteIde() {
        return iteIde;
    }

    public void setIteIde(Integer iteIde) {
        this.iteIde = iteIde;
    }

    public String getIteAltIde() {
        return iteAltIde;
    }

    public void setIteAltIde(String iteAltIde) {
        this.iteAltIde = iteAltIde;
    }

    public String getIteNom() {
        return iteNom;
    }

    public void setIteNom(String iteNom) {
        this.iteNom = iteNom;
    }

    public Double getIteTam() {
        return iteTam;
    }

    public void setIteTam(Double iteTam) {
        this.iteTam = iteTam;
    }

    public Short getIteVer() {
        return iteVer;
    }

    public void setIteVer(Short iteVer) {
        this.iteVer = iteVer;
    }

    public Date getIteFecCre() {
        return iteFecCre;
    }

    public void setIteFecCre(Date iteFecCre) {
        this.iteFecCre = iteFecCre;
    }

    public String getIteUrlDes() {
        return iteUrlDes;
    }

    public void setIteUrlDes(String iteUrlDes) {
        this.iteUrlDes = iteUrlDes;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }
    
    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public String getIteCodCat() {
        return iteCodCat;
    }

    public void setIteCodCat(String iteCodCat) {
        this.iteCodCat = iteCodCat;
    }

//    public List<Objetivos> getObjetivos() {
//        return objetivos;
//    }
//
//    public void setObjetivos(List<Objetivos> objetivos) {
//        this.objetivos = objetivos;
//    }

    public List<ItemFileDetalle> getItemFileDetalle() {
        return itemFileDetalle;
    }

    public void setItemFileDetalle(List<ItemFileDetalle> itemFileDetalle) {
        this.itemFileDetalle = itemFileDetalle;
    }

    public List<ItemFile> getItemFilePadre() {
        return itemFilePadre;
    }

    public void setItemFilePadre(List<ItemFile> itemFilePadre) {
        this.itemFilePadre = itemFilePadre;
    }

    public ItemFile getItePadIde() {
        return itePadIde;
    }

    public void setItePadIde(ItemFile itePadIde) {
        this.itePadIde = itePadIde;
    }

//    public TipoItemFile getIteTifIde() {
//        return iteTifIde;
//    }
//
//    public void setIteTifIde(TipoItemFile iteTifIde) {
//        this.iteTifIde = iteTifIde;
//    }

    public Double getPgeAva() {
        return pgeAva;
    }

    public void setPgeAva(Double pgeAva) {
        this.pgeAva = pgeAva;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iteIde != null ? iteIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemFile)) {
            return false;
        }
        ItemFile other = (ItemFile) object;
        if ((this.iteIde == null && other.iteIde != null) || (this.iteIde != null && !this.iteIde.equals(other.iteIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.smdg.ItemFile[ iteIde=" + iteIde + " ]";
    }
    
}
