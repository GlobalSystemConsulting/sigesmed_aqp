/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;

import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.InventarioEliminacion;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.SerieDocumental;

import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public interface InventarioEliminacionDAO extends GenericDao<InventarioEliminacion>{
    
    public List<InventarioEliminacion> buscarPorOrganizacion();
    public List<InventarioEliminacion> buscarPorInventarioTransferencia();
    public void registrarDocumentos(List documentos);
    public void validarInventario();
    /*Agregados*/
    public void validarInventario(SerieDocumental sd);
    
}
