/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.AreaDao;
      
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Area;
import com.dremo.ucsm.gsc.sigesmed.core.entity.Organizacion;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Jeferson
 */
public class InsertarAreaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        Area area = null;
        
        Organizacion organizacion = null;
        organizacion = new Organizacion();
        
      //  List<FileJsonObject> lista_uni_org = new ArrayList<FileJsonObject>();
        try{
          
             JSONObject requestData = (JSONObject)wr.getData();
             // Datos del Area
             String cod = requestData.getString("cod");
             String nom = requestData.getString("nom");
             
             Date fechaInicio = new Date();
             
             JSONArray lista_uni_org = requestData.getJSONArray("unidades_organicas");
             
             // Leendo la Lista de Unidades Organicas
             if(lista_uni_org.length()>0){
                 organizacion.setOrganizaciones(new ArrayList<Organizacion>());
                 for(int i = 0 ;i< lista_uni_org.length();i++){
                     JSONObject bo = lista_uni_org.getJSONObject(i);
                     // Datos de las Unidades Organicas
                     String abreviacion = bo.getString("abreviacion");
                     String nombre_unidad = bo.getString("nombre_unidad");
                     String descripcion = "";
                     
                     
                 }
             }
             
             
        //   area = new Area(0,)
        //    public Area(int areId, Organizacion organizacion,TipoArea tipoArea,String cod, String nom, Date fecMod, Integer usuMod, char estReg) {
    
        }
        catch(Exception e){
             System.out.println(e);
             return WebResponse.crearWebResponseError("No se pudo registrar el Area, datos incorrectos", e.getMessage() );
        }
        
        //temporal al final se debe sacar
         JSONObject oResponse = new JSONObject();
           return WebResponse.crearWebResponseExito("El registro del expediente se realizo correctamente", oResponse);
        //Fin
        
    }
    
    
}
