package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.ParienteEstudianteDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesId;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.ParientesMMI;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class EliminarParienteEstudianteTx implements ITransaction {

    private static Logger logger = Logger.getLogger(EliminarParienteEstudianteTx.class.getName());

    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        long perId, parId;

        try {
            perId = data.getInt("perId");
            parId = data.getInt("parId");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al cargar datos al eliminar Pariente", e);
            return WebResponse.crearWebResponseError("Nose pudo eliminar Pariente");
        }

        ParienteEstudianteDaoHibernate dhPariente = new ParienteEstudianteDaoHibernate();

        try {
            ParientesMMI myPariente = new ParientesMMI();
            myPariente.setId(new ParientesId(parId, perId));
            dhPariente.delete(myPariente);

            return WebResponse.crearWebResponseExito("Se elimino el Pariente exitosamente", myPariente);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error al Eliminar Pariente", e);
            return WebResponse.crearWebResponseError("Nose pudo eliminar Pariente");
        }
    }
}
