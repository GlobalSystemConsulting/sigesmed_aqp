/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.tipo_tramite.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoTramiteDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RequisitoTramite;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.RutaTramite;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoTramite;
import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildCodigo;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @class InsertarTipoTramiteTx
 * Encargada de recibir realizar los datos de un tipo de tramite y 
 * hacer la persistencia
 * 
 * @author Administrador
 * 
 */
public class InsertarTipoTramiteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        TipoTramiteDao tipoTramiteDao = (TipoTramiteDao)FactoryDao.buildDao("std.TipoTramiteDao");
        
        TipoTramite nuevoTipoTramite = null;
        List<FileJsonObject> listaArchivos = new ArrayList<FileJsonObject>();
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            int duracion = requestData.getInt("duracion");
            double costo = requestData.getDouble("costo");
            boolean tipo = requestData.getBoolean("tipo");
            boolean tupa = requestData.getBoolean("tupa");
            int tipoOrganizacion = requestData.getInt("tipoOrganizacionID");
            JSONArray listaRequisitos = requestData.getJSONArray("requisitos");
            JSONArray listaRutas = requestData.getJSONArray("rutas");
            
            nuevoTipoTramite = new TipoTramite(0, BuildCodigo.cerosIzquierda(Integer.parseInt(tipoTramiteDao.buscarUltimoCodigo())+1,4), nombre, descripcion, duracion, new BigDecimal(costo), tipo,tupa, new Date(), wr.getIdUsuario(), 'A', tipoOrganizacion);
            
            //leendo los requisitos            
            if(listaRequisitos.length() > 0){
                nuevoTipoTramite.setRequisitoTramites( new ArrayList<RequisitoTramite>());
                for(int i = 0; i < listaRequisitos.length();i++){
                    JSONObject bo =listaRequisitos.getJSONObject(i);

                    String nombreArchivo = "";
                    String rutaDescripcion = bo.getString("descripcion");
                    
                    //verificamos si existe un archivo adjunto al requisito
                    JSONObject jsonArchivo = bo.optJSONObject("archivo");
                    if( jsonArchivo !=null && jsonArchivo.length() > 0 ){
                        FileJsonObject miF = new FileJsonObject( jsonArchivo ,nuevoTipoTramite.getCod()+"_doc_req_"+BuildCodigo.cerosIzquierda(i+1,2));
                        nombreArchivo = miF.getName();
                        listaArchivos.add(miF);
                    }
                    nuevoTipoTramite.getRequisitoTramites().add( new RequisitoTramite(i+1, nuevoTipoTramite,rutaDescripcion,nombreArchivo,new Date(),wr.getIdUsuario(),'A') );
                }
            }
            //leendo las rutas
            if(listaRutas.length() > 0){
                nuevoTipoTramite.setRutaTramites( new ArrayList<RutaTramite>() );
                for(int i = 0; i < listaRutas.length();i++){
                    JSONObject bo = listaRutas.getJSONObject(i);

                    int areaOriID = bo.getInt("areaOriID");
                    int areaDesID = bo.getInt("areaDesID");
                    String areaDescripcion = bo.getString("descripcion");

                    nuevoTipoTramite.getRutaTramites().add( new RutaTramite(i+1,nuevoTipoTramite,areaDescripcion,areaOriID,areaDesID, new Date(), wr.getIdUsuario(),'A'  ) );
                }
            }
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el tipo de tramite, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        /*
        *   Parte de Logica de Negocio
        *   descripcion: El Sitema debe generar el codigo para el nuevo Tipo de Tramite antes de insertar a la BD
        */
        
        
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        try{
            tipoTramiteDao.insert(nuevoTipoTramite);            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el tipo de tramite ", e.getMessage() );
        }
        //Fin
        
        //si ya se registro el tipo de tramite 
        //ahora creamos los archivos que se desean subir
        for(FileJsonObject archivo : listaArchivos){
            BuildFile.buildFromBase64("tramite", archivo.getName(), archivo.getData());
        }
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("tipoTramiteID",nuevoTipoTramite.getTipTraId());
        oResponse.put("codigo",nuevoTipoTramite.getCod());
        return WebResponse.crearWebResponseExito("El registro de Tipo Tramite se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
