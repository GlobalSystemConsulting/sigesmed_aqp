/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.documento_estado_prioridad.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.std.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.std.TipoDocumento;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

/**
 *
 * @author abel
 */
public class EliminarTipoDocumentoTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int tipoDocumentoID = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            tipoDocumentoID = requestData.getInt("tipoDocumentoID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el tipo de documento, datos incorrectos");
        }
        //Fin
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        TipoDocumentoDao documentoDao = (TipoDocumentoDao)FactoryDao.buildDao("std.TipoDocumentoDao");
        try{
            documentoDao.delete(new TipoDocumento(tipoDocumentoID));
        
        }catch(Exception e){
            System.out.println("No se pudo eliminar el Tipo de Documento\n"+e);
            return WebResponse.crearWebResponseError("No se pudo eliminar el Tipo de Documento", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */        
        return WebResponse.crearWebResponseExito("El Tipo de Documento se elimino correctamente");
        //Fin
    }
}
