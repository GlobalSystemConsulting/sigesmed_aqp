/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;

/**
 *
 * @author Jeferson
 */
@Entity
@Table(name="inventario_inmueble", schema="administrativo")
public class InventarioInmueble implements java.io.Serializable {
    
    @Id
    @Column(name="inv_inm_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.inventario_inmueble_inv_inm_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int inv_inm_id;
    
    @Id
    private int mov_ing_id;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="mov_ing_id", insertable = false , updatable = false)
    private MovimientoIngresos mov_ingresos;
    
    
    
    
    @Column(name="est_reg")
    private char est_reg;

    public void setInv_inm_id(int inv_inm_id) {
        this.inv_inm_id = inv_inm_id;
    }

    public void setMov_ing_id(int mov_ing_id) {
        this.mov_ing_id = mov_ing_id;
    }

    public void setMov_ingresos(MovimientoIngresos mov_ingresos) {
        this.mov_ingresos = mov_ingresos;
    }

    

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getInv_inm_id() {
        return inv_inm_id;
    }

    public int getMov_ing_id() {
        return mov_ing_id;
    }

    public MovimientoIngresos getMov_ingresos() {
        return mov_ingresos;
    }

   

    public char getEst_reg() {
        return est_reg;
    }
    
    
    
    
}
