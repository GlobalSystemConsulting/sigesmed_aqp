/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_area_archivo_central.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sad.UnidadOrganicaDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.UnidadOrganica;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.util.BuildFile;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Jeferson
 */
public class ListarUnidadOrganicaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        int are_id = 0;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            are_id = requestData.getInt("codigo_area");
         //   are_id = 9;
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades Organicas ", e.getMessage() );
        }
        
        
        List<UnidadOrganica> unidades_organicas = null;
        try{
            
             UnidadOrganicaDAO uni_org_dao = (UnidadOrganicaDAO)FactoryDao.buildDao("sad.UnidadOrganicaDAO");
             unidades_organicas = uni_org_dao.buscarPorArea(are_id);
            
        }catch(Exception e){
            System.out.println("No se pudo Listar las Unidades Organicas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades Organicas", e.getMessage() );
        }
         JSONArray miArray = new JSONArray();
        try{
          
           for(UnidadOrganica uni_org : unidades_organicas){
               JSONObject oResponse = new JSONObject();
               oResponse.put("uni_org_id", uni_org.getUniOrgId());
              oResponse.put("nom_uni",  uni_org.getnombre());
               miArray.put(oResponse);
           }   
        } 
        catch(Exception e){
            System.out.println("No se pudo Listar las Unidades Organicas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar las Unidades Organicas", e.getMessage() );
        }
        
        return WebResponse.crearWebResponseExito("Se Listo las Unidades Organicas Correctamente",miArray); 
        
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
