/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.maestro.plan_curricular;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 *
 * @author abel
 */
@Entity
@Table(name = "tarea_indicador", schema = "pedagogico")  
public class TareaIndicador {
    
   @SequenceGenerator(name = "pedagogico.tarea_indicador_tar_ind_id_seq", sequenceName = "pedagogico.tarea_indicador_tar_ind_id_seq")
   @GeneratedValue(generator = "pedagogico.tarea_indicador_tar_ind_id_seq")
   @Id
   @Column(name = "tar_ind_id", nullable = false)
   private int tar_ind_id; 
   
   @Column(name = "ind_apr_id")
   private int ind_apr_id;
   
   @Column(name = "tar_id_ses")
   private int tar_id_ses;

   @ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="tar_id_ses" , insertable=false , updatable=false)
   private TareaSesionAprendizaje tar_ses_apr;
   
   
    public TareaIndicador() {
    }
    public TareaIndicador(int tar_ind_id ,TareaSesionAprendizaje tar_ses , int ind_apr_id){
        
        this.tar_ind_id = tar_ind_id;
        this.tar_ses_apr = tar_ses;
        this.ind_apr_id = ind_apr_id;
    }
    
    public TareaIndicador(int tar_ind_id, int ind_apr_id, int tar_id_ses) {
        this.tar_ind_id = tar_ind_id;
        this.ind_apr_id = ind_apr_id;
        this.tar_id_ses = tar_id_ses;
    }

    public void setTar_ses_apr(TareaSesionAprendizaje tar_ses_apr) {
        this.tar_ses_apr = tar_ses_apr;
    }

    public TareaSesionAprendizaje getTar_ses_apr() {
        return tar_ses_apr;
    }

    
    
    public void setSesAprId(int tar_ind_id) {
        this.tar_ind_id = tar_ind_id;
    }

    public void setInd_apr_id(int ind_apr_id) {
        this.ind_apr_id = ind_apr_id;
    }

    public void setTar_id_ses(int tar_id_ses) {
        this.tar_id_ses = tar_id_ses;
    }

    public int getSesAprId() {
        return tar_ind_id;
    }

    public int getInd_apr_id() {
        return ind_apr_id;
    }

    public int getTar_id_ses() {
        return tar_id_ses;
    }
   
    
}
