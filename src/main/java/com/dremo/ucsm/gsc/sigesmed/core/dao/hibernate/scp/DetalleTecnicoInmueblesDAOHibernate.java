/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.scp;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.DetalleTecnicoInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.DetalleTecnicoInmueblesDAO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienesMueblesDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import java.util.Date;


/**
 *
 * @author Administrador
 */
public class DetalleTecnicoInmueblesDAOHibernate extends GenericDaoHibernate<DetalleTecnicoInmuebles> implements  DetalleTecnicoInmueblesDAO{

    @Override
    public DetalleTecnicoInmuebles listar_detalle_inmueble(int id_inm) {
        
        
         DetalleTecnicoInmuebles dti = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            
            String hql = "SELECT dti FROM DetalleTecnicoInmuebles dti  WHERE dti.bie_inm_id_=:p1 ";
            Query query = session.createQuery(hql); 
            query.setParameter("p1",id_inm);
            dti = (DetalleTecnicoInmuebles)(query.uniqueResult());
 
       }
        catch(Exception e){
            System.out.println("No se pudo Mostrar el Detalle Tecnico de Inmuebles \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo Mostrar el Detalle Tecnico de Inmuebles\\n "+ e.getMessage());

        }
        finally{
            session.close();
        }
        return dti;  
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
