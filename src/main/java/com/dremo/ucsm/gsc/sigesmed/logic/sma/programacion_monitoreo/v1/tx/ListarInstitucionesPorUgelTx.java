/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.programacion_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.OrganizacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ListarInstitucionesPorUgelTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ListarInstitucionesPorUgelTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        /*
        *  Parte para la operacion en la Base de Datos
        */        
        JSONObject requestData = (JSONObject)wr.getData();
        Integer ugelId = requestData.getInt("ugelId");
                
        List<Organizacion> organizaciones = null;
        OrganizacionDao organizacionDao = (OrganizacionDao)FactoryDao.buildDao("sma.OrganizacionDao");
        
        try{
            organizaciones = organizacionDao.listarInstitucionesxUgel(ugelId);
        
        }catch(Exception e){
            logger.log(Level.SEVERE,"Listar instituciones",e);
            System.out.println("No se pudo listar las instituciones\n"+e);
            return WebResponse.crearWebResponseError("No se pudo listar las instituciones", e.getMessage() );
        }
        
        
        //Fin
             
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(Organizacion o:organizaciones ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("idIE", o.getOrgId());
            oResponse.put("nombreIE", o.getNom());
            oResponse.put("nivelIE", o.getNivel().getNom());
            miArray.put(oResponse);
            System.out.println(miArray);
        }
        
        return WebResponse.crearWebResponseExito("Las instituciones fueron listadas exitosamente", miArray);
    }
    
}
