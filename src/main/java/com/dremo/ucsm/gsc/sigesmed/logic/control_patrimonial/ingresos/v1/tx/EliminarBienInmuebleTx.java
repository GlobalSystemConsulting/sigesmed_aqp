/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_patrimonial.ingresos.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;

import org.json.JSONArray;
import java.util.List;

import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesInmuebles;
import com.dremo.ucsm.gsc.sigesmed.core.dao.scp.BienInmuebleDAO;
import com.dremo.ucsm.gsc.sigesmed.core.entity.scp.BienesMuebles;



/**
 *
 * @author Administrador
 */
public class EliminarBienInmuebleTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        BienesInmuebles bin = null;
        
        try{
            JSONObject requestData = (JSONObject)wr.getData();
         //   int org_id = requestData.getInt("org_id");
            int bie_inm_id = requestData.getInt("cod_bie_inm");
            BienInmuebleDAO bie_inmue_dao = (BienInmuebleDAO)FactoryDao.buildDao("scp.BienInmuebleDAO");
            bin = new BienesInmuebles();
            bin.setBie_inm_id(bie_inm_id);
            bie_inmue_dao.eliminar_bien_inmueble(bie_inm_id);
   
        }
        catch(Exception e){
             System.out.println("No se pudo Eliminar los Bienes Inmuebles\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Eliminar los Bienes Inmuebles", e.getMessage() );
        }
         return WebResponse.crearWebResponseExito("El Bien Inmueble se elimino correctamente");
         
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
