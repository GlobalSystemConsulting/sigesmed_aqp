/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.se;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "ascenso", schema="administrativo")
public class Ascenso implements Serializable {

    @Id
    @Column(name = "asc_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_ascenso", sequenceName="administrativo.ascenso_asc_id_seq" )
    @GeneratedValue(generator="secuencia_ascenso")
    private Integer ascId;
    
    @Column(name = "num_res")
    private String numRes;
    
    @Column(name = "fec_res")
    @Temporal(TemporalType.DATE)
    private Date fecRes;
    
    @Column(name = "fec_efe")
    @Temporal(TemporalType.DATE)
    private Date fecEfe;
    
    @Column(name = "esc", length=4)
    private String esc;
    
    @Column(name = "usu_mod")
    private Integer usuMod;
    
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fic_esc_id")
    private FichaEscalafonaria fichaEscalafonaria;

    public Ascenso() {
    }

    public Ascenso(Integer ascId) {
        this.ascId = ascId;
    }
    
    public Ascenso(FichaEscalafonaria fichaEscalafonaria, String numRes, Date fecRes, Date fecEfe, String esc, Integer usuMod, Date fecMod, Character estReg) {
        this.fichaEscalafonaria = fichaEscalafonaria;
        this.numRes = numRes;
        this.fecRes = fecRes;
        this.fecEfe = fecEfe;
        this.esc = esc;
        this.usuMod = usuMod;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }

    public int getAscId() {
        return ascId;
    }

    public void setAscId(Integer ascId) {
        this.ascId = ascId;
    }

    public String getNumRes() {
        return numRes;
    }

    public void setNumRes(String numRes) {
        this.numRes = numRes;
    }

    public Date getFecRes() {
        return fecRes;
    }

    public void setFecRes(Date fecRes) {
        this.fecRes = fecRes;
    }

    public Date getFecEfe() {
        return fecEfe;
    }

    public void setFecEfe(Date fecEfe) {
        this.fecEfe = fecEfe;
    }

    public String getEsc() {
        return esc;
    }

    public void setEsc(String esc) {
        this.esc = esc;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public FichaEscalafonaria getFichaEscalafonaria() {
        return fichaEscalafonaria;
    }

    public void setFichaEscalafonaria(FichaEscalafonaria fichaEscalafonaria) {
        this.fichaEscalafonaria = fichaEscalafonaria;
    }

    @Override
    public String toString() {
        return "Ascenso{" + "ascId=" + ascId + ", numRes=" + numRes + ", fecRes=" + fecRes + ", fecEfe=" + fecEfe + ", esc=" + esc + ", usuMod=" + usuMod + ", fecMod=" + fecMod + ", estReg=" + estReg + ", fichaEscalafonaria=" + fichaEscalafonaria + '}';
    }
    
    
}
