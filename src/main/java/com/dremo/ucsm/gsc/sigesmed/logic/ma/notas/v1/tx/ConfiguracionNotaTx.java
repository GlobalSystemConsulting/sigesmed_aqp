/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.notas.v1.tx;
import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.maestro.plan.ProgramacionAnualDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abel
 */
public class ConfiguracionNotaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
         JSONArray miArray = new JSONArray();
          try{
             
              List<ConfiguracionNota> conf;
              ConfiguracionNotaDao conf_not = (ConfiguracionNotaDao) FactoryDao.buildDao("ma.ConfiguracionNotaDao");
              conf = conf_not.listar_configuracion();
               for(ConfiguracionNota cf : conf){ 
                    JSONObject oResponse = new JSONObject();
                    oResponse.put("lit",cf.getConfig_not_id());
                    oResponse.put("min",cf.getNot_min_num());
                    oResponse.put("max",cf.getNot_max_num());
                    oResponse.put("des",cf.getDes());
                    miArray.put(oResponse); 
               }
               
          }catch(Exception e){
            System.out.println("No se pudo Mostrar la Configuracion de Notas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Mostrar la Configuracion de Notas", e.getMessage() );  

          }
          
             return WebResponse.crearWebResponseExito("Se listo la configuracion de Notas Correctamente",miArray); 
    
        
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
