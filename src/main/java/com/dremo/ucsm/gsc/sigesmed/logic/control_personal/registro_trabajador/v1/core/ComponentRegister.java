/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.BuscarPersonaTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.ListarCargoTrabajadorByTipoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.ListarTrabajadorDetalladoTx;
import com.dremo.ucsm.gsc.sigesmed.logic.control_personal.registro_trabajador.v1.tx.nuevoTrabajadorTx;




/**
 *
 * @author carlos
 */
public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_CONTROL_PERSONAL);        
        
        //Registrnado el Nombre del componente
        component.setName("registroTrabajador");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
//        component.addTransactionPOST("editarPlantilla", EditarPlantillaTx.class);
        component.addTransactionGET("listarTrabajadorDetallado", ListarTrabajadorDetalladoTx.class);
        component.addTransactionGET("listarCargoTrabajador", ListarCargoTrabajadorByTipoTx.class);
        component.addTransactionPOST("nuevoTrabajador", nuevoTrabajadorTx.class);
//        component.addTransactionPUT("eliminarPlantilla", EliminarPlantillaTx.class);
        component.addTransactionGET("buscarPersona", BuscarPersonaTx.class);
      
        
        return component;
    }
}
