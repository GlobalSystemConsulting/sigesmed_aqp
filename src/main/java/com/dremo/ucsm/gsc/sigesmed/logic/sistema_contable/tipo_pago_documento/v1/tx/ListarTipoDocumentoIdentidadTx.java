/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_contable.tipo_pago_documento.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sci.TipoDocumentoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sci.TipoDocumentoIdentidad;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author RE
 */
public class ListarTipoDocumentoIdentidadTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<TipoDocumentoIdentidad> tiposDocumentos = null;
        TipoDocumentoDao tiposDao = (TipoDocumentoDao)FactoryDao.buildDao("sci.TipoDocumentoDao");
        try{
            tiposDocumentos = tiposDao.buscarTodos(TipoDocumentoIdentidad.class);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar los tipos pagos \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar los tipos de documento", e.getMessage() );
        }
        //Fin
        
        /*
        *  Repuesta Correcta
        */
        JSONArray miArray = new JSONArray();
        for(TipoDocumentoIdentidad estado:tiposDocumentos ){
            JSONObject oResponse = new JSONObject();
            oResponse.put("tipoDocumentoID",estado.getTipDocIdeId() );
            oResponse.put("nombre",estado.getNom());
            oResponse.put("fecha",estado.getFecMod().toString());
            oResponse.put("estado",""+estado.getEstReg());
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("Se Listo correctamente los tipos de documentos",miArray);        
        //Fin
    }
    
}
