package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "material_curso_capacitacion", schema = "pedagogico")
public class MaterialCursoCapacitacion implements Serializable {
    @Id
    @Column(name = "mat_cur_cap_id", unique = true, nullable = false)
    @SequenceGenerator(name = "secuencia_material_curso_capacitacion", sequenceName = "pedagogico.material_curso_capacitacion_mat_cur_cap_id_seq")
    @GeneratedValue(generator = "secuencia_material_curso_capacitacion")
    private int matCurCapId;

    @Column(name = "des",nullable = false)
    private String des;
    
    @Column(name = "cos", precision = 6, scale = 3)
    private BigDecimal cos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cur_cap_id")
    private CursoCapacitacion cursoCapacitacion;
    
    public MaterialCursoCapacitacion() {}
    
    public MaterialCursoCapacitacion(String des, BigDecimal cos) {
        this.des = des;
        this.cos = cos;
    }

    public int getMatCurCapId() {
        return matCurCapId;
    }

    public void setMatCurCapId(int matCurCapId) {
        this.matCurCapId = matCurCapId;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public BigDecimal getCos() {
        return cos;
    }

    public void setCos(BigDecimal cos) {
        this.cos = cos;
    }

    public CursoCapacitacion getCursoCapacitacion() {
        return cursoCapacitacion;
    }

    public void setCursoCapacitacion(CursoCapacitacion cursoCapacitacion) {
        this.cursoCapacitacion = cursoCapacitacion;
    }    
}
