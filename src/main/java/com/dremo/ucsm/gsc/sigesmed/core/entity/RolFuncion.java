package com.dremo.ucsm.gsc.sigesmed.core.entity;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="rol_funcion")
@AssociationOverrides({
    @AssociationOverride(name = "claveRolFuncion.rol", 
            joinColumns = @JoinColumn(name = "rol_id")),
    @AssociationOverride(name = "claveRolFuncion.funcionSistema", 
            joinColumns = @JoinColumn(name = "fun_sis_id")) })
public class RolFuncion  implements java.io.Serializable {
    
    private RolFuncionId claveRolFuncion = new RolFuncionId();
    
    private int num;
    private String dep;
    private int tip;

    public RolFuncion() {
    }
    
    public RolFuncion( FuncionSistema funcionSistema, Rol rol, int num,String dep,int tip) {
       claveRolFuncion.setFuncionSistema( funcionSistema );
       claveRolFuncion.setRol(rol);
       this.num = num;
       this.dep = dep;
       this.tip = tip;
    }
    @EmbeddedId
    public RolFuncionId getClaveRolFuncion() {
        return this.claveRolFuncion;
    }
    public void setClaveRolFuncion(RolFuncionId claveRolFuncion) {
        this.claveRolFuncion = claveRolFuncion;
    }
    
    
    @Transient
    public FuncionSistema getFuncionSistema() {
        return claveRolFuncion.getFuncionSistema();
    }
    public void setFuncionSistema(FuncionSistema funcionSistema) {
        claveRolFuncion.setFuncionSistema( funcionSistema );
    }
    
    @Transient
    public Rol getRol() {
        return claveRolFuncion.getRol();
    }
    public void setRol(Rol rol) {
        claveRolFuncion.setRol( rol );
    }

    @Column(name="num")
    public int getNum() {
        return this.num;
    }    
    public void setNum(int num) {
        this.num = num;
    }


    @Column(name="tip")
    public int getTip() {
        return this.tip;
    }    
    public void setTip(int tip) {
        this.tip = tip;
    }
    
    @Column(name="dep")
    public String getDep() {
        return this.dep;
    }    
    public void setDep(String dep) {
        this.dep = dep;
    }


}


