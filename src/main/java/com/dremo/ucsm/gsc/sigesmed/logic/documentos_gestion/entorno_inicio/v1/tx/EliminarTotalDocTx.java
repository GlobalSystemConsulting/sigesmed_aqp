/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class EliminarTotalDocTx implements ITransaction{
    Integer codDoc = 0;
    @Override
    public WebResponse execute(WebRequest wr) {
        /*Lectura del id del documento*/
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            codDoc = requestData.getInt("idDoc");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("No se pudo leer los datos enviados");
        }
        
        /* Lectura en Base de Datos*/
        ItemFileDao iteFilDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        ItemFile filToDelete = iteFilDao.buscarPorID(codDoc);
        
        if (filToDelete.getIteCodCat().equals("D")) {
            //Eliminamos ahroa los hijos del padre contenedor en caso de tener hijos
            iteFilDao.eliminarTotalHijosByPadre(filToDelete);
            
            //Primero eliminamos el padre
            iteFilDao.deleteAbsolute(filToDelete);
        } else {
            //Primero debemos eliminar el itemfile y despues los tipode itemf files que conlleva
            //El segundo paso es eliminar los tipos de item files que tiene en caso de ser un archivo
            
            //Si se elimino completamente los hijos puede eliminarse el padre
            iteFilDao.deleteAbsolute(filToDelete);
            
//            TipoItemFile tifObj = filToDelete.getIteTifIde();
//            if (tifObj != null) {
//                TipoItemFileDao tifDao = (TipoItemFileDao) FactoryDao.buildDao("rdg.TipoItemFileDao");
//                tifObj = tifDao.buscarPorID(tifObj.getTifIde());
//            }
            
        }
        //En caso de que fuera un archivo o en caso de que fuera un directorio
        File dirToDelete = null;
        if(filToDelete.getIteCodCat().equals("D"))
            dirToDelete = new File(filToDelete.getIteUrlDes()+"/"+filToDelete.getIteNom()+"/");
        else{
//            String extension = filToDelete.getIteTifIde().getTifTipExt().getTexNom();
//            dirToDelete = new File(filToDelete.getIteUrlDes()+"/"+filToDelete.getIteNom()+"."+extension);
        }
        try {
            FileUtils.deleteDirectory(dirToDelete);
        } catch (IOException ex) {
            Logger.getLogger(EliminarDirectorioTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("Se borro completamente el documento");
    }
    
}
