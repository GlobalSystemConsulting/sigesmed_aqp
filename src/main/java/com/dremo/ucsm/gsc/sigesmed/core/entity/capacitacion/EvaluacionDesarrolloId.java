package com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EvaluacionDesarrolloId implements Serializable {
    @Column(name = "des_tem_cap_id", nullable = false)
    protected int desTemCapId;

    @Column(name = "per_id", nullable = false)
    protected int perId;

    public EvaluacionDesarrolloId() {}
    
    public EvaluacionDesarrolloId(int desTemCapId, int perId) {
        this.desTemCapId = desTemCapId;
        this.perId = perId;
    }

    public int getDesTemCapId() {
        return desTemCapId;
    }

    public int getPerId() {
        return perId;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.desTemCapId;
        hash = 79 * hash + this.perId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EvaluacionDesarrolloId other = (EvaluacionDesarrolloId) obj;
        if (this.desTemCapId != other.desTemCapId) {
            return false;
        }
        if (this.perId != other.perId) {
            return false;
        }
        return true;
    }
}
