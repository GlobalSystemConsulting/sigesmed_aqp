/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.scp;

/**
 *
 * @author Administrador
 */
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.logging.Logger;

@Entity
@Table(name="anexo_bienes", schema="administrativo")
public class AnexoBienes {
    
    @Id
    @Column(name="an_id" , unique=true, nullable=false)
    @SequenceGenerator(name="secuencia_inv_tra",sequenceName="administrativo.anexo_bienes_an_id_seq")
    @GeneratedValue(generator="secuencia_inv_tra")
    private int an_id;
    
    @Column(name="an_des")
    private String an_des;
    
    @Column(name="fec_mod")
    private Date fec_mod;
    
    @Column(name="usu_mod")
    private int usu_mod;
    
    @Column(name="est_reg")
    private char est_reg;

    public AnexoBienes(int an_id, String an_des, Date fec_mod, int usu_mod, char est_reg) {
        this.an_id = an_id;
        this.an_des = an_des;
        this.fec_mod = fec_mod;
        this.usu_mod = usu_mod;
        this.est_reg = est_reg;
    }

    public AnexoBienes() {
    }

    public void setAn_id(int an_id) {
        this.an_id = an_id;
    }

    public void setAn_des(String an_des) {
        this.an_des = an_des;
    }

    public void setFec_mod(Date fec_mod) {
        this.fec_mod = fec_mod;
    }

    public void setUsu_mod(int usu_mod) {
        this.usu_mod = usu_mod;
    }

    public void setEst_reg(char est_reg) {
        this.est_reg = est_reg;
    }

    public int getAn_id() {
        return an_id;
    }

    public String getAn_des() {
        return an_des;
    }

    public Date getFec_mod() {
        return fec_mod;
    }

    public int getUsu_mod() {
        return usu_mod;
    }

    public char getEst_reg() {
        return est_reg;
    }
    
    
    
}
