package com.dremo.ucsm.gsc.sigesmed.core.dao;

import com.dremo.ucsm.gsc.sigesmed.core.entity.ModuloSistema;
import java.util.List;

public interface ModuloSistemaDao extends GenericDao<ModuloSistema> {

    public List<ModuloSistema> listarConSubModulos();

    public List<ModuloSistema> listarModulos();
}
