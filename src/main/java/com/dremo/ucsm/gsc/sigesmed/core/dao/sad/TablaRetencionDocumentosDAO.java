/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.sad;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sad.TablaRetencionDocumentos;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import java.util.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public interface TablaRetencionDocumentosDAO extends GenericDao<TablaRetencionDocumentos> {
    
    public List<TablaRetencionDocumentos> buscarPorCodigo(int SerieID);
    
}
