package com.dremo.ucsm.gsc.sigesmed.logic.matricula_institucional.agregar_estudiante.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mmi.DomicilioDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mmi.Domicilio;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarDomiciliosTx implements ITransaction {

    @Override
    public WebResponse execute(WebRequest wr) {

        JSONObject data = (JSONObject) wr.getData();
        JSONObject persona = data.getJSONObject("persona");

        DomicilioDaoHibernate hb = new DomicilioDaoHibernate();
        JSONArray jsonDomicilios = new JSONArray();
        long perId;
        List<Domicilio> misDomicilios;

        try {
            perId = persona.getInt("perId");
            misDomicilios = hb.find4Estudiante(perId);

            for (Domicilio dom : misDomicilios) {
                JSONObject temp = new JSONObject();
                temp.put("domCod", dom.getId().getDomId());
                temp.put("domAnyo", dom.getDomAno());
                temp.put("domUbi", dom.getUbiCod());
                temp.put("domDir", dom.getDomDir());
                temp.put("domRef", dom.getDomRef());
                temp.put("domTel", dom.getDomTel());
                jsonDomicilios.put(temp);
            }

        } catch (Exception e) {
            return WebResponse.crearWebResponseError("Error al cargar los domicilios! ", e.getMessage());
        }

        return WebResponse.crearWebResponseExito("Listaron las domicilios", jsonDomicilios);
    }

}
