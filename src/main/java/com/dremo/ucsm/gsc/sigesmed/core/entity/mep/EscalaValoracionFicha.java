package com.dremo.ucsm.gsc.sigesmed.core.entity.mep;


import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author geank
 */
@Entity
@Table(name="escala_valoracion_ficha"
    ,schema="pedagogico"
)
@DynamicUpdate(value=true)
public class EscalaValoracionFicha  implements java.io.Serializable,ElementoFicha {

    @Id 
    @Column(name="esc_val_fic_id", unique=true, nullable=false)
    @SequenceGenerator(name = "secuencia_escala_valoracion_ficha", sequenceName="pedagogico.escala_valoracion_ficha_esc_val_fic_id_seq" )
    @GeneratedValue(generator="secuencia_escala_valoracion_ficha")
    private int escValFicId;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fic_eva_per_id")
    private FichaEvaluacionPersonal fichaEvaluacionPersonal;
    
    @Column(name="nom", nullable=false, length=100)
    private String nom;
    
    @Column(name="des")
    private String des;
    
    @Column(name="val", nullable=false)
    private int val;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public EscalaValoracionFicha() {

    }
    public EscalaValoracionFicha(int id, String nom, String des,int val) {
        this.escValFicId = id;
        this.nom = nom;
        this.val = val;
        this.des = des;
    }
    public EscalaValoracionFicha(String nom, String des,int val) {
        this.nom = nom;
        this.val = val;
        this.des = des;
    }
    public EscalaValoracionFicha(String nom, String des,int val,Date fecMod, char estReg,FichaEvaluacionPersonal fichaEvaluacionPersonal) {
        this.nom = nom;
        this.val = val;
        this.des = des;
        this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
        this.fecMod = fecMod;
        this.estReg = estReg;
    }
    public EscalaValoracionFicha(int escValFicId, String nom, int val) {
        this.escValFicId = escValFicId;
        this.nom = nom;
        this.val = val;
    }
    public EscalaValoracionFicha(int escValFicId, FichaEvaluacionPersonal fichaEvaluacionPersonal, String nom, String des, int val) {
       this.escValFicId = escValFicId;
       this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
       this.nom = nom;
       this.des = des;
       this.val = val;
    }
    public int getEscValFicId() {
        return this.escValFicId;
    }
    
    public void setEscValFicId(int escValFicId) {
        this.escValFicId = escValFicId;
    }
    
    @Override
    public FichaEvaluacionPersonal getFichaEvaluacionPersonal() {
        return this.fichaEvaluacionPersonal;
    }
    @Override
    public void setFichaEvaluacionPersonal(FichaEvaluacionPersonal fichaEvaluacionPersonal) {
        this.fichaEvaluacionPersonal = fichaEvaluacionPersonal;
    }

    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDes() {
        return this.des;
    }
    
    public void setDes(String des) {
        this.des = des;
    }
    
    public int getVal() {
        return this.val;
    }
    
    public void setVal(int val) {
        this.val = val;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    @Override
    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    @Override
    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @Override
    public ElementoFicha copyFromOther(ElementoFicha elemento) {
        if(elemento != null && elemento instanceof EscalaValoracionFicha){
            EscalaValoracionFicha escala = (EscalaValoracionFicha)elemento;
            this.setNom(escala.getNom());
            this.setDes(escala.getDes());
            this.setVal(escala.getVal());
        }
        return this;
    }

    @Override
    public String toString(){
        return "{\"id\":" + escValFicId+",\"nom\":\""+nom +
                "\",\"des\":\"" + des + "\",\"val\":" + val + "}";
    }
}


