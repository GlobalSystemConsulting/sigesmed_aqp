package com.dremo.ucsm.gsc.sigesmed.logic.capacitacion.curso_capacitacion.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion.HorarioSedeCapacitacionDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.HorarioSedeCapacitacion;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class ListarHorariosTx implements ITransaction {
    
    private static final Logger logger = Logger.getLogger(ListarHorariosTx.class.getName());
    
    @Override
    public WebResponse execute(WebRequest wr) {
        JSONObject data = (JSONObject) wr.getData();
        int opt = data.getInt("opt");
        WebResponse response = null;

        switch (opt) {
            case 0:
                response = listarParaAsistencia(data.getInt("sedCod"));
                break;

            case 1:
                response = listarPorSede(data.getInt("sedCod"));
                break;
        }

        return response;   
    }
    
    private WebResponse listarParaAsistencia(int sedCod) {
        try {
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");
            List<HorarioSedeCapacitacion> horarios = horarioSedeCapacitacionDao.buscarParaOnTime(sedCod, Calendar.getInstance().get(Calendar.DAY_OF_WEEK), new Date());
            
            JSONArray array  = new JSONArray();
            DateFormat format = new SimpleDateFormat("HH:mm");

            for(HorarioSedeCapacitacion shift: horarios) {
                JSONObject object = new JSONObject();
                object.put("horCod", shift.getHorSedCapId());
                object.put("des", format.format(shift.getTurIni()) + " - " + format.format(shift.getTurFin()));
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarParaAsistencia", e);
            return WebResponse.crearWebResponseError("Error al listar los participantes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private WebResponse listarPorSede(int sedCod) {
        try {
            HorarioSedeCapacitacionDao horarioSedeCapacitacionDao = (HorarioSedeCapacitacionDao) FactoryDao.buildDao("capacitacion.HorarioSedeCapacitacionDao");
            List<HorarioSedeCapacitacion> horarios = horarioSedeCapacitacionDao.buscarPorSede(sedCod);
            
            JSONArray array  = new JSONArray();

            for(HorarioSedeCapacitacion shift: horarios) {
                JSONObject object = new JSONObject();
                object.put("horCod", shift.getHorSedCapId());
                object.put("des", getDay(shift.getDia(), shift.getTurIni(), shift.getTurFin()));
                array.put(object);
            }
            
            return WebResponse.crearWebResponseExito("Se listo correctamente", WebResponse.OK_RESPONSE).setData(array);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "listarPorSede", e);
            return WebResponse.crearWebResponseError("Error al listar los participantes", WebResponse.BAD_RESPONSE);
        }
    }
    
    private String getDay(int day, Date ini, Date fin) {
        String name = "";
        DateFormat format = new SimpleDateFormat("HH:mm");
        
        switch(day) {
            case 1: name = "Domingo " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 2: name = "Lunes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 3: name = "Martes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 4: name = "Miércoles " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 5: name = "Jueves " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 6: name = "Viernes " + format.format(ini) + " - " + format.format(fin);
                    break;
            case 7: name = "Sábado " + format.format(ini) + " - " + format.format(fin);
                    break;
        }
        
        return name;
    }
}
