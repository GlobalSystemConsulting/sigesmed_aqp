/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.CicloEducativo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
/**
 *
 * @author abel
 */
public class InsertarCicloTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        CicloEducativo nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int disenoID = requestData.getInt("diseñoID");
            int cicloID = requestData.optInt("cicloID");
            String abreviacion = requestData.getString("abreviacion");
            String nombre = requestData.getString("nombre");
            String descripcion = requestData.getString("descripcion");
            
            nuevo = new CicloEducativo(cicloID,abreviacion , nombre, descripcion, disenoID, new Date(), wr.getIdUsuario(), 'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Ciclo Educativo, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        try{
            disenoDao.insertarCiclo(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo registrar el Ciclo Educativo\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Ciclo Educativo", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("cicloID",nuevo.getCicEduId());
        return WebResponse.crearWebResponseExito("El registro del Ciclo Educativo se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
