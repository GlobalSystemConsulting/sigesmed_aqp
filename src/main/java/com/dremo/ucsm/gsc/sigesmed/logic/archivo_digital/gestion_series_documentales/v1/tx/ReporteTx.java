/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.archivo_digital.gestion_series_documentales.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.dremo.ucsm.gsc.sigesmed.util.GCell;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Paragraph;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author Jeferson
 */
public class ReporteTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        
        List<List<String>> tabla_series_generales = new ArrayList<>();
        List<List<String>> tabla_series_especificas = new ArrayList<>();
        List<List<String>> tabla_unidades_especificas = new ArrayList<>();
        int cantidad ;
       
        try{ 
            // Llenamos la informacion a la tabla de la series generales
            JSONObject requestData = (JSONObject)wr.getData(); 
            JSONArray series_generales = requestData.getJSONArray("series_generales");
            cantidad = series_generales.length() / 3;
            if(requestData.length() > 0){
                
                int serie = 0;
                for(int i=1;i<4;i++){
                    List<String>serie_col = new ArrayList<>();
                    for(int c=0;c<cantidad;c++){               
                        if(((c+1)*i)>series_generales.length()){break;}
                        Object o = series_generales.get(serie);
                        JSONObject objOpe = (JSONObject)o;
                        String nombre_serie= objOpe.getString("nombre_serie"); 
                        serie_col.add(nombre_serie);
                        serie++;
                    }
                    tabla_series_generales.add(serie_col);
                 }   
            }
            
            
            
        }catch(Exception e){
            System.out.println("No se pudo Generar el Reporte \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Generar el Reporte ", e.getMessage() );
        }
        /////////////////////////////////////////////////////////////////////////////////
        
        
        
        
        /* PREPARAMOS EL REPORTE  */
        
         Mitext m = null;        
        try {
            
            
            m = new Mitext(true,"SIGESMED");
            
            /* Preparamos la primera tabla (SERIES DOCUMENTALES GENERALES)*/
            float[] columnWidths={4,4,4};
            GTabla t_general = new GTabla(columnWidths);
            t_general.addHeaderCell(new Cell(1,12).setBorder(Border.NO_BORDER).add(new Paragraph("  SERIES DOCUMENTALES GENERALES ")).setFontSize(10).setBorder(Border.NO_BORDER));
            GCell[] cell ={t_general.createCellCenter(1,1),t_general.createCellCenter(1,1),t_general.createCellCenter(1,1)};
           
            
            /*Preparamos nuestro arreglo de Serie Generales para que imprima en filas */
            String[] serie_fila = new String[3];
            for(int i = 0;i<cantidad;i++){
                for(int j=0;j<3;j++){
                    if(tabla_series_generales.get(j).get(i) == ""){break;}
                    serie_fila[j]=tabla_series_generales.get(j).get(i);
                }
                t_general.processLineCell(serie_fila,cell);
            }
            m.agregarTabla(t_general); /*Insertamos nuestra primera tabla al reporte */
            
            
            /*Preparamos la segunda tabla (SERIES DOCUMENTALES ESPECIFICAS)*/
            
            JSONObject requestData = (JSONObject)wr.getData(); 
            
            JSONArray series_especificas = requestData.getJSONArray("series_especificas");
            
            
            float[] columnWidths2={6,6};
            GTabla t_especifica = new GTabla(columnWidths2);
            t_especifica.addHeaderCell(new Cell(1,12).setBorder(Border.NO_BORDER).add(new Paragraph("  SERIES DOCUMENTALES ESPECIFICAS ")).setFontSize(10).setBorder(Border.NO_BORDER));
          
            /* Obtenemos el tamanyo del numero de series por cada unidad organica */
            
            
            List<Integer> tamanyo_unidades = new ArrayList<>();
            for(int i=0;i<series_especificas.length();i++){
                   int longuitud = series_especificas.getJSONArray(i).length();
                   tamanyo_unidades.add(longuitud); 
            }
            
            /* Preparamos nuestro reporte de la tabla de series especificas */
            /*Preparamos el formato de plantilla*/
            GCell[] cell2 = new GCell[tamanyo_unidades.size()];
            for(int i=0;i<tamanyo_unidades.size();i++){
                /*Unidad Organica*/
                cell2[i] = t_especifica.createCellCenter(tamanyo_unidades.get(i),1);
                /* Series */
                for(int j=0 ;j<tamanyo_unidades.get(i);j++){   /*Preparamos el formato de las celdas*/
                    cell2[j+1] =t_especifica.createCellCenter(1,1);
                }    
                
                t_general.processLineCell(serie_fila,cell2);  
            }
            
            List<String> data_series_esp = new ArrayList<>();
            
            
            /*Insertamos la data al formato de la tabla ya creada*/
            for(int u=0;u<series_especificas.length();u++){
                data_series_esp.add(series_especificas.getString(u));
                for(int s=0;s<series_especificas.length();s++){
                    data_series_esp.add(series_especificas.getJSONArray(u).getString(s));
                }
            }
            String[] data_series = new String[data_series_esp.size()];
            data_series = data_series_esp.toArray(data_series);
            
            
            t_general.processLineCell(data_series,cell2);
            m.agregarTabla(t_especifica);
            
            
            m.cerrarDocumento();
            
            JSONArray miArray = new JSONArray();
            JSONObject oResponse = new JSONObject();        
            oResponse.put("datareporte",m.encodeToBase64());
            miArray.put(oResponse);                       
        
        return WebResponse.crearWebResponseExito("Se genero el reporte correctamente",miArray);   
            
        }catch (Exception e){
            System.out.println("No se pudo Generar el Reporte \n"+e);
            return WebResponse.crearWebResponseError("No se pudo Generar el Reporte ", e.getMessage() );
        }
        
         
        
        
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
