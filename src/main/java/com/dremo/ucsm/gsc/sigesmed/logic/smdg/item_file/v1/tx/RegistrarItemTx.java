/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.item_file.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.PlantillaFichaInstitucionalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaFichaInstitucional;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaTipo;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.TipoGrupo;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */

public class RegistrarItemTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray grupos = requestData.getJSONArray("grupos");
        
        int total = requestData.optInt("total");
        int iteide = requestData.optInt("iteide");        
        try {
            Date fevfec = formatter.parse(requestData.optString("fevfec"));
        } catch (ParseException ex) {
            Logger.getLogger(RegistrarItemTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        int feveva = requestData.optInt("feveva");
        int fevesp = requestData.optInt("fevesp");
        int plaide = requestData.optInt("plaide");
        
//        PlantillaFichaInstitucional plantilla = new PlantillaFichaInstitucional(codigo, nombre, new Date(), new PlantillaTipo(tipo));
        PlantillaFichaInstitucionalDao plantillaDao = (PlantillaFichaInstitucionalDao)FactoryDao.buildDao("smdg.PlantillaFichaInstitucionalDao");
        
//        plantillaDao.insert(plantilla);
        
        List<PlantillaGrupo> gruposList = new ArrayList<PlantillaGrupo>();
        
        PlantillaGrupo grupo = null;
//        PlantillaGrupoDao grupoDao = (PlantillaGrupoDao)FactoryDao.buildDao("smdg.PlantillaGrupoDao");
        
        for(int i = 0; i < grupos.length(); ++i){
//            grupo = new PlantillaGrupo(grupos.getJSONObject(i).optString("gruNom"), new TipoGrupo(grupos.getJSONObject(i).optInt("Tipo")), plantilla);
//            grupoDao.insert(grupo);
                        
            JSONArray indicadores = grupos.getJSONObject(i).getJSONArray("indicadores");
            List<PlantillaIndicadores> indicadoresList = new ArrayList<PlantillaIndicadores>();
            
            PlantillaIndicadores indicador = null;
//            PlantillaIndicadoresDao indicadorDao = (PlantillaIndicadoresDao)FactoryDao.buildDao("smdg.PlantillaIndicadoresDao");
            for(int j = 0; j < indicadores.length(); ++j){
                indicador = new PlantillaIndicadores(indicadores.getJSONObject(j).optString("indNom"), grupo);
//                indicadorDao.insert(indicador);
                
//                JSONArray items = indicadores.getJSONObject(j).getJSONArray("items");
//                Set<PlantillaItems> itemsList = new HashSet<PlantillaItems>();
//                
//                PlantillaItems item = null;
////                PlantillaItemsDao itemDao = (PlantillaItemsDao)FactoryDao.buildDao("smdg.PlantillaItemsDao");
//                for(int k = 0; k < items.length(); ++k){
//                    item = new PlantillaItems(items.getJSONObject(k).optString("iteNom"), items.getJSONObject(k).optInt("itePun"), indicador);
////                    itemDao.insert(item);
//                    itemsList.add(item);
//                }                
//                indicador.setItems(itemsList);
                indicadoresList.add(indicador);
                
            }
            grupo.setIndicadores(indicadoresList);
            gruposList.add(grupo);
        }
        
//        plantilla.setGrupos(gruposList);
//        plantillaDao.insert(plantilla);
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();            
//        oResponse.put("plaId",plantilla.getPfiInsId());
        
        return WebResponse.crearWebResponseExito("El registro la plantilla de Ficha", oResponse);
    }
}
