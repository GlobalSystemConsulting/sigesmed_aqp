/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mnt;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mnt.PeticionDao;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.FuncionSistema;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mnt.Peticion;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class PeticionDaoHibernate extends GenericDaoHibernate<Peticion> implements PeticionDao{

    @Override
    public char obtenerTipoPeticionPorIdMensaje(int mensajeID) {
        char tipoPeticion=' ';
        Session session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();            
        try{
            String hql="SELECT p.petTip FROM Peticion p WHERE p.mensaje.mensajeID=:p1";
            Query query=session.createQuery(hql);
            query.setParameter("p1",mensajeID);
            tipoPeticion=(char)query.uniqueResult();
            System.out.println("t: "+tipoPeticion);
        }catch(Exception e){
            t.rollback();
            System.err.println("No se puede obtener el tipo de peticion \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el tipo de peticion\\n" +e.getMessage());
        }finally{
            session.close();
        }
        return tipoPeticion;
    }

    @Override
    public FuncionSistema obtenerFuncionPorIdMensaje(int mensajeID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        FuncionSistema func=null;
        try{
            String hql="SELECT p.funcion FROM Peticion p WHERE p.mensaje";
        }catch(Exception e){
           System.err.println("No se puede obtener la funcion del mensaje \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se puede obtener la funcion del mensaje\\n" +e.getMessage()); 
        }finally{
            session.close();
        }
        return func;
    }

    @Override
    public Peticion obtenerPeticionPorIdmensaje(int mensajeID) {
        Session session=HibernateUtil.getSessionFactory().openSession();
        Peticion pet=null;
        try{
            String hql="SELECT p FROM Peticion p LEFT JOIN FETCH p.funcion func  LEFT JOIN FETCH func.subModuloSistema submod LEFT JOIN FETCH submod.moduloSistema mod WHERE p.mensaje.mensajeID=:p1";
            Query q=session.createQuery(hql);
            q.setParameter("p1",mensajeID);
            pet=(Peticion)q.uniqueResult();
            q.setMaxResults(1);
        }catch(Exception e){
           System.err.println("No se puede obtener la funcion del mensaje \\n "+e.getMessage());
            throw new UnsupportedOperationException("No se puede obtener la funcion del mensaje\\n" +e.getMessage()); 
        }finally{
            session.close();
        }
        return pet;
    }
    
}
