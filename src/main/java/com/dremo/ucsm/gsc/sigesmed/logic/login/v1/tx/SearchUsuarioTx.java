/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.login.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.UsuarioDao;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.core.entity.UsuarioSession;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class SearchUsuarioTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        String username = "";
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            username = requestData.getString("nombre");
        }catch(Exception e){
            return WebResponse.crearWebResponseError("Los datos enviados son incorrectos", e.getMessage() );
        }
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<UsuarioSession> sessiones = null;
        try{
            sessiones = ((UsuarioDao)FactoryDao.buildDao("UsuarioDao")).buscarPorUsuario(username);
        
        }catch(Exception e){
            System.out.println("\n"+e);
            return WebResponse.crearWebResponseError("No se pudo buscar por nombre", e.getMessage() );
        }
        
        //validar el usuario
        if(sessiones != null && sessiones.size()>0){
            
            JSONObject organizacionAnterior = null;
            int organizacionAnteriorID = 0;
            
            JSONArray organizaciones = new JSONArray();
            JSONArray roles = new JSONArray();
            for(UsuarioSession session:sessiones){

                if( organizacionAnteriorID == session.getOrganizacion().getOrgId() ){
                    
                    JSONObject oRol = new JSONObject();
                    oRol.put("rolID",session.getRol().getRolId() );
                    oRol.put("nombre",session.getRol().getNom());
                        
                    roles.put(oRol);
                }
                else{
                    if(roles.length()>0){
                        organizacionAnterior.put("roles", roles);
                        roles = new JSONArray();
                    }
                    
                    JSONObject oOrg = new JSONObject();
                    oOrg.put("organizacionID",session.getOrganizacion().getOrgId() );
                    oOrg.put("nombre",session.getOrganizacion().getNom());
                    
                    JSONObject oRol = new JSONObject();
                    oRol.put("rolID",session.getRol().getRolId() );
                    oRol.put("nombre",session.getRol().getNom());
                    roles.put(oRol);
                
                    organizacionAnterior = oOrg;
                    organizacionAnteriorID = session.getOrganizacion().getOrgId();
                    organizaciones.put(oOrg);
                }                
            }
            if(roles.length()>0){
                organizacionAnterior.put("roles", roles);
            }
            
            return WebResponse.crearWebResponseExito("el usuario se encuentra en la BD", organizaciones);
        }else{
            return WebResponse.crearWebResponseError("Disculpe, el nombre de usuario es incorrecto o no existe");
        }
    }
    
}
