/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.di;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.entity.di.TipoPariente;

/**
 *
 * @author Administrador
 */
public interface TipoParienteDao extends GenericDao<TipoPariente>{
    
    public String buscarUltimoCodigo();
    
}
