/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.directorio.estudiante.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.directorio.estudiante.v1.tx.ListarEstudiantesPorOrganizacionIETx;


/**
 *
 * @author ucsm
 */

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        //Asiganando el modulo al que pertenece
        WebComponent component = new WebComponent(Sigesmed.MODULO_DIRECTORIO);        
        
        //Registrando el Nombre del componente
        component.setName("estudiante");
        //Version del componente
        component.setVersion(1);
        //Lista de operaciones de logica, propias del componente
        //component.addTransactionPOST("insertarApoderado", InsertarApoderadoTx.class);      
        component.addTransactionGET("listarEstudiantesPorOrganizacionIE", ListarEstudiantesPorOrganizacionIETx.class);
        //component.addTransactionDELETE("eliminarApoderado", EliminarApoderadoTx.class);
        //component.addTransactionPUT("actualizarApoderado", ActualizarApoderadoTx.class);
        
        return component;
    }
}
