/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.smdg;

import com.dremo.ucsm.gsc.sigesmed.core.dao.smdg.*;
import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import com.dremo.ucsm.gsc.sigesmed.core.entity.smdg.PlantillaIndicadores;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrador
 */
public class PlantillaIndicadoresDaoHibernate extends GenericDaoHibernate<PlantillaIndicadores> implements PlantillaIndicadoresDao{
    @Override
    public String buscarUltimoCodigo() {
        String codigo = null;
//        Session session = HibernateUtil.getSessionFactory().openSession();
//        try{
//            //listar ultimo codigo de tramite
//            String hql = "SELECT a.apoId FROM Apoderado a ORDER BY a.apoId DESC ";
//            Query query = session.createQuery(hql);
//            query.setMaxResults(1);
//            codigo = ((String)query.uniqueResult());
//            
//            //solo cuando no hay ningun registro aun
//            if(codigo==null)
//                codigo = "0000";
//        
//        }catch(Exception e){
//            System.out.println("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());
//            throw new UnsupportedOperationException("No se pudo encontrar el ultimo codigo \\n "+ e.getMessage());            
//        }
//        finally{
//            session.close();
//        }
        return codigo;
    }
    @Override
    public PlantillaIndicadores obtenerPlantillaIndicadores(int indId){
        Session session = HibernateUtil.getSessionFactory().openSession();
        PlantillaIndicadores indicador= null;
        Transaction t = session.beginTransaction();
        try{                                        
//            plantilla = (PlantillaFichaInstitucional)session.get(PlantillaFichaInstitucional.class, plaId);
            String hql = "SELECT g FROM PlantillaIndicadores i "
//                    + "join fetch i.items it "                    
                    + "WHERE i.pinId = " + indId;
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            indicador = (PlantillaIndicadores)query.uniqueResult();            
            t.commit();
                      
        }catch(Exception e){
            t.rollback();
            System.out.println("No se pudo obtener el grupo \\n "+ e.getMessage());
            throw new UnsupportedOperationException("No se pudo obtener el grupo \\n "+ e.getMessage());            
        }
        finally{
            session.close();
        }
        
        return indicador;
    }
}
