/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "seccion_carpeta_pedagogica", schema = "pedagogico")

public class SeccionCarpetaPedagogica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sec_car_ped_id")
    private Long secCarPedId;
    @Column(name = "ord")
    private String ord;
    @Basic(optional = false)
    @Column(name = "nom")
    private String nom;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "est_reg")
    private Character estReg;
    @ManyToMany(mappedBy = "seccionCarpetaPedagogicaList", fetch = FetchType.LAZY)
    private List<CarpetaPedagogica> carpetaPedagogicaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "secCarPedId", fetch = FetchType.LAZY)
    private List<ContenidoSeccionesCarpetaPedagogica> contenidoSeccionesCarpetaPedagogicaList;

    public SeccionCarpetaPedagogica() {
    }

    public SeccionCarpetaPedagogica(Long secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public SeccionCarpetaPedagogica(Long secCarPedId, String nom) {
        this.secCarPedId = secCarPedId;
        this.nom = nom;
    }

    public Long getSecCarPedId() {
        return secCarPedId;
    }

    public void setSecCarPedId(Long secCarPedId) {
        this.secCarPedId = secCarPedId;
    }

    public String getOrd() {
        return ord;
    }

    public void setOrd(String ord) {
        this.ord = ord;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    @XmlTransient
    public List<CarpetaPedagogica> getCarpetaPedagogicaList() {
        return carpetaPedagogicaList;
    }

    public void setCarpetaPedagogicaList(List<CarpetaPedagogica> carpetaPedagogicaList) {
        this.carpetaPedagogicaList = carpetaPedagogicaList;
    }

    @XmlTransient
    public List<ContenidoSeccionesCarpetaPedagogica> getContenidoSeccionesCarpetaPedagogicaList() {
        return contenidoSeccionesCarpetaPedagogicaList;
    }

    public void setContenidoSeccionesCarpetaPedagogicaList(List<ContenidoSeccionesCarpetaPedagogica> contenidoSeccionesCarpetaPedagogicaList) {
        this.contenidoSeccionesCarpetaPedagogicaList = contenidoSeccionesCarpetaPedagogicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (secCarPedId != null ? secCarPedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeccionCarpetaPedagogica)) {
            return false;
        }
        SeccionCarpetaPedagogica other = (SeccionCarpetaPedagogica) object;
        if ((this.secCarPedId == null && other.secCarPedId != null) || (this.secCarPedId != null && !this.secCarPedId.equals(other.secCarPedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.SeccionCarpetaPedagogica[ secCarPedId=" + secCarPedId + " ]";
    }
    
}
