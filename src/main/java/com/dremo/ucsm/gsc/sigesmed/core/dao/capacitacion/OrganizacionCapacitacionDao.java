package com.dremo.ucsm.gsc.sigesmed.core.dao.capacitacion;

import com.dremo.ucsm.gsc.sigesmed.core.dao.GenericDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.capacitacion.Organizacion;

public interface OrganizacionCapacitacionDao extends GenericDao<Organizacion> {
    public Organizacion buscarPorId(int orgID);
    public String buscarCapacitacionOrg(int codCap);
    public String buscarCapacitacionAut(int codCap);
}
