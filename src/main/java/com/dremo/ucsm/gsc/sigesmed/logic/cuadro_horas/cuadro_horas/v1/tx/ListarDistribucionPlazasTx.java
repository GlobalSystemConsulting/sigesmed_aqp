/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.cuadro_horas.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DistribucionHoraGradoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraArea;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.DistribucionHoraGrado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.List;

/**
 *
 * @author abel
 */
public class ListarDistribucionPlazasTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        int planID = 0;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            planID = requestData.getInt("planID");
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion de plazas, datos incorrectos", e.getMessage() );
        }
        //Fin        
        
        /*
        *  Parte para la operacion en la Base de Datos
        */
        List<DistribucionHoraGrado> distribucion = null;
        
        try{
            DistribucionHoraGradoDao disDao = (DistribucionHoraGradoDao)FactoryDao.buildDao("mech.DistribucionHoraGradoDao");
            
            distribucion = disDao.listarDistribucionHorasGradoPorPlanEstudios(planID);
        
        }catch(Exception e){
            System.out.println("No se pudo Listar Listar la distribucion de plazas\n"+e);
            return WebResponse.crearWebResponseError("No se pudo Listar la distribucion de plazas", e.getMessage() );
        }
        //Fin        
        /*
        *  Repuesta Correcta
        */
        
        JSONArray miArray = new JSONArray();
        
        for(DistribucionHoraGrado d:distribucion ){
            JSONObject oResponse = new JSONObject();
            
            oResponse.put("disGradoID",d.getDisHorGraId());
            oResponse.put("hora",d.getHorAsi());
            oResponse.put("gradoID",d.getGraId());
            oResponse.put("plazaID",d.getPlaMagId());
            oResponse.put("seccionID",""+d.getSecId());
            
            JSONArray aDisA = new JSONArray();
            for(DistribucionHoraArea da : d.getDistribucionAreas()){
                JSONObject oDisA = new JSONObject();
                
                oDisA.put("disAreaID", da.getDisHorAreId() );
                oDisA.put("hora", da.getHorAsi() );
                oDisA.put("areaID", da.getAreCurId() );
                oDisA.put("disGradoID", da.getDisHorGraId() );
                aDisA.put(oDisA);
            }
            //verificando si tiene asignado horas de tutoria
            if(d.getHorTut()>0){
                JSONObject oDisA = new JSONObject();
                oDisA.put("disAreaID", 0 );
                oDisA.put("hora", d.getHorTut() );
                oDisA.put("areaID", -1 );
                oDisA.put("disGradoID", d.getDisHorGraId() );
                aDisA.put(oDisA);
            }
            oResponse.put("areas", aDisA);
            
            miArray.put(oResponse);
        }
        
        return WebResponse.crearWebResponseExito("La distribucion de las plazas se listo correctamente",miArray);        
        //Fin
    }
    
}

