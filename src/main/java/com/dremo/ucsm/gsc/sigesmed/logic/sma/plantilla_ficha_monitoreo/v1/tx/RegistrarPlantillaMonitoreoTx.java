/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sma.plantilla_ficha_monitoreo.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.CompromisosGestionDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.ItemsDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.sma.PlantillaFichaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.CompromisosGestion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.Items;
import com.dremo.ucsm.gsc.sigesmed.core.entity.sma.PlantillaFicha;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class RegistrarPlantillaMonitoreoTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {        
        /*
        *  Parte para la operacion en la Base de Datos
        */                
        JSONObject requestData = (JSONObject)wr.getData();
        JSONArray compromisos = requestData.getJSONArray("compromisos");
        
        String codigo = "";
        String nombre = requestData.optString("nombre");
        String descripcion = requestData.optString("descripcion");
        
        PlantillaFichaDao plantillaDao = (PlantillaFichaDao)FactoryDao.buildDao("sma.PlantillaFichaDao");
        
        Object o = plantillaDao.llave(PlantillaFicha.class, "plfId");
        System.out.println("Objeto o: "+o.toString());
        
        codigo =  nombre.substring(0, 3);
                
        PlantillaFicha plantilla = new PlantillaFicha(codigo, nombre, new Date(),descripcion);
        plantillaDao.insert(plantilla);
        
        List<CompromisosGestion> compromisosList = new ArrayList<>();
        
        CompromisosGestion compromiso = null;
        CompromisosGestionDao comGesDao = (CompromisosGestionDao)FactoryDao.buildDao("sma.CompromisosGestionDao");
        ItemsDao itemDao = (ItemsDao)FactoryDao.buildDao("sma.ItemsDao");
        for(int i = 0; i < compromisos.length(); ++i){
            compromiso = new CompromisosGestion(compromisos.getJSONObject(i).optString("comNom"), plantilla);
            comGesDao.insert(compromiso);

            JSONArray items = compromisos.getJSONObject(i).getJSONArray("items");
            List<Items> itemsList = new ArrayList<>();
            
            Items item = null;
            for(int j = 0; j < items.length(); ++j){
                item = new Items(items.getJSONObject(j).optString("iteNom"), compromiso);
                itemDao.insert(item);
                itemsList.add(item);
                
            }
            compromiso.setItemsList(itemsList);
            compromisosList.add(compromiso);
        }
        
        System.out.println(plantilla);
       
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();            
        oResponse.put("plaId",plantilla.getPlfId());
        
        return WebResponse.crearWebResponseExito("El registro la plantilla de Ficha", oResponse);
    }
}
