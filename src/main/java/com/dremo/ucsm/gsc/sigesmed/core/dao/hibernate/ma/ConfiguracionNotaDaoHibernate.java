/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.ConfiguracionNotaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliar;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.ConfiguracionNota;
import java.util.List;
import com.dremo.ucsm.gsc.sigesmed.core.datastore.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author abel
 */
public class ConfiguracionNotaDaoHibernate extends GenericDaoHibernate<ConfiguracionNota> implements ConfiguracionNotaDao{

    @Override
    public List<ConfiguracionNota> listar_configuracion() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            String hql = "SELECT cn FROM ConfiguracionNota cn WHERE cn.estReg!='E'";
            Query query = session.createQuery(hql);
            return query.list();
        }catch(Exception e){
              throw e;
        }finally {
            session.close();
        }

      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
