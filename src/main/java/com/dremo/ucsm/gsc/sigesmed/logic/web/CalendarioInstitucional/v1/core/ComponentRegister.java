package com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.EditarActividadCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.RegistrarActividadCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.EliminarActividadCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.ListarActividadesCalendarioTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.ListarFuncionalidadesTx;
import com.dremo.ucsm.gsc.sigesmed.logic.web.CalendarioInstitucional.v1.tx.ReactivarActividadCalendarioTx;

public class ComponentRegister implements IComponentRegister{
    @Override
    public WebComponent createComponent(){
        WebComponent component = new WebComponent(Sigesmed.MODULO_WEB);        
        component.setName("calendario");
        component.setVersion(1);

        component.addTransactionGET("listarFuncionalidades", ListarFuncionalidadesTx.class);
        component.addTransactionGET("listarActividades", ListarActividadesCalendarioTx.class);
        
        component.addTransactionPOST("registrarActividad", RegistrarActividadCalendarioTx.class);
        component.addTransactionPOST("reactivarActividad", ReactivarActividadCalendarioTx.class);
        
        component.addTransactionPUT("editarActividad", EditarActividadCalendarioTx.class);
        
        component.addTransactionDELETE("eliminarActividad", EliminarActividadCalendarioTx.class);
        return component;
    }
}


