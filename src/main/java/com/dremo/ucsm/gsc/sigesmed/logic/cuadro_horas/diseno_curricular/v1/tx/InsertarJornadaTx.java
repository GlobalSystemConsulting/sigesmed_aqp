/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.cuadro_horas.diseno_curricular.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.JornadaEscolar;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import org.json.JSONObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.Date;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mech.DisenoCurricularDao;
/**
 *
 * @author abel
 */
public class InsertarJornadaTx implements ITransaction{

    @Override
    public WebResponse execute(WebRequest wr) {
                
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        JornadaEscolar nuevo = null;
        try{
            
            JSONObject requestData = (JSONObject)wr.getData();
            int jornadaID = requestData.optInt("jornadaID");
            int disenoID = requestData.getInt("diseñoID");
            int nivelID = requestData.getInt("nivelID");
            String abreviacion = requestData.getString("abreviacion");
            String nombre = requestData.getString("nombre");
            int obligatoria = requestData.getInt("hObligatoria");
            int libre = requestData.getInt("hLibre");
            int tutoria = requestData.getInt("hTutoria");
            int total = obligatoria + libre + tutoria;
            String descripcion = requestData.getString("descripcion");
            
            nuevo = new JornadaEscolar(jornadaID,abreviacion, nombre, descripcion,obligatoria, libre, tutoria, total, disenoID,nivelID, new Date(), wr.getIdUsuario(), 'A');
        
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo registrar la Jornada Escolar, datos incorrectos", e.getMessage() );
        }
        //Fin
        
        DisenoCurricularDao disenoDao = (DisenoCurricularDao)FactoryDao.buildDao("mech.DisenoCurricularDao");
        try{
            disenoDao.insertarJornadaEscolar(nuevo);
        }catch(Exception e){
            System.out.println("No se pudo registrar la Jornada Escolar\n"+e);
            return WebResponse.crearWebResponseError("No se pudo registrar el Jornada Escolar", e.getMessage() );
        }
        //Fin
        
        
        /*
        *  Repuesta Correcta
        */
        JSONObject oResponse = new JSONObject();
        oResponse.put("jornadaID",nuevo.getJorEscId());
        oResponse.put("total",nuevo.getHorTot());
        oResponse.put("fecha",nuevo.getFecMod().toString());
        return WebResponse.crearWebResponseExito("El registro de la Jornada Escolar se realizo correctamente", oResponse);
        //Fin
    }    
    
    
}
