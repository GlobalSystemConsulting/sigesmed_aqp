/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sci;

import java.math.BigDecimal;

/**
 *
 * @author Administrador
 */
public class ResultadosMensualPorCuenta {

    private int numeroCuenta;
    private String nombreCuenta; 
    private int mes;
    private BigDecimal importe;

    public ResultadosMensualPorCuenta(int numeroCuenta, String nombreCuenta, int mes, BigDecimal importe) {
        this.numeroCuenta = numeroCuenta;
        this.nombreCuenta = nombreCuenta;
        this.mes = mes;
        this.importe = importe;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

  
    
    
   
   
    }
