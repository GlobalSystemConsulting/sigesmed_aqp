/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.documentos_gestion.entorno_inicio.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.ItemFileDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.rdg.TipoEspecializadoDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.ItemFile;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.Organizacion;
import com.dremo.ucsm.gsc.sigesmed.core.entity.rdg.TipoEspecializado;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.util.MChart;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.data.general.DefaultPieDataset;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ucsm
 */
public class ConstruirReporteGeneralTx implements ITransaction{
    Integer idUsuario = 0;
    String strFecMod = "";
    Date fechaMod = null;
    
    int usuCod=0;
    int orgID=0;
    boolean flagUgel=false;
    @Override
    public WebResponse execute(WebRequest wr) {
        try {
            JSONObject requestData = (JSONObject) wr.getData();
            usuCod=requestData.getInt("usuCod");
            orgID=requestData.getInt("organizacionID");
            flagUgel=requestData.getBoolean("flagUgel");            
        } catch (Exception e) {
            return WebResponse.crearWebResponseError("No se pudo leer los datos de entrada");
        }
        
//        RgeDao rgeDao = (RgeDao)FactoryDao.buildDao("rdg.RgeDao");
//        ReporteGrupoEtiqueta newRge = new ReporteGrupoEtiqueta();        
        ItemFileDao itemDao = (ItemFileDao)FactoryDao.buildDao("rdg.ItemFileDao");
        TipoEspecializadoDao tipDao= (TipoEspecializadoDao)FactoryDao.buildDao("rdg.TipoEspecializadoDao");        
        List<String> tipos=itemDao.buscarTiposDocumentos(usuCod, orgID);
        List<ItemFile>carpetas=itemDao.buscarCarpetas();
        JSONArray reporte_=new JSONArray();
        for(String t: tipos){
            JSONObject elem=new JSONObject();
            TipoEspecializado tip=tipDao.getByAlias(t);//Obtenemos tipo especializado
            //elem.put("cabecera", tip.getTesNom());
            elem.put("cabecera", t);
            int cont=0;
            for(ItemFile folder: carpetas){                
                ItemFile item=new ItemFile();
                item.setTeside(tip);
                item.setIteOrgIde(new Organizacion(orgID));
                cont=cont+itemDao.obtenerNumDocPorCarpetaYTipo(folder, item);
            }
            elem.put("valor", cont);
            reporte_.put(elem);
        }
        
        return WebResponse.crearWebResponseExito("Reporte generado con éxito", reporte_);        
    }
    
}
