/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gscadmin
 */
@Entity
@Table(name = "docente_carpeta_pedagogica", schema = "pedagogico")

public class DocenteCarpetaPedagogica implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocenteCarpetaPedagogicaPK docenteCarpetaPedagogicaPK;
    @Column(name = "ver")
    private Integer ver;
    @Basic(optional = false)
    @Column(name = "des")
    private String des;
    @Basic(optional = false)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @Column(name = "com")
    private boolean com;
    @Column(name = "est_ava")
    private Short estAva;
    @Column(name = "usu_mod")
    private Integer usuMod;
    @Column(name = "fec_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecMod;
    @Column(name = "est_reg")
    private Character estReg;
    @JoinColumn(name = "car_dig_id", referencedColumnName = "car_dig_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CarpetaPedagogica carpetaPedagogica;
    @JoinColumn(name = "con_sec_car_ped_id", referencedColumnName = "con_sec_car_ped_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ContenidoSeccionesCarpetaPedagogica contenidoSeccionesCarpetaPedagogica;
    @JoinColumn(name = "doc_id", referencedColumnName = "doc_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Docente docente;

    public DocenteCarpetaPedagogica() {
    }

    public DocenteCarpetaPedagogica(DocenteCarpetaPedagogicaPK docenteCarpetaPedagogicaPK) {
        this.docenteCarpetaPedagogicaPK = docenteCarpetaPedagogicaPK;
    }

    public DocenteCarpetaPedagogica(DocenteCarpetaPedagogicaPK docenteCarpetaPedagogicaPK, String des, String url, boolean com) {
        this.docenteCarpetaPedagogicaPK = docenteCarpetaPedagogicaPK;
        this.des = des;
        this.url = url;
        this.com = com;
    }

    public DocenteCarpetaPedagogica(int docId, long carDigId, int conSecCarPedId) {
        this.docenteCarpetaPedagogicaPK = new DocenteCarpetaPedagogicaPK(docId, carDigId, conSecCarPedId);
    }

    public DocenteCarpetaPedagogicaPK getDocenteCarpetaPedagogicaPK() {
        return docenteCarpetaPedagogicaPK;
    }

    public void setDocenteCarpetaPedagogicaPK(DocenteCarpetaPedagogicaPK docenteCarpetaPedagogicaPK) {
        this.docenteCarpetaPedagogicaPK = docenteCarpetaPedagogicaPK;
    }

    public Integer getVer() {
        return ver;
    }

    public void setVer(Integer ver) {
        this.ver = ver;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getCom() {
        return com;
    }

    public void setCom(boolean com) {
        this.com = com;
    }

    public Short getEstAva() {
        return estAva;
    }

    public void setEstAva(Short estAva) {
        this.estAva = estAva;
    }

    public Integer getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public CarpetaPedagogica getCarpetaPedagogica() {
        return carpetaPedagogica;
    }

    public void setCarpetaPedagogica(CarpetaPedagogica carpetaPedagogica) {
        this.carpetaPedagogica = carpetaPedagogica;
    }

    public ContenidoSeccionesCarpetaPedagogica getContenidoSeccionesCarpetaPedagogica() {
        return contenidoSeccionesCarpetaPedagogica;
    }

    public void setContenidoSeccionesCarpetaPedagogica(ContenidoSeccionesCarpetaPedagogica contenidoSeccionesCarpetaPedagogica) {
        this.contenidoSeccionesCarpetaPedagogica = contenidoSeccionesCarpetaPedagogica;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docenteCarpetaPedagogicaPK != null ? docenteCarpetaPedagogicaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocenteCarpetaPedagogica)) {
            return false;
        }
        DocenteCarpetaPedagogica other = (DocenteCarpetaPedagogica) object;
        if ((this.docenteCarpetaPedagogicaPK == null && other.docenteCarpetaPedagogicaPK != null) || (this.docenteCarpetaPedagogicaPK != null && !this.docenteCarpetaPedagogicaPK.equals(other.docenteCarpetaPedagogicaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.DocenteCarpetaPedagogica[ docenteCarpetaPedagogicaPK=" + docenteCarpetaPedagogicaPK + " ]";
    }
    
}
