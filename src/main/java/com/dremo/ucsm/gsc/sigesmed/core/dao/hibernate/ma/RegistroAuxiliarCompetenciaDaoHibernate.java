/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.ma;

import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.GenericDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.RegistroAuxiliarCompetenciaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.RegistroAuxiliarCompetencia;

/**
 *
 * @author gscadmin
 */
public class RegistroAuxiliarCompetenciaDaoHibernate extends GenericDaoHibernate<RegistroAuxiliarCompetencia> implements RegistroAuxiliarCompetenciaDao{
    
}
