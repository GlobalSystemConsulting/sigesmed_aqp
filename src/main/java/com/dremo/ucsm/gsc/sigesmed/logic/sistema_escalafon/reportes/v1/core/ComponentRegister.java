/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.core;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebComponent;
import com.dremo.ucsm.gsc.sigesmed.core.service.constantes.Sigesmed;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.IComponentRegister;
import com.dremo.ucsm.gsc.sigesmed.logic.sistema_escalafon.reportes.v1.tx.*;
/**
 *
 * @author Yemi
 */
public class ComponentRegister implements IComponentRegister{

    @Override
    public WebComponent createComponent() {
        WebComponent seComponent = new WebComponent(Sigesmed.MODULO_ESCALAFON);

        seComponent.setName("reportes");
        seComponent.setVersion(1);
        
        
        seComponent.addTransactionGET("getChildrenGroupByWorker", GetChildrenGroupByWorkerTx.class);
        seComponent.addTransactionGET("getChildrenGroupByWorker", GetChildrenGroupByWorkerTx.class);
        seComponent.addTransactionGET("contarMeritosPorOrganizacion", ContarMeritosPorOrganizacionTx.class);
        seComponent.addTransactionGET("contarDemeritosPorOrganizacion", ContarDemeritosPorOrganizacionTx.class);
        
        seComponent.addTransactionPOST("reporteFichaEscalafonaria", ReporteFichaEscalafonariaTx.class); 
        seComponent.addTransactionPOST("reporteEstadistico", ReporteEstadisticoTx.class); 
        
        return seComponent;
    }
    
}
