/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.smdg.reporte.tx;

import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.FileJsonObject;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.dremo.ucsm.gsc.sigesmed.logic.tramite_documentario.expediente.v1.tx.ReporteTx;
import com.dremo.ucsm.gsc.sigesmed.util.GTabla;
import com.dremo.ucsm.gsc.sigesmed.util.Mitext;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gscadmin
 */
public class ReporteObjetivosTx implements ITransaction{
    @Override
    public WebResponse execute(WebRequest wr) {
        
        /*
        *   Parte para la lectura, verificacion y validacion de datos
        */
        FileJsonObject miGrafico1 = null;
        FileJsonObject miGrafico2 = null;
        GTabla miTabla = null;
        try{
            JSONObject requestData = (JSONObject)wr.getData();
            
            JSONArray tabla = requestData.optJSONArray("tabla");
            
            if(tabla!=null && tabla.length() > 0){
                
                float[] cols = {4f,1f,1f,3f,1f};
                miTabla = new GTabla(cols);

                String[] labels = {"Descripcion","Avance","Fecha","Responsable","Tipo"};
                miTabla.build(labels);
                for(int i = 0; i < tabla.length();i++){
                    JSONObject bo =tabla.getJSONObject(i);
                    
                    String[] fila = new String [5];
                    
                    fila[0] = bo.getString("objdes");
                    fila[1] = bo.getString("objava") + "%";
                    fila[2] = bo.getString("objfec");
                    
                    JSONObject res = bo.getJSONObject("objusu");
                    
                    fila[3] = res.getString("perNom") + " " + res.getString("perPat") + " " + res.getString("perMat");
                    fila[4] = bo.getString("objtip")=="q"?"cualitativo":"cuantitativo";
                    
                    miTabla.processLine(fila);
                }                
                miGrafico1 = new FileJsonObject( requestData.getJSONObject("grafico1"));
                miGrafico2 = new FileJsonObject( requestData.getJSONObject("grafico2"));
            }
            
        }catch(Exception e){
            System.out.println(e);
            return WebResponse.crearWebResponseError("No se pudo realizar el reporte, grafico iicorrecto", e.getMessage() );
        }
        //Fin        
        
        
        /*
        *  Repuesta Correcta
        */
        
        JSONObject response = new JSONObject();
        try {
            Mitext r = new Mitext();            
//            if(miTabla==null){
//                r.agregarTitulo(miGrafico1.getName());
//                r.newLine(1);
//                r.agregarImagen64(miGrafico2.getData());
//            }
            r.agregarTitulo("REPORTE DE AVANCE DE OBJETIVOS");
            r.newLine(1);
            r.agregarTabla(miTabla);
            r.newLine(1);

            
            Image imagen1 = new Image(ImageDataFactory.create(Base64.decodeBase64(miGrafico1.getData())));
            Image imagen2 = new Image(ImageDataFactory.create(Base64.decodeBase64(miGrafico2.getData())));
            
            Table tablaGraficos = new Table(2);
            tablaGraficos.addCell(imagen1);
            tablaGraficos.addCell(imagen2);
            
            r.agregarTabla(tablaGraficos);
            
            r.cerrarDocumento();
            
            response.append("reporte", r.encodeToBase64() );
        } catch (Exception ex) {
            Logger.getLogger(ReporteTx.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return WebResponse.crearWebResponseExito("el reporte se realizo",response);
        //Fin
    }
}
