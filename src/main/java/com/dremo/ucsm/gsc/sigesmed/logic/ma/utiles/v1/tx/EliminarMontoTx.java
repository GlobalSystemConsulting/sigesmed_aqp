/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.ma.utiles.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.ma.MontoListaDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.ma.MontoLista;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import com.sun.org.apache.xml.internal.serialize.SerializerFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ucsm
 */
public class EliminarMontoTx implements ITransaction{
    private static final Logger logger = Logger.getLogger(EliminarMontoTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String idStr = wr.getMetadataValue("id");
        return eliminarMonto(Integer.parseInt(idStr));
    }
    private WebResponse eliminarMonto(int idStr){
        try{
            MontoListaDao montoDao = (MontoListaDao) FactoryDao.buildDao("ma.MontoListaDao");
            MontoLista montoLista = montoDao.buscarPorId(idStr);
            montoDao.delete(montoLista);
            return WebResponse.crearWebResponseExito("Operacion realizada con exito",WebResponse.OK_RESPONSE);
        }catch (Exception e){
            logger.log(Level.SEVERE,"eliminarMonto",e);
            return WebResponse.crearWebResponseError("No se realizo la operacion",WebResponse.BAD_RESPONSE);
        }
    }
}
