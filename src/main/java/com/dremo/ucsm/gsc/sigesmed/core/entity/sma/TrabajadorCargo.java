/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.sma;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gscadmin
 */

@Entity(name = "com.dremo.ucsm.gsc.sigesmed.core.entity.sma.TrabajadorCargo")
@Table(name = "trabajador_cargo")

public class TrabajadorCargo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "crg_tra_ide")
    private Integer crgTraIde;
    
    @Column(name = "crg_tra_ali")
    private String crgTraAli;
    
    @Column(name = "crg_tra_des")
    private String crgTraDes;
    
    @Basic(optional = false)
    @Column(name = "crg_tra_nom")
    private String crgTraNom;
    
    @Column(name = "est_reg")
    private Character estReg;
    
    @Column(name = "crg_tra_tip")
    private String crgTraTip;
    
    @OneToMany(mappedBy = "trabajadorCargo", fetch = FetchType.LAZY)
    private List<Trabajador> trabajadorList;

    public TrabajadorCargo() {
    }

    public TrabajadorCargo(Integer crgTraIde) {
        this.crgTraIde = crgTraIde;
    }

    public TrabajadorCargo(Integer crgTraIde, String crgTraNom) {
        this.crgTraIde = crgTraIde;
        this.crgTraNom = crgTraNom;
    }

    public Integer getCrgTraIde() {
        return crgTraIde;
    }

    public void setCrgTraIde(Integer crgTraIde) {
        this.crgTraIde = crgTraIde;
    }

    public String getCrgTraAli() {
        return crgTraAli;
    }

    public void setCrgTraAli(String crgTraAli) {
        this.crgTraAli = crgTraAli;
    }

    public String getCrgTraDes() {
        return crgTraDes;
    }

    public void setCrgTraDes(String crgTraDes) {
        this.crgTraDes = crgTraDes;
    }

    public String getCrgTraNom() {
        return crgTraNom;
    }

    public void setCrgTraNom(String crgTraNom) {
        this.crgTraNom = crgTraNom;
    }

    public Character getEstReg() {
        return estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

    public String getCrgTraTip() {
        return crgTraTip;
    }

    public void setCrgTraTip(String crgTraTip) {
        this.crgTraTip = crgTraTip;
    }

    @XmlTransient
    public List<Trabajador> getTrabajadorList() {
        return trabajadorList;
    }

    public void setTrabajadorList(List<Trabajador> trabajadorList) {
        this.trabajadorList = trabajadorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (crgTraIde != null ? crgTraIde.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrabajadorCargo)) {
            return false;
        }
        TrabajadorCargo other = (TrabajadorCargo) object;
        if ((this.crgTraIde == null && other.crgTraIde != null) || (this.crgTraIde != null && !this.crgTraIde.equals(other.crgTraIde))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades_sma.TrabajadorCargo[ crgTraIde=" + crgTraIde + " ]";
    }
    
}
