/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.logic.mep.v1.tx;

import com.dremo.ucsm.gsc.sigesmed.core.dao.FactoryDao;
import com.dremo.ucsm.gsc.sigesmed.core.dao.hibernate.mep.FichaEvaPerslDaoHibernate;
import com.dremo.ucsm.gsc.sigesmed.core.dao.mep.FichaEvaluacionPersonalDao;
import com.dremo.ucsm.gsc.sigesmed.core.entity.mep.FichaEvaluacionPersonal;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebRequest;
import com.dremo.ucsm.gsc.sigesmed.core.service.WebResponse;
import com.dremo.ucsm.gsc.sigesmed.core.service.base.EntityUtil;
import com.dremo.ucsm.gsc.sigesmed.core.service.interfaces.ITransaction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author geank
 */
public class ObtenerFichaEvaluacionTx implements ITransaction{

    private static final Logger logger = Logger.getLogger(ObtenerFichaEvaluacionTx.class.getName());
    @Override
    public WebResponse execute(WebRequest wr) {
        String tipo = wr.getMetadataValue("tipo");
        return obtenerFicha(tipo);
    }
    private WebResponse obtenerFicha(String cargo){
        try{

            FichaEvaluacionPersonalDao fichaDao = (FichaEvaluacionPersonalDao) FactoryDao.buildDao("mep.FichaEvaluacionPersonalDao");
            FichaEvaluacionPersonal fep  = fichaDao.getFichaByCargo(cargo);
            if(fep == null){
                return WebResponse.crearWebResponseError("No existe la ficha para el cargo seleccionado",WebResponse.BAD_RESPONSE);
            }
            else{
                JSONObject oData = new JSONObject(EntityUtil.objectToJSONString(
                        new String[]{"ficEvaPerId","tipTra","nom","escalaValoracionFichas","rangoPorcentualFichas","contenidoFichaEvaluacions"},
                        new String[]{"id","rol","nom","esc","ran","cont"}, fep));
                return WebResponse.crearWebResponseExito("Exito al buscar la ficha",WebResponse.OK_RESPONSE).setData(oData);
            }
        }catch(Exception e){
            logger.log(Level.SEVERE,logger.getName()+":listarFichasEvaluacion()",e);
            return WebResponse.crearWebResponseError("Error al ejecutar la consulta",WebResponse.BAD_RESPONSE);
        }
    }
    
}
