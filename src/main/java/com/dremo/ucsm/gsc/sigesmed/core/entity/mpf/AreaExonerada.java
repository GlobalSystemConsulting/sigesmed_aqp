/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dremo.ucsm.gsc.sigesmed.core.entity.mpf;

import com.dremo.ucsm.gsc.sigesmed.core.entity.mech.AreaCurricular;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Carlos
 */
@Entity(name = "AreaExoneradaMPF")
@Table(name = "area_exonerada", schema = "pedagogico"
)
public class AreaExonerada implements java.io.Serializable {

    @Id
    @Column(name="are_exo_id")
    private Integer id;
    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "per_id", nullable = false, insertable = false, updatable = false)
    private Estudiante estudiante;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "are_cur_id")
    private AreaCurricular areaCurricular;

    @Column(name = "res_exo", length = 60)
    private String resExo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_exo", length = 29)
    private Date fecExo;

    @Column(name = "obs")
    private String obs;

    @Column(name = "usu_mod")
    private Integer usuMod;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fec_mod", length = 29)
    private Date fecMod;

    @Column(name = "est_reg", length = 1)
    private Character estReg;

    public AreaExonerada() {
    }

    public AreaExonerada(Integer id) {
        this.id = id;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Estudiante getEstudiante() {
        return this.estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public AreaCurricular getAreaCurricular() {
        return this.areaCurricular;
    }

    public void setAreaCurricular(AreaCurricular areaCurricular) {
        this.areaCurricular = areaCurricular;
    }

    public String getResExo() {
        return this.resExo;
    }

    public void setResExo(String resExo) {
        this.resExo = resExo;
    }

    public Date getFecExo() {
        return this.fecExo;
    }

    public void setFecExo(Date fecExo) {
        this.fecExo = fecExo;
    }

    public String getObs() {
        return this.obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Integer getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(Integer usuMod) {
        this.usuMod = usuMod;
    }

    public Date getFecMod() {
        return this.fecMod;
    }

    public void setFecMod(Date fecMod) {
        this.fecMod = fecMod;
    }

    public Character getEstReg() {
        return this.estReg;
    }

    public void setEstReg(Character estReg) {
        this.estReg = estReg;
    }

}
