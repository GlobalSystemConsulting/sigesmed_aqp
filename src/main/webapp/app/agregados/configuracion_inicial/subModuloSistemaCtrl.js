app.controller("subModuloSistemaCtrl",["$scope","NgTableParams","crud","modal", function ($scope,NgTableParams,crud,modal){
        
    $scope.modulos = [];
    $scope.nuevoSubModulo = {codigo:"",nombre:"",descripcion:"",icono:"",estado:'A',moduloID:0};    
    $scope.subModuloSel = {};
    
    $scope.funcionesSel = [];
    $scope.estados = [{id:'A',title:"activo"},{id:'E',title:"eliminado"}];
    
    var paramsSubModulo = {count: 10};
    var settingSubModulo = { counts: []};
    $scope.tablaSubModulo = new NgTableParams(paramsSubModulo, settingSubModulo);
    
    
    $scope.listarSubModulos = function(){
        //preparamos un objeto request
        var request = crud.crearRequest('subModuloSistema',1,'listarSubModulos');
        request.setData({listar:true});
        //y las funciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            settingSubModulo.dataset = data.data;
            //asignando la posicion en el arreglo a cada objeto
            iniciarPosiciones(settingSubModulo.dataset);
            $scope.tablaSubModulo.settings(settingSubModulo);
        },function(data){
            console.info(data);
        });
    };
    $scope.agregarSubModulo = function(){
        
        var request = crud.crearRequest('subModuloSistema',1,'insertarSubModulo');
        request.setData($scope.nuevoSubModulo);
        
        crud.insertar("/configuracionInicial",request,function(response){
            
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){
                //recuperamos las variables que nos envio el servidor
                $scope.nuevoSubModulo.subModuloID = response.data.subModuloID;
                $scope.nuevoSubModulo.fecha = response.data.fecha;
                
                insertarElemento(settingSubModulo.dataset,$scope.nuevoSubModulo);
                $scope.tablaSubModulo.reload();
                //reiniciamos las variables
                $scope.nuevoSubModulo = {codigo:"",nombre:"",descripcion:"",icono:"",estado:'A',moduloID:0};
                //cerramos la ventana modal
                $('#modalNuevo').modal('hide');
            }            
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.eliminarSubModulo = function(i,idDato){
        
        modal.mensajeConfirmacion($scope,"seguro que desea eliminar este registro",function(){
            
            var request = crud.crearRequest('subModuloSistema',1,'eliminarSubModulo');
            request.setData({subModuloID:idDato});

            crud.eliminar("/configuracionInicial",request,function(response){

                modal.mensaje("CONFIRMACION",response.responseMsg);
                if(response.responseSta){
                    eliminarElemento(settingSubModulo.dataset,i);
                    $scope.tablaSubModulo.reload();
                }

            },function(data){
                console.info(data);
            });
            
        });
        
    };
    $scope.prepararEditar = function(t){
        $scope.subModuloSel = JSON.parse(JSON.stringify(t));
        $('#modalEditar').modal('show');
    };
    
    $scope.editarSubModulo = function(){
        
        var request = crud.crearRequest('subModuloSistema',1,'actualizarSubModulo');
        request.setData($scope.subModuloSel);
                
        crud.actualizar("/configuracionInicial",request,function(response){
            modal.mensaje("CONFIRMACION",response.responseMsg);
            if(response.responseSta){                
                //actualizando
                settingSubModulo.dataset[$scope.subModuloSel.i] = $scope.subModuloSel;
                $scope.tablaSubModulo.reload();
                $('#modalEditar').modal('hide');
            }
        },function(data){
            console.info(data);
        });
        
    };
    
    $scope.eliminarModulos = function(){
            
        
    };
    
    $scope.verFunciones = function(sm){
        if(sm.funciones && sm.funciones.length>0 )
            $scope.funcionesSel = sm.funciones;
        else
            $scope.funcionesSel = [];
            
        $('#modalVer').modal('show');
    };
    
    listarModulos();
    
    function listarModulos(){
        //preparamos un objeto request
        var request = crud.crearRequest('moduloSistema',1,'listarModulos');
        //llamamos al servicio listar, le damos la ruta donde se encuentra el recurso,el objeto request
        //y las funciones de exito y error
        crud.listar("/configuracionInicial",request,function(data){
            $scope.modulos = data.data;
        },function(data){
            console.info(data);
        });
    };
    
    
}]);
